<html>
    <head>
        <title>Bundle App</title>
		 <link rel="stylesheet" href="assets/css/main.css">
		 <link rel="stylesheet" href="assets/css/main_2.css">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		 <script src="//code.jquery.com/jquery-1.10.2.js"></script>  
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet"/>
		<script src="assets/js/function.js" type="text/javascript"></script>
		<script src="assets/js/function_uom.js" type="text/javascript"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
		
		<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/autofill/2.3.9/js/dataTables.autoFill.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.2.0/js/dataTables.fixedHeader.min.js"></script>
		<script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>

		

		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    </head>
    <body>
        <header class="header_container">
		<input type="hidden" class="store_id" value="<?php echo $store;?>">
           <div class="container">
             <div class="left_container">
				<a href="index?store=<?php echo $store;?>">
					<img id="header_logo" src="assets/images/Integer_Logo.png" alt="Integer" title="Integer">
				</a>
			 </div>
			 <div class="right_container">
				<ul>
					<li><a href="index?store=<?php echo $store;?>">Dashboard</a></li>
					<li><a href="list?store=<?php echo $store;?>&type=both">Bundles/Kits</a></li>
					<li><a href="settings?store=<?php echo $store;?>">Settings</a></li>
					<!--<li><a href="#">Multipack</a></li>
					<li><a href="sales_uom?store=<?php echo $store;?>">Sales UOM</a></li>
					<li><a href="uom_settings?store=<?php echo $store;?>">UOM Settings</a></li>-->
				</ul>
			 </div>
           </div>
			<div id="overlay">
				<div id="overlay_middle">
					<div id="overlay_container">
						<div class="overlay_content">
							<div class="sk-circle" style="float: right;">
								<div class="sk-circle1 sk-child"></div>
								<div class="sk-circle2 sk-child"></div>
								<div class="sk-circle3 sk-child"></div>
								<div class="sk-circle4 sk-child"></div>
								<div class="sk-circle5 sk-child"></div>
								<div class="sk-circle6 sk-child"></div>
								<div class="sk-circle7 sk-child"></div>
								<div class="sk-circle8 sk-child"></div>
								<div class="sk-circle9 sk-child"></div>
								<div class="sk-circle10 sk-child"></div>
								<div class="sk-circle11 sk-child"></div>
								<div class="sk-circle12 sk-child"></div>
							</div>	
						</div>
					</div>
				</div>
			</div>
        </header>

        <main>