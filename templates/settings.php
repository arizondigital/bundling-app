<?php 
$store = $data['store'];
include_once "header.php";
//echo "data<pre>";print_r($data);echo "</pre>";
?>

<section class="main_container">
		<div class="container settings_page">
			<h2 class="heading">SETTINGS</h2>
		</div>
		<div class="add_page_container">
			<!--<form action="save_form_labels" class="frm_labels" id="frm_labels" method="POST">-->
				<div class="form_field_container">
					<label>Display Label</label>
					<input type="text" name="display_view" id="display_view" placeholder="Display View" value="<?php echo $data[0]['param_display_title']?>" class="ui-autocomplete-input" autocomplete="off">
				</div>
				<div class="form_field_container">
				<div class="label_settings"><label>View</label></div>
				<div class="label_settings_value_common">
					<div class="label_settings_value">
						
						<label style="width: 15%;margin-right:10px;">Grid View</label>
						<label style="margin-right:10px;" class="switch">
							<input type="checkbox" <?php if(strtolower($data[0]["param_product_display"]) =="grid" || $data[0]["param_product_display"] == null){echo "checked";} ?> id="grid" class="<?php if(strtolower($data[0]["param_product_display"]) =="grid" || $data[0]["param_product_display"] == null){echo "checked";}else{echo "unchecked";}?> grid status-switch">
							<span class="slider round"></span>
						</label>
					</div>
					<div class="label_settings_value">
						<label style="width: 15%;margin-right:10px;">List View</label>
						<label style="margin-right:10px;" class="switch">
							<input type="checkbox" id="list" <?php if(strtolower($data[0]["param_product_display"]) =="list"){echo "checked";} ?> class="<?php if(strtolower($data[0]["param_product_display"]) =="list"){echo "checked";}else{echo "unchecked";}?> list status-switch">
							<span class="slider round"></span>
						</label>
					</div>
					<div class="label_settings_value">
						<label style="width: 15%;margin-right:10px;">Bundle/Kit Embedded Display</label>
						<label style="margin-right:10px;" class="switch">
							<input type="checkbox" <?php if(strtolower($data[0]["param_product_display"]) =="embedded" || $data[0]["param_product_display"] == null){echo "checked";} ?> id="embedded" class="<?php if(strtolower($data[0]["param_product_display"]) =="embedded" || $data[0]["param_product_display"] == null){echo "checked";}else{echo "unchecked";}?> embedded status-switch">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
				</div>
				<div class="form_field_container">
					<label>Custom Field Label</label>
					<input type="text" name="custom_field_label" id="custom_field_label" placeholder="Custom Field Name" value="<?php echo $data[0]['param_bundling_custom_field_name']?>" class="ui-autocomplete-input" autocomplete="off">
				</div>
				<div class="form_field_container">
					<label >Bundle/Kit Display</label>
					<label style="margin-right:10px;" class="switch">
						<input type="checkbox" disabled id="bundle_display" <?php if(strtolower($data[0]["param_is_bundle_product_display"]) =="true"){echo "checked";} ?> class="<?php if(strtolower($data[0]["param_is_bundle_product_display"]) =="true"){echo "checked";}else{echo "unchecked";}?> bundle_display switch-display">
						<span class="slider round"></span>
					</label>
				</div>
				<!--<div class="form_field_container">
					<label >Bundle/Kit Embedded Display</label>
					<label style="margin-right:10px;" class="switch">
						<input type="checkbox" id="bundle_display" <?php if(strtolower($data[0]["param_is_embedded_display"]) =="true"){echo "checked";} ?> class="<?php if(strtolower($data[0]["param_is_embedded_display"]) =="true"){echo "checked";}else{echo "unchecked";}?> bundle_embed_display">
						<span class="slider round"></span>
					</label>
				</div>-->
				<div class="button_block">		
						<input type="button"  class="btn btn_settings" value="Save">
				</div>
			<!--</form>-->
		</div>
</section>
