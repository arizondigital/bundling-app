<?php 
$store = $data['store'];
$type = $data['type'];
include_once "header.php";
//echo "bundle_kit_output<pre>";print_r($data);echo "</pre>";
?>
<section class="main_container bundle_kit_page">
	<div class="container">

		<section class="main_container bundle_kit_page">
			<div class="container">
				<h2 class="heading"><?php if($data['type'] == 'Bundle'){echo "BUNDLES";}else if($data['type'] == 'Kit'){ echo "KITS";}else{echo "BUNDLES/KITS";}?></h2>
				<div class="list_page">
					<div class="filter_container">
						<form class="bundle_kit_form" action="list?store=<?php echo $store;?>&type=<?php echo $type;?>" method="POST" autocomplete="off">
							<div class="filter_search">
								<div class="search_common">
									<div class="form_field form_input_box">
										<input type="text" name="bundle_kit_name" id="Bundle_Kit_Name" placeholder="Bundle/Kit Name" value="<?php if($data['bundle_kit_name']){echo $data['bundle_kit_name'];}else{}?>" class="ui-autocomplete-input" autocomplete="off">
										<input type="hidden" class="current_type" value="<?php echo $type;?>">
										<!--<input type="hidden" class="total_count" value="<?php echo count($data['bundle_kit']);?>">-->
										<input type="hidden" class="current_page" name="page_no" value="1">
										<input type="hidden" id="page_value" value="bundle_kit_page">
									</div>
									<div class="form_field form_input_box">
										<select name="type" id="product_type">
											  <option value="both">All Product Type</option>
											  <option value="Bundle" <?php if($data['type'] == 'Bundle'){ echo 'selected';}?>>Bundle</option>
											  <option value="Kit" <?php if($data['type'] == 'Kit'){echo 'selected';}?>>Kit</option>
											</select>
									</div>
									<div class="btn_block">
										<input type="submit" name="btn_search" id="btn_search" value="" class="btn btn_search_bundle_kit">
										<input type="button" name="btn_reset" id="btn_reset" value="" class="btn">
									</div>
								</div>
							</div>
						</form>
						
					</div>
					<div class="sync_container">
						<div class="action-bar">
							<div class="form_field form_input_box">
									<select name="type" id="sync_type">
									  <option value="one_hour" selected="">Last One Hour</option>
									  <option value="3_hour" >Last Four Hours</option>
									  <option value="one_day">Last 24 Hours</option>
									   <!--<option value="custom">Custom</option>-->
									</select>
							</div>
						<!--	<div class="form_field form_input_box custom_sync_date_div">
								<input type="text"  id="custom_sync_date" placeholder="Choose Date" value class="ui-autocomplete-input" autocomplete="off">
							</div>-->
							<input type="hidden" class="order_from_date"  value="">
							<input type="hidden" class="order_to_date" value="">
							<button class="btn sync-btn">Sync with BC Catalog</button>
							<div class="add_new_block">
							<a class="btn" href="add?store=<?php echo $store;?>">+ Add New</a>
						</div>
						</div>
						
					</div>
					<div class="table_section">
						<table>
							<thead>
								<tr>
									<th>S.No</th>
									<th>Bundle/Kit Name</th>
									<th>SKU</th>
									<th>Product Type</th>
									<th>Data Source</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php if(count($data['bundle_kit']) > 0){$i = 0; foreach($data['bundle_kit'] as $key => $bundle_kit){
							$i=$i+1;
							//$product_details = json_decode($bundle_kit['product_details']);
							//echo "bundle_kit<pre>";print_r($bundle_kit);echo "</pre>";
							?>
							<tr class="body-data">
							<td class="s_no"><?php echo $key+1;?></td>
							<td class="bundle_name"><?php echo $bundle_kit['name'];?></td>
							<td class="bundle_sku"><?php if($bundle_kit['sku'] != null){echo $bundle_kit['sku'];}else{echo "-";}?></td>
							<td class="data_source"><?php echo $bundle_kit['producttype'];?></td>
							<td class="type"><?php echo $bundle_kit['bundle_product_type'];?></td>
							<td>
								<a href="javascript:void(0);" class="view_icon" data-toggle="modal" data-target="#viewscreen_<?php echo $bundle_kit['bundlekit_id'];?>" id="view_bundle_kit_<?php echo $bundle_kit['bundlekit_id'];?>">
									<svg
									   id="svg840"
									   xml:space="preserve"
									   width="302.362"
									   height="302.362"
									   viewBox="0 0 302.362 302.362"><metadata
										 id="metadata846"><rdf:RDF><cc:Work
											 rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type
											   rdf:resource="http://purl.org/dc/dcmitype/StillImage" /><dc:title></dc:title></cc:Work></rdf:RDF></metadata><defs
										 id="defs844" /><g
										 id="g848"
										 transform="matrix(1.3333333,0,0,-1.3333333,0,453.54333)"><g
										   id="g856"
										   transform="translate(-56.692997,56.693003)"><path
											 d="m 282.7647,172.2203 c -3.0266,4.1389 -6.3757,8.0637 -9.7812,11.8936 -7.515,8.4492 -15.6555,16.384 -24.359,23.6058 -22.0102,18.2639 -49.2619,33.1353 -78.5477,33.1353 -29.2853,0 -56.5368,-14.8717 -78.5469,-33.135 -8.703,-7.2218 -16.8435,-15.156 -24.3584,-23.6049 -3.405,-3.8285 -6.7541,-7.752 -9.7784,-11.89 -0.4561,-0.6247 -0.7002,-1.3722 -0.7002,-2.1458 0,-0.7744 0.2446,-1.5228 0.701,-2.1478 3.0223,-4.1392 6.3732,-8.062 9.7776,-11.8899 7.5149,-8.449 15.6554,-16.3834 24.3584,-23.6053 22.0101,-18.2627 49.2618,-33.1338 78.5469,-33.1338 29.2855,0 56.5372,14.8705 78.548,33.1336 8.7029,7.2218 16.8437,15.1554 24.3587,23.6049 3.4058,3.8288 6.7529,7.7533 9.7801,11.8902 0.4546,0.6217 0.6993,1.364 0.701,2.1319 v 0.0247 c -0.002,0.7696 -0.2449,1.5112 -0.6999,2.1325 z M 170.0768,106.5702 c -23.9897,0 -46.5954,11.0106 -65.5101,25.0197 C 90.758,141.817 77.9499,154.2127 67.0229,167.466 l -2.1546,2.6133 2.1518,2.6155 c 10.9069,13.2582 23.7036,25.657 37.5032,35.8804 18.9332,14.0273 41.5409,25.0098 65.5535,25.0098 23.9893,0 46.5944,-11.0083 65.5092,-25.0169 13.8095,-10.2274 26.6201,-22.6241 37.5469,-35.8798 l 2.1543,-2.613 -2.1518,-2.6155 c -10.908,-13.2573 -23.703,-25.6552 -37.5029,-35.8787 -18.9346,-14.0281 -41.542,-25.0109 -65.5557,-25.0109"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path850" /><path
											 d="m 170.0768,212.4488 c -23.3604,0 -42.3689,-19.0117 -42.3689,-42.3712 0,-23.3597 19.0083,-42.3711 42.3689,-42.3711 23.3611,0 42.3708,19.0097 42.3708,42.3711 0,23.3609 -19.01,42.3712 -42.3708,42.3712 z m 0,-77.472 c -19.3573,0 -35.1009,15.7436 -35.1009,35.1008 0,19.3572 15.7436,35.1009 35.1009,35.1009 19.3572,0 35.1008,-15.7437 35.1008,-35.1009 0,-19.3584 -15.7425,-35.1008 -35.1008,-35.1008"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path852" /></g></g>
									</svg>
								</a>
								<div class="modal" class ="viewscreen" id="viewscreen_<?php echo $bundle_kit['bundlekit_id'];?>">
									<div class="modal-dialog">
									  <div class="modal-content">
										<!-- Modal Header -->
										<button type="button" class="close" data-dismiss="modal">X</button>
										<section class="view_page">
											<div class="container">
												<section class="">
													<div class="container">
														<h2 class="heading">view</h2>
														<div class="add_page_container">
															<div class="form_field_container">
																<label>Bundle/Kit Name:</label>
																<span><?php echo $bundle_kit['name'];?></span>
															</div>
															<div class="form_field_container">
																<label>Product Type:</label>
																<span><?php echo $bundle_kit['producttype'];?></span>
															</div>
															<div class="form_add_container">
																<label>Products:</label>
															
															</div>
															<div class="table_section">
															<table class="sub_product_table">
																<thead>
																	<tr>
																		<th>S.No</th>
																		<th>Product Name</th>
																		<th>Mandatory</th>
																		<th>Price</th>
																		<th>Qty</th>
																		<th>Adjusted Price</th>
																	</tr>
																</thead>
																<tbody>
																<?php $i =0;foreach(json_decode($bundle_kit['product_details']) as $index => $bundle_kit_details){
														$i=$i+1;
														//echo "<pre>";print_r($bundle_kit);echo "</pre>"; echo $bundle_kit->adjusted_price;
														?>
																	<tr>
																		<td><?php echo $i;?></td>
																		<td><?php echo $bundle_kit_details->product_name;?></td>
																		<td>
																			<?php if($bundle_kit_details->is_mandatory == 1){ echo 'Yes';}else{echo "No";}?>
																		</td>
																		<td>
																		<?php echo $bundle_kit_details ->price;?>
																		</td>
																		<th><?php if($bundle_kit_details->qty != null){ echo $bundle_kit_details->qty;}else{echo "-";}?></th>
																		<th><?php if($bundle_kit_details->adjusted_price != null){ echo $bundle_kit_details->adjusted_price;}else{echo "-";}?></th>
																	</tr>
																	<?php }?>
																</tbody>
															</table>
														</div>
														<!--<div class="button_block">
															<input type="submit" class="btn" value="Save">
															<input type="submit" class="btn" value="Cancel">
														</div>-->
														</div>
													</div>
												</section>
											</div>
										</section>
									  </div>
									</div>
								</div>
								<a href="edit?store=<?php echo $store;?>&id=<?php echo $bundle_kit['bundlekit_id'];?>" class="edit_icon" >
									<svg
									   id="svg840"
									   xml:space="preserve"
									   width="302.362"
									   height="302.362"
									   viewBox="0 0 302.362 302.362"><metadata
										 id="metadata846"><rdf:RDF><cc:Work
											 rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type
											   rdf:resource="http://purl.org/dc/dcmitype/StillImage" /><dc:title></dc:title></cc:Work></rdf:RDF></metadata><defs
										 id="defs844" /><g
										 id="g848"
										 transform="matrix(1.3333333,0,0,-1.3333333,0,453.54333)"><g
										   id="g1435"
										   transform="translate(-56.692997,56.692947)"><path
											 d="m 269.6958,275.8337 c -9.4025,9.4031 -24.6444,9.4028 -34.0469,0 L 133.3539,173.5393 c -0.4892,-0.4893 -0.8433,-1.0984 -1.0284,-1.7643 l -13.4518,-48.5663 c -0.3864,-1.3887 0.0105,-2.8845 1.0261,-3.903 1.0182,-1.0165 2.5189,-1.4111 3.9051,-1.0284 l 48.5625,13.4541 c 0.6687,0.1854 1.2753,0.5397 1.766,1.0301 l 102.2913,102.2947 c 4.5046,4.5144 7.0169,10.5616 7.0399,16.9347 v 0.1783 c -0.023,6.376 -2.5353,12.424 -7.039,16.9373 z m -127.5111,-104.7994 86.1613,86.1639 29.4421,-29.4421 -86.1642,-86.1655 z m -3.8256,-7.5073 25.7592,-25.7635 -35.6303,-9.8719 z m 132.3995,77.1962 -7.301,-7.3015 -29.4465,29.4443 7.306,7.3032 c 6.2713,6.2686 16.4392,6.2686 22.7112,-3e-4 l 6.7311,-6.7294 c 6.2603,-6.2773 6.262,-16.4387 -8e-4,-22.7163"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path868" /><path
											 d="m 267.4539,139.1323 c -2.215,0 -4.0056,-1.7934 -4.0056,-4.0079 V 84.2142 C 263.4361,73.7674 254.969,65.2972 244.5205,65.2876 H 83.6337 C 73.1863,65.2972 64.7189,73.7674 64.7067,84.2151 V 233.634 c 0.0122,10.4471 8.4802,18.9142 18.9275,18.9264 h 50.9114 c 2.2139,0 4.0059,1.7946 4.0059,4.0079 0,2.2125 -1.7929,4.0076 -4.0059,4.0076 H 83.6362 C 68.7626,260.5592 56.7099,248.5049 56.6929,233.6321 V 84.2148 C 56.7099,69.3405 68.7623,57.2882 83.6354,57.2717 h 160.8834 c 14.8754,0.0165 26.9266,12.068 26.9433,26.9422 v 50.9105 c 0,2.2156 -1.7923,4.0079 -4.0082,4.0079"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path870" /></g></g>
									</svg>
								</a>
								<a href="javascipt:void(0);" class="delete_icon delete_bundle_kit" id="delete_bundle_kit_<?php echo $bundle_kit['bundlekit_id'];?>">
									<svg
									   id="svg840"
									   xml:space="preserve"
									   width="302.362"
									   height="302.362"
									   viewBox="0 0 302.362 302.362"><metadata
										 id="metadata846"><rdf:RDF><cc:Work
											 rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type
											   rdf:resource="http://purl.org/dc/dcmitype/StillImage" /><dc:title></dc:title></cc:Work></rdf:RDF></metadata><defs
										 id="defs844" /><g
										 id="g848"
										 transform="matrix(1.3333333,0,0,-1.3333333,0,453.54333)"><g
										   id="g2044"
										   transform="translate(-56.692997,56.693003)"><path
											 d="m 129.5433,249.2014 h 7.9141 v 26.3494 h 65.2427 v -26.3494 h 7.9138 v 24.1058 c 0,5.5978 -4.5564,10.1574 -10.1548,10.1574 h -60.7607 c -5.5984,0 -10.1551,-4.5596 -10.1551,-10.1574 z"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path1468" /><path
											 d="m 64.7535,247.8163 v -7.9149 H 83.6841 L 99.4878,60.3023 c 0.1798,-2.043 1.8905,-3.6094 3.9413,-3.6094 H 236.414 c 2.0506,0 3.7635,1.5667 3.9418,3.6131 l 15.8035,179.5954 h 19.2447 v 7.9149 z M 232.7907,64.6067 H 107.0529 L 91.6291,239.9014 h 156.5881 z"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path1470" /><path
											 d="m 141.9055,102.7171 -3.6403,99.3707 -7.9089,-0.2911 3.64,-99.3696 z"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path1472" /><path
											 d="m 166.1219,201.9351 v -99.364 h 7.9137 v 99.364 z"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path1474" /><path
											 d="m 209.7967,201.7939 -7.9093,0.29 -3.6382,-99.3682 7.9095,-0.2892 z"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path1476" /></g></g>
									</svg>
								</a>
							</td>
							</tr>
							<?php  }}else{?>
				<tr class="table-data">
					<td colspan=6 style='text-align:center;'>No results found</td>
				</tr>
				<?php }?>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	<div id="pagination-demo">	
		<?php if(count($data['bundle_kit']) >0 && $data['bundle_kit'][0]['total_count'] > 0){ $no_of_pages = ceil($data['bundle_kit'][0]['total_count'] / 15);}else{
		 $no_of_pages = 0;
		}
		 		 $start = 1;$end = 5;if ($data['active'] > 4) {$start =$data['active']-2; $end = $start+4;} if($no_of_pages !=0 && $no_of_pages !=1) { if ($end > $no_of_pages) {$end = $no_of_pages;
            }?>
		 <input type='hidden' class='total_count' value='<?php echo $no_of_pages?>'>
		<div class='pagination'><ul class='pagination_list'>
		<li class='previous pagination_li'><a>Previous</a></li>
		<?php $i=1; for ($i = $start; $i <= $end; $i++) {?>
		<li class='<?php echo $i; if($i== $data['active']){echo ' active';}?> pagination_li page_no' id='"<?php echo $i ?>"'><a><?php echo $i ?></a></li>
		<?php } ?><li class='next pagination_li'><a>Next</a></li></ul></div> <?php }?>
	</div>
	</div>



</section>
 <!-- The Modal -->
