<?php 
$store = $data['store'];
include_once "header.php";
//echo "bundle_kit_output<pre>";print_r($data);echo "</pre>";
//echo count($data);
?>

<!--<?php if(count($data) >1){?>-->
<section class="main_container edit_bundle_kit">
	<div class="container">
		<section class="main_container">
			<div class="container">
				<h2 class="heading">Edit <?php if($data[0]['bundlekit_type'] == 'Bundle'){ echo 'Bundle';}else{echo "Kit";}?></h2>
				<div class="add_page_container">
					<div class="form_field_container">
						<label>Bundle/Kit Name:</label>
						<input type="text" name="Bundle_Kit_Name" id="Bundle_Kit_Name" placeholder="Bundle Name" value="<?php echo $data[0]['bundlekit_name']?>" class="ui-autocomplete-input" autocomplete="off" disabled>
							<input type="hidden" id="page_value" value="edit_bundle_kit_page">
							<input type="hidden" id="Bundle_Kit_id" value="<?php echo $data[0]['bundlekit_id']?>">
							<input type="hidden" id="main_bc_product_id" value="<?php echo $data[0]['bc_product_id']?>">
					</div>
					<div class="form_field_container">
						<label>Bundle/Kit SKU:</label>
						<input type="text" name="Bundle_Kit_SKU" id="Bundle_Kit_SKU" placeholder="Bundle/Kit SKU" value="<?php echo $data[0]['bundle_sku']?>" class="ui-autocomplete-input" autocomplete="off" disabled>
							<input type="hidden" id="page_value" value="edit_bundle_kit_page">
							<input type="hidden" id="Bundle_Kit_id" value="<?php echo $data[0]['bundlekit_id']?>">
							<input type="hidden" id="main_bc_product_id" value="<?php echo $data[0]['bc_product_id']?>">
					</div>
					<div class="form_field_container">
						<label>Product Type:</label>
						<select name="product_type" id="product_type">
							<option value="Bundle" <?php if($data[0]['bundlekit_type'] == 'Bundle'){ echo 'selected';}?>>Bundle</option>
							<option value="Kit" <?php if($data[0]['bundlekit_type'] == 'Kit'){ echo 'selected';}?>>Kit</option>
						</select>
					</div>
					<div class="form_add_container form_field_container">
						<label>Select Product:</label>
						<div class="expand_section">
							<input type="text" name="product_name" id="product_name" placeholder="Select Bigcommerce Product for the Bundle/Kit" value="" class="ui-autocomplete-input" autocomplete="off">
							<a href="javascript:void(0);" class="btn product_add_btn">Add</a>
						</div>
					</div>
				</div>
				<div class="table_section">
				
					<div class="action-bar">
						<?php if($data[0]['new_app_product_status'] == 'pending' || $data[0]['new_app_product_status'] == 'processing'){?>
							<div class="status_bar"><span>This bundle product is currently syncing with BC. Don't make any changes on this Bundle now.</span></div>
						<?php } else{}?>
						<button class="btn sync-btn">Sync with BC Catalog</button>
						<!--<button class="btn bundle-reset">Reset</button>-->
					</div>
				
					<table class="sub_product_table">
						<thead>
							<tr>
								<th>S.No</th>
								<th>Product Name</th>
								<th>SKU</th>
								<th>Price</th>
								<th>Mandatory</th>
								<th class='th-qty' style="display:<?php echo ($data[0]['bundlekit_type'] != 'Kit') ? 'none;' : 'table-cell;'?>">Qty</th>
								<th>Price Adjustment </th>
								<th>Adjusted Price </th>
								<th>Total Price </th>
								<th>Data Source</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php $i =0;foreach(json_decode($data[0]['product_details']) as $index => $bundle_kit){
						$i=$i+1;
						//echo $bundle_kit->is_mandatory;
						?>
							<tr class="<?php if($bundle_kit->is_main_product == true){echo "bundle_kit_main_product";} ?>">
								<td><?php echo $i;?></td>
								<td class="product_name" product_id='<?php echo $bundle_kit->product_id;?>' bundle-kit-product-id=<?php echo $bundle_kit->bundle_kit_productid;?>><?php echo $bundle_kit->product_name;?></td>
								<?php if($bundle_kit->is_main_product != true){?>
									<td><span class="variant_sku" id="<?php echo $bundle_kit->destination_variant_id;?>"><?php if($bundle_kit->sku != null){echo $bundle_kit->sku;}else{ echo "--";} ?></span></td>
								<?php }else{ ?>
									<td><?php echo $data[0]['bundle_sku']?></td>
								<?php }?>
								<?php if($bundle_kit->is_main_product == true){?>
								<input type="hidden" class="bundle_kit_main_price" value ="<?php echo $bundle_kit->price;?>">
								<?php }?>
								<td class="default_price"><?php echo $bundle_kit->price;?></td>
								
								<?php if($bundle_kit->is_main_product != true){?>
								<td>
										<input type="checkbox" id="mandatory" name="mandatory" <?php if($data[0]['bundlekit_type'] == 'Kit'){echo "disabled";}?> value="" <?php if($bundle_kit->is_mandatory == 1){ echo 'checked';}?>>
								</td>
								<?php }else{ ?>
								<td></td>
								<?php } ?>

								<?php if($bundle_kit->is_main_product != true){?>
								<td class='td-qty' style="display:<?php echo ($data[0]['bundlekit_type'] != 'Kit') ? 'none;' : 'table-cell;'?>">
									<?php $qty = ($bundle_kit->qty) ? $bundle_kit->qty : '';?>
										<input type="number" maxlength="3" name="product_qty" class="product_qty"  value=<?php echo $qty;?>>
								</td>
								<?php }else{ if($data[0]['bundlekit_type'] == 'Kit'){?>
								<td class='td-qty' style="display:<?php echo ($data[0]['bundlekit_type'] != 'Kit') ? 'none;' : 'table-cell;'?>"></td>
								
								<?php }} ?>
								
								<?php if($bundle_kit->is_main_product != true){?>
								<td>
										<input type="checkbox" id="price_adjustment" name="price_adjustment" value="" <?php if($bundle_kit->is_price_adjustment == 1){ echo 'checked';}?> >
								</td>
								<?php }else{ ?>
								<td></td>
								<?php } ?>
								<td>

									<?php if($bundle_kit->is_price_adjustment == 1){ ?>
										<input type="number" name="adjusted_price" id="adjusted_price" value="<?php echo preg_replace("/([^0-9\\.])/i", "", $bundle_kit->adjusted_price);?>"  autocomplete="off">
										<span style="display:none" class="no_adjusted_price">--</span>
										<?php } else{?>
										<input type="number"  style="display:none" name="adjusted_price" id="adjusted_price" value="<?php echo preg_replace("/([^0-9\\.])/i", "", $bundle_kit->adjusted_price);?>"  autocomplete="off">
										<span class="no_adjusted_price">--</span>
											<?php }?>
								</td>
								<td>
									<?php if($bundle_kit->is_price_adjustment == 1){ ?>
										<span class="<?php if($bundle_kit->is_main_product == true){echo "bundle_kit_total_price";}else{echo "total_adjusted_price";} ?>"><?php echo $bundle_kit->total_product_price;?></span>
										<?php } else{?>
										<span class="<?php if($bundle_kit->is_main_product == true){echo "bundle_kit_total_price";}else{echo "total_adjusted_price";} ?>"><?php echo $bundle_kit->total_product_price;?></span>
											<?php }?>
								</td>
								<td><?php echo ($bundle_kit->bundle_product_type) ? $bundle_kit->bundle_product_type : '--'?></td>
								<td>
								<a href="javascipt:void(0);" class="<?php if($bundle_kit->is_main_product != true){ echo 'delete_icon delete_product_bundle_kit';}else{ echo 'delete_icon bundle-reset'; }?>" bundle-kit-product-id="<?php echo $data[0]['bundlekit_id'];?>" id="delete_product_bundle_kit_<?php echo $bundle_kit->product_id;?>">
								<?php if($bundle_kit->is_main_product != true){?>
									<svg
									   id="svg840"
									   xml:space="preserve"
									   width="302.362"
									   height="302.362"
									   viewBox="0 0 302.362 302.362"><metadata
										 id="metadata846"><rdf:RDF><cc:Work
											 rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type
											   rdf:resource="http://purl.org/dc/dcmitype/StillImage" /><dc:title></dc:title></cc:Work></rdf:RDF></metadata><defs
										 id="defs844" /><g
										 id="g848"
										 transform="matrix(1.3333333,0,0,-1.3333333,0,453.54333)"><g
										   id="g2044"
										   transform="translate(-56.692997,56.693003)"><path
											 d="m 129.5433,249.2014 h 7.9141 v 26.3494 h 65.2427 v -26.3494 h 7.9138 v 24.1058 c 0,5.5978 -4.5564,10.1574 -10.1548,10.1574 h -60.7607 c -5.5984,0 -10.1551,-4.5596 -10.1551,-10.1574 z"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path1468" /><path
											 d="m 64.7535,247.8163 v -7.9149 H 83.6841 L 99.4878,60.3023 c 0.1798,-2.043 1.8905,-3.6094 3.9413,-3.6094 H 236.414 c 2.0506,0 3.7635,1.5667 3.9418,3.6131 l 15.8035,179.5954 h 19.2447 v 7.9149 z M 232.7907,64.6067 H 107.0529 L 91.6291,239.9014 h 156.5881 z"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path1470" /><path
											 d="m 141.9055,102.7171 -3.6403,99.3707 -7.9089,-0.2911 3.64,-99.3696 z"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path1472" /><path
											 d="m 166.1219,201.9351 v -99.364 h 7.9137 v 99.364 z"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path1474" /><path
											 d="m 209.7967,201.7939 -7.9093,0.29 -3.6382,-99.3682 7.9095,-0.2892 z"
											 style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none"
											 id="path1476" /></g></g>
									</svg>
								<?php }else{?>
									<img class="bundle_reset" src="assets/images/reset_bundle.png">
								<?php }?>
									
								</a>
								</td>
							</tr>
						<?php }?>
						</tbody>
					</table>
				</div>
				<div class="button_block">
					<input type="button" class="btn edit_and_save_buttton" value="Save">
					<input type="button" class="btn cancel_button" value="Cancel">
				</div>
			</div>
		</section>
	</div>
</section>
<!--<?php }else{?>
<section class="main_container edit_bundle_kit" style="text-align:center;margin-top:50px;"><h5> The Collection Sku is not exist in Bigcommerce </h5></section>
<?php }?>-->