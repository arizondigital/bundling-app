<?php 
	include_once "header.php";
?>
<section class="main_container view_page">
	<div class="container">
		<section class="main_container">
			<div class="container">
				<h2 class="heading">view Page</h2>
				<div class="add_page_container">
					<div class="form_field_container">
						<label>Product Name:</label>
						<span>Product Name</span>
					</div>
					<div class="form_field_container">
						<label>Product Type:</label>
						<span>Bundle</span>
					</div>
					<div class="form_add_container">
						<label>Sub Product:</label>
						<!--<a href="javascript:void(0);" class="btn">Add Sub product +</a>
						<div class="expand_section">
							<select name="product_type" id="product_type">
								<option value="">Sub Product 1</option>
								<option value="">Sub Product 2</option>
							</select>
							<a href="javascript:void(0);" class="btn">Add</a>
						</div>-->
					</div>
					<div class="table_section">
					<table class="sub_product_table">
						<thead>
							<tr>
								<th>S.No</th>
								<th>Product Name</th>
								<th>Mandatory</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Sub product 1</td>
								<td>
									Yes
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Sub product 2</td>
								<td>
									Yes
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--<div class="button_block">
					<input type="submit" class="btn" value="Save">
					<input type="submit" class="btn" value="Cancel">
				</div>-->
				</div>
			</div>
		</section>
	</div>
</section>