<?php 
$store = $data['store'];
include_once "header.php";
$import_template_url = 'https://apps.arizonreports.cloud/Furniture/Bundling_App/uploads/import_products.csv';
//echo "sales_uom<pre>";print_r($data);echo "</pre>";
//echo count($data['product_data']);
//$data = json_decode($data);
?>
<?php ?>
<section class="main_container sales_uom">
	<div class="container">
		<section class="main_container">
			<div class="container">
				<input type="hidden" id="page_value" value="sales_uom_page">
                <h2 class="heading">UOM Assignment: Assign Sales UOM to Products</h2>
                <div class="table_section">
					<div class="action-bar">
						<div class="sync_container">
							<div class="">
								<div class="form_field form_input_box">
										<select name="type" id="sync_type">
										<option value="one_hour" selected="">Last One Hour</option>
										<option value="3_hour" >Last Three Hours</option>
										<option value="one_day">Yesterday</option>
										<option value="custom">Custom</option>
										</select>
								</div>
								<div class="form_field form_input_box custom_sync_date_div">
									<input type="text"  id="custom_sync_date" placeholder="Choose Date" value class="ui-autocomplete-input" autocomplete="off">
								</div>
								<input type="hidden" class="order_from_date"  value="">
								<input type="hidden" class="order_to_date" value="">
								<button class="btn sync-btn">Sync with BC</button>
							</div>
						</div>

						<a class="btn <?php if(($status == 'pending' || $status == 'processing') && strtolower($selected_sync_option) != 'no sync'){
								echo "disabled"; }?>" id="importBtn" href="javascript:void(0);">Import</a>
						<a class="btn" target="_blank" href='https://apps.arizonreports.cloud/Furniture/Bundling_App/export-products?store=<?php echo $data['store']; ?>&search_by_sku=<?php echo $data['sku']; ?>&search_by_name=<?php echo $data['name']; ?>&uom_override=<?php echo $data['uom_override']; ?>&is_bundle_override=<?php echo $data['bundle_override']; ?>&is_bundle_available=<?php echo $data['bundle_available']; ?>&measurement_value=<?php echo $data['measurement_value']; ?>'>Export</a>

						
                        <form id="frm_filter" class="order_form row inventory_form" method="POST">
							<div class="filter_block col-xl-8 col-lg-8 col-md-8">
								<div class="search">
									<div class="txt_box">
										<input type="text" name="search_by_sku" class="txt_search sku_search" placeholder="Product SKU" value="<?php echo $data['sku'];?>" />
										<span class="clear">X</span>
									</div>
								</div>
								<div class="search">
									<div class="txt_box">
										<input type="text" name="search_by_name" class="txt_search product_search" placeholder="Product Name" value='<?php echo $data['name'];?>'/>
										<span class="clear">X</span>
									</div>
								</div>							
							</div>
							<input type="hidden" name="is_uom_override" class="txt_search ins_visiblity is_uom_override" value="<?php echo $data['uom_override'];?>" />
							<input type="hidden" name="is_bundle_override" class="txt_search ins_visiblity is_bundle_override" value="<?php echo $data['bundle_override'];?>" />
							<input type="hidden" name="is_bundle_available" class="txt_search ins_visiblity is_bundle_available" value="<?php echo $data['bundle_available'];?>" />
							<input type="hidden" name="measurement_value" class="txt_search ins_visiblity measurement_value" value="<?php echo $data['measurement_value'];?>" />
							<div class="action_block col-xl-4 col-lg-4 col-md-4">
								<div class="search_block">
									<input type="submit" value="Search" class="btn"/>
								</div>
								<div class="show_all_block">
									<a href="sales_uom?store=<?php echo $store;?>">
									<input type="button" value="Show All" class="btn show_all_btn"/>
									</a>
								</div>
							</div>
							<input type="hidden" class="current_page" name="page_no" value="1">
						</form>

                    </div>
					<table class="uom_product_table" id="uom_product_table">
						<thead>
							<tr>
								<th></th>
                                <th>Product Id</th>
								<th>Product Name</th>
								<th>SKU</th>
								<th>Brand</th>
								<th>Base UOM</th>
								<th>Sales UOM</th>
								<th>Measurement</th>
								<th>UOM<br />Override</th>
								<th>Multi<br />Pack<br />Availability</th>
								<th>Multi<br />Pack<br />Qty</th>
								<th>Multi<br />Pack<br />Override</th>
								<th>Multi<br />Pack<br />Broken Per-centage</th>
								<th>Action</th>
							</tr>
						</thead>
                        <tbody>
                        <tr class="table_data">
								<td class="name">
									<a href="sales_uom?store=<?php echo $store;?>">
										<input type="button" value="" class="btn reset-btn cancel_customer_page">
									</a>
								</td>
                                <td class="name">
								</td>
								<td class="name">
									<div class="filter_block">
										<div class="search">
											<div class="txt_box">
												<input type="text" name="search_by_name" class="txt_search product_search_visible" placeholder="Product Name" value='<?php echo $data['name'];?>' />
												<span class="clear">X</span>
											</div>
										</div>
									</div>
								</td>
								<td class="name">
									<div class="filter_block">
										<div class="search">
											<div class="txt_box">
												<input type="text" name="search_by_sku" class="txt_search sku_search_visible" placeholder="Product SKU" value="<?php echo $data['sku'];?>" />
												<span class="clear">X</span>
											</div>
										</div>
									</div>
								</td>
								<td class="name">
								</td>
								<td class="name">
								</td>
								<td class="name">
								</td>
								<td class="name">
								<select type="text" id="measurement" name="measurement" class="input_text">
										<?php if($data['measurement_value'] == ''){ ?>
											<option selected value="">ALL</option>
										<?php }else{ ?>
											<option value="">ALL</option>
										<?php }?>
										<?php if($data['measurement_value'] == 'Bundle'){ ?>
											<option selected value="Bundle">Bundle</option>
										<?php }else{ ?>
											<option value="Bundle">Bundle</option>
										<?php }?>
										<?php if($data['measurement_value'] == 'Feet'){ ?>
											<option selected value="Feet">Feet</option>
										<?php }else{ ?>
											<option value="Feet">Feet</option>
										<?php }?>
										<?php if($data['measurement_value'] == 'Linear Feet'){ ?>
											<option selected value="Linear Feet">Linear Feet</option>
										<?php }else{ ?>
											<option value="Linear Feet">Linear Feet</option>
										<?php }?>
										<?php if($data['measurement_value'] == 'Pack'){ ?>
											<option selected value="Pack">Pack</option>
										<?php }else{ ?>
											<option value="Pack">Pack</option>
										<?php }?>
										<?php if($data['measurement_value'] == 'Pair'){ ?>
											<option selected value="Pair">Pair</option>
										<?php }else{ ?>
											<option value="Pair">Pair</option>
										<?php }?>
										<?php if($data['measurement_value'] == 'Piece'){ ?>
											<option selected value="Piece">Piece</option>
										<?php }else{ ?>
											<option value="Piece">Piece</option>
										<?php }?>
										<?php if($data['measurement_value'] == 'Set'){ ?>
											<option selected value="Set">set</option>
										<?php }else{ ?>
											<option value="Set">Set</option>
										<?php }?>
									</select>
								</td>
								<td class="name">
                                    <select type="text" id="visibility" name="visibility_search" class="input_text">
										<?php if($data['uom_override'] == ''){ ?>
											<option selected value="">ALL</option>
										<?php }else{ ?>
											<option value="">ALL</option>
										<?php }?>
										<?php if($data['uom_override'] == 'true'){ ?>
											<option selected value="true">YES</option>
										<?php }else{ ?>
											<option value="true">YES</option>
										<?php }?>
										<?php if($data['uom_override'] == 'false'){ ?>
											<option selected value="false">NO</option>
										<?php }else{ ?>
											<option value="false">NO</option>
										<?php }?>
									</select>
								</td>
								
								
								<td class="name">
									<select type="text" id="bundle_available" name="bundle_available" class="input_text">
										<?php if($data['bundle_available'] == ''){ ?>
											<option selected value="">ALL</option>
										<?php }else{ ?>
											<option value="">ALL</option>
										<?php }?>
										<?php if($data['bundle_available'] == 'true'){ ?>
											<option selected value="true">YES</option>
										<?php }else{ ?>
											<option value="true">YES</option>
										<?php }?>
										<?php if($data['bundle_available'] == 'false'){ ?>
											<option selected value="false">NO</option>
										<?php }else{ ?>
											<option value="false">NO</option>
										<?php }?>
									</select>
								</td>
								<td class="name">
								</td>
								<td class="name">
									<select type="text" id="bundle_override" name="bundle_override" class="input_text">
										<?php if($data['bundle_override'] == ''){ ?>
											<option selected value="">ALL</option>
										<?php }else{ ?>
											<option value="">ALL</option>
										<?php }?>
										<?php if($data['bundle_override'] == 'true'){ ?>
											<option selected value="true">YES</option>
										<?php }else{ ?>
											<option value="true">YES</option>
										<?php }?>
										<?php if($data['bundle_override'] == 'false'){ ?>
											<option selected value="false">NO</option>
										<?php }else{ ?>
											<option value="false">NO</option>
										<?php }?>
									</select>
								</td>
								<td class="name">
								</td>
								<td class="name">
								</td>
								
							</tr>

                            <?php if(count($data['product_data']) > 0){$i =0;foreach($data['product_data'] as $index => $sales_uom){
                            $i=$i+1; 
                             if(is_numeric($index)){
                            ?>
                                <tr class="row_<?php echo $i; ?>">
                                    <td>
										<?php  if (count($sales_uom['variation'])!=0) {?>
											<img src="assets/images/Plus-AEADB3.png" alt="Plus-AEADB3" class="plus_img_orders" name="order"/>
											<img src="assets/images/Minus_Black.png" alt="Minus_Black" class="minus_img_orders"/>
										<?php } ?>
									</td>
									<td class="product_id" product_id='<?php echo $sales_uom["destination_product_id"];?>'><?php echo $sales_uom["destination_product_id"]?></td>
                                    <td class="product_name" product_id='<?php echo $sales_uom["destination_product_id"];?>'><?php echo $sales_uom["name"]?></td>
                                    <td><span class="variant_sku" id="<?php echo $sales_uom["product_option_sku_id"];?>"><?php if($sales_uom["sku"] != null){echo $sales_uom["sku"];}else{ echo "--";} ?></span></td> 
                                    <td><span class="brand" ><?php if($sales_uom["brand_name"]!=''){ echo $sales_uom["brand_name"];}else{echo '--';} ?></span></td>
                                    <td><input type="number" name="base_uom" class="base_uom" min="1" value=<?php echo $sales_uom["base_uom"];?> readonly></td>
									<td><input type="number" name="sales_uom" class="sales_uom" min="1" value=<?php echo $sales_uom["sales_uom"];?> readonly></td>
                                    <td>	<select type="text" name="measurement_value" class="measurement_value" disabled>
										<?php if($sales_uom['value'] == ''){ ?>
											<option selected value="">None</option>
										<?php }else{ ?>
											<option value="">none</option>
										<?php }?>
										
										<?php if($sales_uom['value'] == 'Bundle'){ ?>
											<option selected value="Bundle">Bundle</option>
										<?php }else{ ?>
											<option value="Bundle">Bundle</option>
										<?php }?>
										<?php if($sales_uom['value'] == 'Feet'){ ?>
											<option selected value="Feet">Feet</option>
										<?php }else{ ?>
											<option value="Feet">Feet</option>
										<?php }?>
										<?php if($sales_uom['value'] == 'Linear Feet'){ ?>
											<option selected value="Linear Feet">Linear Feet</option>
										<?php }else{ ?>
											<option value="Linear Feet">Linear Feet</option>
										<?php }?>
										<?php if($sales_uom['value'] == 'Pack'){ ?>
											<option selected value="Pack">Pack</option>
										<?php }else{ ?>
											<option value="Pack">Pack</option>
										<?php }?>
										<?php if($sales_uom['value'] == 'Pair'){ ?>
											<option selected value="Pair">Pair</option>
										<?php }else{ ?>
											<option value="Pair">Pair</option>
										<?php }?>
										<?php if($sales_uom['value'] == 'Piece'){ ?>
											<option selected value="Piece">Piece</option>
										<?php }else{ ?>
											<option value="Piece">Piece</option>
										<?php }?>
										<?php if($sales_uom['value'] == 'Set'){ ?>
											<option selected value="Set">set</option>
										<?php }else{ ?>
											<option value="Set">Set</option>
										<?php }?>
									</select> 
									</td>
									<td><input class="uom_override" type="checkbox" name="uom_override"  value="" <?php if($sales_uom["is_uom_override"] == 1){ echo 'checked';}?> disabled >
									
									<td><input class="bundle_available" type="checkbox" name="bundle_available" value="" <?php if($sales_uom["is_bundle_available"] == 1){ echo 'checked';}?> disabled >
                                    <td><input type="number" name="bundle_qty" class="bundle_qty" min="2" value=<?php echo $sales_uom["bundle_qty"];?> readonly></td>
                                    <td><input class="bundle_override" type="checkbox" name="bundle_override" value="" <?php if($sales_uom["is_bundle_override"] == 1){ echo 'checked';}?> disabled >
                                    <td><input type="number" name="broken_percentage" class="broken_percentage" value=<?php echo $sales_uom["bundle_broken_percentage"];?> readonly></td>
                                    <td class="name">
										<?php  if (count($sales_uom['variation'])==0) {?>
											<span class="icon" id="edit" data-toggle="modal" data-target="#contactModal">
												<img class="edit_img" src="assets/images/pencil-edit.png"  class="edit" title="Edit" alt="Edit">
												<span class="save_img_section">
													<a href="">
														<img class="save_img" id="img1" src="assets/images/floppy-disk-green.png" title="save" alt="save">
													</a>
													<a href="">
														<img class="cancel_img" id="img2" src="assets/images/floppy-disk-red.png" title="cancel" alt="cancel">
													</a>
												</span>
											</span>
											<?php } ?>
                                    </td>
                                </td>
                                </tr>
                           
                             <?php } ?>
                             
							 <?php
								
								if(is_array($sales_uom)){
									if (count($sales_uom['variation'])!=0){ ?>
										<tr class="order_list" style="width:100%">
											<td colspan="13" class="detail_list" style="width:100%!important">
												<table id="product-child-table" class="fast-order" cellspacing="0" cellpadding="0">
													<tbody>
													<?php foreach($sales_uom['variation'] as $key=>$value){ ?>
														<tr class="">
														<td>
														</td>
														<td class="product_id" product_id='<?php echo $value["destination_product_id"];?>'><?php echo $value["destination_product_id"]?></td>
														<td class="product_name" product_id='<?php echo $value["destination_product_id"];?>'><?php echo $value["name"]?></td>
														<td><span class="variant_sku" id="<?php echo $value["product_option_sku_id"];?>"><?php if($value["sku"] != null){echo $value["sku"];}else{ echo "--";} ?></span></td> 
														<td><span class="brand" ><?php if($value["brand_name"]!=''){ echo $value["brand_name"];}else{echo '--';} ?></span></td>
														<td><input type="number" name="base_uom" min="1" class="base_uom" value=<?php echo $value["base_uom"];?> readonly></td>
														<td><input type="number" name="sales_uom" min="1" class="sales_uom" value=<?php echo $value["sales_uom"];?> readonly></td>
														<td>	<select type="text" name="measurement_value" class="measurement_value" disabled>
															<?php if($value['value'] == ''){ ?>
																<option selected value="">None</option>
															<?php }else{ ?>
																<option value="">None</option>
															<?php }?>
															<?php if($value['value'] == 'Bundle'){ ?>
																<option selected value="Bundle">Bundle</option>
															<?php }else{ ?>
																<option value="Bundle">Bundle</option>
															<?php }?>
															<?php if($value['value'] == 'Feet'){ ?>
																<option selected value="Feet">Feet</option>
															<?php }else{ ?>
																<option value="Feet">Feet</option>
															<?php }?>
															<?php if($value['value'] == 'Linear Feet'){ ?>
																<option selected value="Linear Feet">Linear Feet</option>
															<?php }else{ ?>
																<option value="Linear Feet">Linear Feet</option>
															<?php }?>
															<?php if($value['value'] == 'Pack'){ ?>
																<option selected value="Pack">Pack</option>
															<?php }else{ ?>
																<option value="Pack">Pack</option>
															<?php }?>
															<?php if($value['value'] == 'Pair'){ ?>
																<option selected value="Pair">Pair</option>
															<?php }else{ ?>
																<option value="Pair">Pair</option>
															<?php }?>
															<?php if($value['value'] == 'Piece'){ ?>
																<option selected value="Piece">Piece</option>
															<?php }else{ ?>
																<option value="Piece">Piece</option>
															<?php }?>
															<?php if($value['value'] == 'Set'){ ?>
																<option selected value="Set">set</option>
															<?php }else{ ?>
																<option value="Set">Set</option>
															<?php }?>
														</select> 
														</td>
														<td><input class="uom_override" type="checkbox" name="uom_override" value="" <?php if($value["is_uom_override"] == 1){ echo 'checked';}?> disabled >
														
														<td><input class="bundle_available" type="checkbox" name="bundle_available" value="" <?php if($value["is_bundle_available"] == 1){ echo 'checked';}?> disabled >
														<td><input type="number" name="bundle_qty" min="2" class="bundle_qty" value=<?php echo $value["bundle_qty"];?> readonly></td>
														<td><input class="bundle_override" type="checkbox" name="bundle_override" value="" <?php if($value["is_bundle_override"] == 1){ echo 'checked';}?> disabled >
														<td><input type="number" name="broken_percentage" class="broken_percentage" value=<?php echo $value["bundle_broken_percentage"];?> readonly></td>
														
														<td class="name">
															<span class="icon" id="edit" data-toggle="modal" data-target="#contactModal">
																<img class="edit_img" src="assets/images/pencil-edit.png"  class="edit" title="Edit" alt="Edit">
																<span class="save_img_section">
																	<a href="">
																		<img class="save_img" id="img1" src="assets/images/floppy-disk-green.png" title="save" alt="save">
																	</a>
																	<a href="">
																		<img class="cancel_img" id="img2" src="assets/images/floppy-disk-red.png" title="cancel" alt="cancel">
																	</a>
																</span>
															</span>
														</td>
													</td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</td>
									</tr>
								<?php  }
							} } } else{ ?>
                         
                                <tr class="table-data">
                                    <td colspan=13 style='text-align:center;'>No results found</td>
                                </tr>
						  
<?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
	<div id="pagination-demo">	
	<?php if($data['total_count'] > 0 ){ 
			$no_of_pages = ceil($data['total_count'] / 15);
		  }else{
			$no_of_pages = 0;
		  }
		  $start = 1;
		  $end = 5;
		  if($data['active'] > 4) {
			  $start =$data['active']-2; 
			  $end = $start+4;
		  } 
		  if($no_of_pages !=0 && $no_of_pages !=1) { 
			  if ($end > $no_of_pages) {
				  $end = $no_of_pages;
			  }
		  ?>
	<input type='hidden' class='total_count' value='<?php echo $no_of_pages?>'>
	<div class='pagination'><ul class='pagination_list'>
		<li class='previous pagination_uom_li'><a>Previous</a></li>
		<?php $i=1; for ($i = $start; $i <= $end; $i++) {?>
		<li class='<?php echo $i; if($i== $data['active'] || $i==1 && $data['active'] == null){echo ' active';}?> pagination_uom_li page_no' id='"<?php echo $i ?>"'><a><?php echo $i ?></a></li>
		<?php } ?><li class='next pagination_uom_li'><a>Next</a></li></ul></div> <?php }?>
	</div>
</div>
</section>
<div id="importModal" class="importModal">
	<div class="modal-content">
		<div class="close_section">
				<h2>ImportProduct Details</h2>
				<span class="close">&times;</span>
		</div>
			<div class="content_inner_section">
				<p>Please upload a csv file with the following columns:</p>
				<p style="color:red">* Product Id, SKU is mandatory fields</p>
				<div class="button_Section">
					<a class="btn" id="download_location" target="_blank" href="<?php echo $import_template_url; ?>">Download</a>
					<input type="file" id="fileUpload" />
					<input type="button" class="btn" id="upload" value="Upload" />
					
				</div>
			</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal-error-inventory">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">				
				<h3 class="modal-title" id="myModalLabel">Import completed sucessfully</h3>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="modal-btn-ok--pickup-details">OK</button>							
			</div>
		</div>
	</div>
</div>
<div class="loading">Loading&#8230;</div>





