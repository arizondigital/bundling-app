<?php 
$store = $data['store'];
include_once "header.php";
//echo "product_output<pre>";print_r($data);echo "</pre>";
//echo count($data);
?>

<!--<?php if(count($data) > 0){?>-->
<section class="main_container edit_sales_uom sales_uom">
	<div class="container">
		<section class="main_container">
			<div class="container">
				<h2 class="heading">Edit</h2>
				<div class="table_section">
					<table class="uom_product_table">
						<thead>
							<tr>
								<th>S.No</th>
								<th>Product Id</th>
								<th>Product Name</th>
								<th>SKU</th>
								<th>Brand</th>
								<th>Base Uom</th>
								<th>Sales Uom</th>
								<th>Rounding Rule</th>
								<th>UOM Override</th>
							</tr>
						</thead>
                        <tbody>
                            <?php $i =0; foreach($data as $index => $sales_uom){
                            $i=$i+1;
                             if(is_numeric($index)){
                            ?>
                                <tr class="row_<?php echo $i; ?>">
                                    <td><?php echo $i;?></td>
									<td class="product_id" product_id='<?php echo $sales_uom["destination_product_id"];?>'><?php echo $sales_uom["destination_product_id"]?></td>
                                    <td class="product_name" product_id='<?php echo $sales_uom["destination_product_id"];?>'><?php echo $sales_uom["name"]?></td>
                                    <td><span class="variant_sku" id="<?php echo $sales_uom["product_option_sku_id"];?>"><?php if($sales_uom["sku"] != null){echo $sales_uom["sku"];}else{ echo "--";} ?></span></td> 
                                    <td><span class="brand" ><?php  echo "--"; ?></span></td>
                                    <td><input type="number" name="base_uom" class="base_uom" value=<?php echo $sales_uom["base_uom"];?>></td>
									<td><input type="number" name="sales_uom" class="sales_uom" value=<?php echo $sales_uom["sales_uom"];?>></td>
                                    <td><span class="Rounding Rule" ><?php echo "--";?></span></td>
                                    <td><input class="uom_override" type="checkbox" id="uom_override" name="uom_override" value="" <?php if($sales_uom["is_uom_override"] == 1){ echo 'checked';}?> >
									</td>
                                </tr>
                            <?php } } ?>  
                        </tbody>
                    </table>
                </div>
				<div class="button_block">
					<input type="button" class="btn edit_product_button" value="Save">
					<input type="button" class="btn cancel_product_button" value="Cancel">
				</div>
			</div>
		</section>
	</div>
</section>
<!--<?php }else{?>
<section class="main_container edit_bundle_kit" style="text-align:center;margin-top:50px;"><h5> The Collection Sku is not exist in Bigcommerce </h5></section>
<?php }?>-->