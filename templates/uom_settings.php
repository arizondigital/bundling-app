<?php 
$store = $data['store'];
include_once "header.php";
//echo "data<pre>";print_r($data);echo "</pre>";
?>

<section class="main_container">
		<div class="container uom_settings_page">
			<h2 class="heading">UOM SETTINGS</h2>
		</div>
		<div class="add_page_container">
			<!--<form action="save_form_labels" class="frm_labels" id="frm_labels" method="POST">-->
                <div class="form_field_container">
					<label>Unit of Measurement</label>
                    <select name="uom" id="uom_types">
                        <option value="">Please select the unit of measurement</option>
                        <option value="base" <?php if(strtolower($data[0]["param_buy_in_base_sale_uom"]) =="base"){echo "selected";} ?>>Base UOM</option>
                        <option value="sale" <?php if(strtolower($data[0]["param_buy_in_base_sale_uom"]) == "sale"){echo "selected";} ?>>Sales UOM</option>
                    </select>
				</div>
                <div class="form_field_container">
					<label >Display UOM conversion in PDP</label>
					<label style="margin-right:10px;" class="switch">
						<input type="checkbox" id="uom_conversion"  <?php if(strtolower($data[0]["param_display_uom_coversion_details"]) =="1"){echo "checked";} ?> class="<?php if(strtolower($data[0]["param_display_uom_coversion_details"]) =="true"){echo "checked";}else{echo "unchecked";}?>bundle_display switch-display">
						<span class="slider round"></span>
					</label>
				</div>
                <div class="form_field_container">
					<label >Cart Quantity Restriction</label>
					<label style="margin-right:10px;" class="switch">
						<input type="checkbox" id="quantity_restriction" <?php if(strtolower($data[0]["param_restrict_qty_change_cart"]) =="1"){echo "checked";} ?> class="<?php if(strtolower($data[0]["param_restrict_qty_change_cart"]) =="true"){echo "checked";}else{echo "unchecked";}?> bundle_display switch-display">
						<span class="slider round"></span>
					</label>
				</div>

				<div class="button_block">		
						<input type="button"  class="btn save_uom_settings" value="Save">
				</div>
			<!--</form>-->
		</div>
</section>
