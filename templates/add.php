<?php 
$store = $data['store'];
include_once "header.php";
?>
<section class="main_container add_bundle_kit_page">
	<div class="container">
		<section class="main_container">
			<div class="container">
				<h2 class="heading">Add Bundle/Kit</h2>
				<div class="add_page_container">
					<div class="form_field_container">
						<label>Bundle/Kit Name:</label>
						<input type="text" name="Bundle_Kit_Name" id="Bundle_Kit_Name" placeholder="Bundle/Kit Name" value="" class="ui-autocomplete-input" autocomplete="off">
						<input type="hidden" id="page_value" value="add_bundle_kit_page">
					</div>
					<div class="form_field_container">
						<label>Bundle/Kit SKU:</label>
						<input type="text" name="Bundle_Kit_SKU" id="Bundle_Kit_SKU" placeholder="Bundle/Kit SKU" value="" class="ui-autocomplete-input" autocomplete="off" >
					</div>
					<div class="form_field_container">
						<label>Product Type:</label>
						<select name="product_type" id="product_type">
							<option value="">Product type</option>
							<option value="Bundle">Bundle</option>
							<option value="Kit">Kit</option>
						</select>
					</div>
					<div class="form_add_container form_field_container">
						<label>Select Product:</label>
						<div class="expand_section add_section">
							<input type="text" name="product_name" id="product_name" placeholder="Select Bigcommerce Product for the Bundle/Kit" value="" class="ui-autocomplete-input" autocomplete="off">
							<a href="javascript:void(0);" class="btn product_add_btn">Add</a>
						</div>
					</div>
				</div>
				<div class="table_section add_bundle_table">
					<table>
						<thead>
							<tr>
							<th>S.No</th>
							<th>Product Name</th>
							<th>SKU</th>
							<th>Price</th>
							<th>Mandatory</th>
							<th class="th-qty">Qty</th>
							<th>Price Adjustment </th>
							<th>Adjusted Price </th>
							<th>Total Price </th>
							<th>Data Source</th>
							<th>Action </th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
			</div>
			<div class="button_block">
				<input type="button" class="btn edit_and_save_buttton" value="Save">
				<input type="button" class="btn cancel_button" value="Cancel">
			</div>
			</div>
		</section>
	</div>
</section>
