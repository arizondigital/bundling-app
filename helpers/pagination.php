<?php

function pagination($page,$total_pages,$limit,$adjacents){

    if($page) 
        $start = ($page - 1) * $limit;        
    else
        $start = 0;                            

    if ($page == 0) $page = 1;                  
    $prev = $page - 1;                         
    $next = $page + 1;                          
    $lastpage = ceil($total_pages/$limit);
    $lpm1 = $lastpage - 1;                      
    $pagination = "";
	
    if($lastpage > 1)
    {  

        if ($page > 1) 
            $pagination.= "<li class='page-item prev'><a href='javascript:void(0)' class='page-link'  data-id='".$prev."'>Previous</a></li>";
        else
            $pagination.= '<li class="page-item prev"><span class="page-link disabled"> previous</span></li>'; 
        if ($lastpage < 7 + ($adjacents * 2))
        {   
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= '<li class="page-item active"><span class="current page-link"  data-id='.$counter.'>'.$counter.'</span></li>';
                else
                    $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id='.$counter.'>'.$counter.'</a></li>';                 
            }
        }
        elseif($lastpage > 5 + ($adjacents * 2))
        {
              if($page < 1 + ($adjacents * 2))     
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
                    if ($counter == $page)
                        $pagination.= '<li class="page-item active"><span class="current page-link"  data-id='.$counter.'>'.$counter.'</span></li>';
                    else
                        $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id='.$counter.'>'.$counter.'</a></li>';                 
                }
                $pagination.= "...";
                $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id='.$lpm1.'>'.$lpm1.'</a></li>';
                $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id='.$lastpage.'>'.$lastpage.'</a>';       
            }
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id=1>1</a></li>';
                $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id=2>2</a></li>';
                $pagination.= "...";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= '<li class="page-item active"><span class="page-link current" data-id='.$counter.'>'.$counter.'</span></li>';
                    else
                        $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id='.$counter.'>'.$counter.'</a></li>';                 
                }
                $pagination.= "...";
                $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id='.$lpm1.'>'.$lpm1.'</a></li>';
                $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id='.$lastpage.'>'.$lastpage.'</a>';     
            }
            else
            {
                $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id=1>1</a></li>';
                $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id=2>2</a></li>';
                $pagination.= "...";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= '<li class="page-item active"><a class="page-link current" data-id='.$counter.'>'.$counter.'</span></li>';
                    else
                        $pagination.= '<li class="page-item extra_page pagination_nums"><a class="page-link" href="javascript:void(0)" data-id='.$counter.'>'.$counter.'</a></li>';                 
                }
            }
        }
        if ($page < $counter - 1) 
            $pagination.= '<li class="page-item next"><a class="page-link" href="javascript:void(0)" data-id='.$next.'>next </a>';
        else
            $pagination.= '<li class="page-item"><a class="disabled page-link" href="javascript:void(0)" data-id='.($next-1).'>next</a>';
    
    }
	echo $pagination;
}
?>
