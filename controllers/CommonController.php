<?php
include_once 'models/CommonModel.php';
class CommonController
{
	function __construct()
	{
		$this->commonModel = new CommonModel();
	}

	function getCategoryProductDetails($request,$store_hash,$client_id,$access_token,$storeId){
		$customer_group_id = $request->get("customer_group_id")?$request->get("customer_group_id"):null;
		$customer_group_current_page = $request->get("customer_group_current_page");
		if($request->get('customer_group_current_page')!=null || $request->get('customer_group_current_page')!=""){
			if($request->get('customer_group_current_page') != 1){
			$pageNumber=($request->get('customer_group_current_page') - 1) * 100;
		}else{
			$pageNumber=1;
		}
			$page_end = $pageNumber + 99;
		}else{
			$pageNumber=1;
			$page_end = 100;
			$customer_group_current_page = 1;
		}
		$product_name_search = $request->get("product_name_search")?$request->get("product_name_search"):null;
		$category_name_search = $request->get("category_search")?$request->get("category_search"):null;
		$brand_name_search = $request->get("brand_search")?$request->get("brand_search"):null;
		$visibility = ($request->get("visibility_search")!="")?$request->get("visibility_search"):null;
		$product_sku_search = ($request->get("product_sku_search")!="")?$request->get("product_sku_search"):null;
		$input_data = array("page_start"=>$pageNumber,"page_end"=>$page_end,"bc_customer_group_id"=>$customer_group_id,"bc_category_id"=>null,"product_name_search"=>$product_name_search,"category_name_search"=>$category_name_search,"brand_name_search"=>$brand_name_search,"product_is_visible"=>$visibility
		,"product_sku_search"=>$product_sku_search,"app_installed_store_id"=>$storeId);
		//echo "<pre>";print_r($input_data);echo "</pre>";
		$result_data = $this->commonModel->getProductCustomergroups($input_data);
		/*echo "result_data<pre>";
		print_r($result_data);
		echo "</pre>";*/
		$row_count = 0;
		$customer_group_name='';
		$result = Array();
		$product_array = Array();
		foreach($result_data as $key => $products){
			//echo $products['product_id'];die;
			//$products['product_id'] = $products;
			
			/*$product_array[$products['product_id']]['product_name'] = $products['product_name'];
			$product_array[$products['product_id']]['is_active'] = $products['is_active'];
			$product_array[$products['product_id']]['category_name'] = $products['category_name'];
			$product_array[$products['product_id']]['brand_name'] = $products['brand_name'];
			$product_array[$products['product_id']]['parent_sku'] = $products['parent_sku'];
			$product_array[$products['product_id']]['sku'] = $products['sku'];
			$product_array[$products['product_id']]['options'] = $products['options'];
			$product_array[$products['product_id']]['total_row'] = $products['total_row'];*/
			$product_array[$products['product_id']]['product_name'] = $products['product_name'];
			$product_array[$products['product_id']]['is_active'] = $products['is_active'];
			$product_array[$products['product_id']]['category_name'] = $products['category_name'];
			$product_array[$products['product_id']]['brand_name'] = $products['brand_name'];
			$product_array[$products['product_id']]['parent_sku'] = $products['parent_sku'];
			$product_array[$products['product_id']]['child'][$products['sku']]['sku'] = $products['sku'];
			$product_array[$products['product_id']]['child'][$products['sku']]['options'] = $products['options'];
			$product_array[$products['product_id']]['total_row'] = $products['total_row'];
			$product_array[$products['product_id']]['child_sku'] = $products['child_sku'];
			$row_count = $products['total_row'];
			
		}
		/*echo "products<pre>";
		print_r($product_array);
		echo "</pre>";*/
		$input = array("bc_customer_group_id"=>$customer_group_id,"app_installed_store_id"=>$storeId);
		$customer_group_name=$this->commonModel->getCustomerGroupName($input);
		$result['product_array'] = $product_array;
		$result['product_name'] = $product_name_search;
		$result['product_sku'] = $product_sku_search;
		$result['category'] = $category_name_search;
		$result['brand'] = $brand_name_search;
		$result['visibility'] = $visibility;
		$result['customer_group_name'] = $customer_group_name['0']['param_customer_group_name'];
		$result['row_count'] = $row_count;
		$result['active_page'] = $customer_group_current_page;		

		return $result;

	}
	function getBundleKitList($request,$storeId,$type){
		
		$bundle_kit_name = $request->get("bundle_kit_name")?$request->get("bundle_kit_name"):null;
		//$type = $request->get("type_status")?$request->get("type_status"):$type_data;
		//echo $type_data;echo "<br>";
		$count = 15;
		if($type == 'both'){
			$type = null;
		}
		//$page_no = $request->get("page_no")?$request->get("page_no"):null;
		$current_page = $request->get("page_no");
		if($request->get('page_no')!=null || $request->get('page_no')!=""){
				
		}else{
			$current_page = 1;
		}
		$bundle_kit_input = array("bundle_kit_name"=>$bundle_kit_name,"type"=>$type,"count"=>$count,"page_no"=>$current_page,"app_installed_store_id"=>$storeId);
		//echo "bundle_kit_input<pre>";print_r($bundle_kit_input);echo "</pre>";
		$bundle_kit_output = $this->commonModel->getBundleKitList($bundle_kit_input);
		$result['bundle_kit'] = $bundle_kit_output;
		$result['bundle_kit_name'] = $bundle_kit_name;
		$result['type'] = $type;
		$result['active'] = $current_page;
		

		//echo "bundle_kit_output<pre>";print_r($bundle_kit_output);echo "</pre>";
		return $result;	
	}
	function editBundleKitList($request,$storeId,$id){
		$edit_bundle_kit_input = array("bundle_kit_id"=>$id,"app_installed_store_id"=>$storeId);
		//echo "edit_bundle_kit_input<pre>";print_r($edit_bundle_kit_input);echo "</pre>";
		$edit_bundle_kit_output = $this->commonModel->editBundleKitList($edit_bundle_kit_input);

		//echo "edit_bundle_kit_output<pre>";print_r($edit_bundle_kit_output);echo "</pre>";
		return $edit_bundle_kit_output;	
	}
	//uom update
	function getSalesUom($request, $storeId, $productId, $sku){
		$offset_count = null;
		$count = null;
		$product_name = $request->get("search_by_name")?$request->get("search_by_name"):null;
		$sku = $request->get("search_by_sku")?$request->get("search_by_sku"):null;
		$uom_override = $request->get("is_uom_override")?$request->get("is_uom_override"):null;
		$bundle_override = $request->get("is_bundle_override")?$request->get("is_bundle_override"):null;
		$is_bundle_available = $request->get("is_bundle_available")?$request->get("is_bundle_available"):null;
		$measurement_value= $request->get("measurement_value")?$request->get("measurement_value"):null;
		$total_count_input = array("page_offset"=>$offset_count,"page_limit"=>$count,"product_name"=>$product_name,"is_uom_override"=>$uom_override,"product_id"=>$productId, "sku"=>$sku , "app_installed_store_id"=>$storeId,"is_bundle_override"=>$bundle_override,"is_bundle_available"=>$is_bundle_available,"measurement_value"=>$measurement_value);
		//echo "edit_bundle_kit_input<pre>";print_r($store_input);echo "</pre>";
		$total_count_output['product_data'] = $this->commonModel->getSalesUom($total_count_input);
		$count = 15;
		$offset_count = 0;
		$page_no = $request->get("page_no")?$request->get("page_no"):null;
		if($page_no > 1){
			$offset_count = ($page_no-1)*$count;
		}
		$store_input = array("page_offset"=>$offset_count,"page_limit"=>$count,"product_name"=>$product_name,"is_uom_override"=>$uom_override,"product_id"=>$productId, "sku"=>$sku , "app_installed_store_id"=>$storeId,"is_bundle_override"=>$bundle_override,"is_bundle_available"=>$is_bundle_available, "measurement_value"=>$measurement_value);
		//echo "store_input<pre>";print_r($store_input);echo "</pre>";
		$product_data=[];
		$store_output = $this->commonModel->getSalesUom($store_input);
		foreach($store_output as $key =>$value){
			
			//$sync_option_type = $value['sync_option'];
			if($value['product_option_sku_id'] == null  || $value['product_option_sku_id'] == ''){
				$product_data[$value['product_id']] = $value;
				$temp = Array();
				foreach($store_output as $key_1 =>$value_1){
					if($value_1['product_id'] == $value['product_id'] && $value_1['product_option_sku_id']!= null && $value_1['product_option_sku_id']!=''){
						$temp[] = $value_1;
					}
				}
				$product_data[$value['product_id']]['variation'] = $temp;
			}
		}
		$final_output['product_data']=$product_data;
		$final_output['name'] = $product_name;
		$final_output['sku'] = $sku;
		$final_output['uom_override'] = $uom_override;
		$final_output['bundle_override'] = $bundle_override;
		$final_output['bundle_available'] = $is_bundle_available;
		$final_output['measurement_value'] = $measurement_value;
		$final_output['active'] = $page_no;
		$final_output['total_count'] = count($total_count_output['product_data']);
		//echo "edit_bundle_kit_output<pre>";print_r($final_output);echo "</pre>";die;
		return $final_output;	
	}
	function ManageSalesUomProduct($request, $storeId, $productId, $sku, $base_uom, $sales_uom, $is_uom_override, $bundle_override, $bundle_available, $bundle_qty, $broken_percentage, $measurement_value){
		$store_input = array("product_id"=>$productId, "sku"=>$sku ,"base_uom"=>$base_uom,"sales_uom"=>$sales_uom,"is_uom_override"=>$is_uom_override, "app_installed_store_id"=>$storeId, "is_bundle_override"=>$bundle_override, "is_bundle_available"=>$bundle_available, "bundle_qty"=>$bundle_qty, "broken_percentage"=>$broken_percentage, "measurement_value"=>$measurement_value);
		//echo "edit_bundle_kit_input<pre>";print_r($store_input);echo "</pre>";
		$store_output = $this->commonModel->ManageSalesUomProduct($store_input);
		//echo "edit_bundle_kit_output<pre>";print_r($edit_bundle_kit_output);echo "</pre>";
		return $store_output;	
	}
	function getUomSettings($request,$storeId){

		$input_store_settings["app_installed_store_id"]=$storeId;
		$output_settings = $this->commonModel->getUomSettings($input_store_settings);
		return $output_settings;
	}
	function manageUomSettings($request,$storeId,$buy_in_base_sale_uom,$restrict_qty_change_cart,$display_uom_conversion_detail){
		$input_store_settings["param_buy_in_base_sale_uom"]=$buy_in_base_sale_uom;
		$input_store_settings["param_restrict_qty_change_cart"]=$restrict_qty_change_cart;
		$input_store_settings["param_display_uom_coversion_details"]=$display_uom_conversion_detail;
		$input_store_settings["app_installed_store_id"]=$storeId;
		$output_settings = $this->commonModel->manageUomSettings($input_store_settings);
		return $output_settings;

	}
	function importProducts($request, $storeId){
			
		$data = json_decode($request->get('importData'));
		//$ip_address = $request->get('ip_address');
		//$user_id = $request->get('user_id');
		//print_r($data);
		foreach($data as $product){
			$productId = $product->productId? $product->productId : null;
			$sku = $product->sku? $product->sku : null;
			$base_uom= $product->baseUOM != ''? $product->baseUOM : null;
			$sales_uom = $product->salesUOM? $product->salesUOM : null;
			$is_uom_override = $product->UOMOverride? $product->UOMOverride : null;
			$bundle_available = $product->bundleAvailable? $product->bundleAvailable : null;
			$bundle_qty = $product->bundleQty? $product->bundleQty : null;
			$bundle_override = $product->bundleOverride? $product->bundleOverride : null;
			$broken_percentage = $product->brokenBundlePercentage? $product->brokenBundlePercentage : null;
			$measurement_value = $product->measurement? $product->measurement : null;
			$app_installed_store_id = $storeId;
			$inputData = array("product_id"=>$productId, "sku"=>$sku ,"base_uom"=>$base_uom,"sales_uom"=>$sales_uom,"is_uom_override"=>$is_uom_override, "app_installed_store_id"=>$storeId, "is_bundle_override"=>$bundle_override, "is_bundle_available"=>$bundle_available, "bundle_qty"=>$bundle_qty, "broken_percentage"=>$broken_percentage, "measurement_value"=>$measurement_value);
			//$result = $this->secondaryadminModel->importPro($inputData);
			$result = $this->commonModel->ManageSalesUomProduct($inputData);

			echo "<pre>";
			print_r($inputData);
			print_r($result);
			echo "</pre>";
			

			
		}

		return true;
	}
	function createExportProducts($request,$storeId){

		//$location_id = $request->get('location_id');
		//$location_name = $request->get('location_name');
		//$total_pages=0;
		//$start =  $start_limit+1;
		//$end = $end_limit;
		
		$sku = $request->get("search_by_sku")?$request->get("search_by_sku"):null;
		$name = $request->get("search_by_name")?$request->get("search_by_name"):null;
		$is_uom_override = $request->get("uom_override")?$request->get("uom_override"):null;
		$bundle_override = $request->get("is_bundle_override")?$request->get("is_bundle_override"):null;
		$is_bundle_available = $request->get("is_bundle_available")?$request->get("is_bundle_available"):null;
		$measurement_value= $request->get("measurement_value")?$request->get("measurement_value"):null;
		//$total_count_input = array("page_offset"=>$offset_count,"page_limit"=>$count,"product_name"=>$product_name,"is_uom_override"=>$uom_override,"product_id"=>$productId, "sku"=>$sku , "app_installed_store_id"=>$storeId,"is_bundle_override"=>$bundle_override,"is_bundle_available"=>$is_bundle_available);
		
		$result = array();
		$inputData = array("page_offset"=>null,"page_limit"=>null,"product_name"=>$name,"is_uom_override"=>$is_uom_override,"product_id"=>null, "sku"=>$sku , "app_installed_store_id"=>$storeId,"is_bundle_override"=>$bundle_override,"is_bundle_available"=>$is_bundle_available,"measurement_value"=>$measurement_value);

		$fileName = 'Export_products';

		$data = $this->commonModel->getSalesUom($inputData);
		
		/*$product_data_for_export = Array();
		foreach($data as $key =>$value){
			$product_data_for_export[$value['product_id']] = $value;
			if($value['is_child_sku'] == 'true'){
				$temp = Array();
				foreach($data as $key_1 =>$value_1){
					if($value_1['product_id'] == $value['product_id']){
						$temp[] = $value_1;
					}
				}
				$product_data_for_export[$value['product_id']]['variation'] = $temp;
			}
		}*/
		
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";die;*/
		ob_clean();
		ob_end_clean();
		header('Content-type: application/csv');
		header("Content-Disposition: attachment; filename=".$fileName.".csv");
		
		$output = fopen("php://output", "w");
		fputcsv($output, array('PRODUCT ID', 'PRODUCT NAME', 'SKU', 'BRAND', 'BASE UOM', 'SALES UOM', 'MEASUREMENT' , 'UOM OVERRIDE','BUNDLE AVAILABLE', 'BUNDLE QTY', 'BUNDLE OVERRRIDE', 'BROKEN BUNDLE PERCENTAGE' ));

		if(count($data)!=0){
			foreach($data as $product){
				$id = $product['destination_product_id']? $product['destination_product_id'] : null;
				$name = $product['name']? $product['name'] : null;
				$sku = $product['sku']? $product['sku']: null;
				$brand = null;
				$base_uom = $product['base_uom']? $product['base_uom'] : null;
				$sales_uom = $product['sales_uom']? $product['sales_uom'] : null; 
				$measurement = $product['value']? $product['value'] : null; 
				$is_uom_override = $product['is_uom_override']? 'Yes' : 'No';
				$is_bundle_override = $product['is_bundle_override']? 'Yes' : 'No';
				$is_bundle_available = $product['is_bundle_available']? 'Yes' : 'No';
				$bundle_qty = $product['bundle_qty']? $product['bundle_qty'] : null;
				$bundle_broken_percentage = $product['bundle_broken_percentage']? $product['bundle_broken_percentage'] : null;
				fputcsv($output, array($id, $name, $sku, $brand, $base_uom, $sales_uom, $measurement, $is_uom_override, $is_bundle_available, $bundle_qty, $is_bundle_override, $bundle_broken_percentage));

				/*if (array_key_exists("variation",$product_data)){
					foreach($product_data['variation'] as $child_data){

						if($child_data["stock_level"] == ''){
							$child_stock = 0;
						}else{
							$child_stock = $child_data["stock_level"];
						}
						if($child_data["buffer_stock"] == ''){
							$child_buffer_stock = 0;
						}else{
							$child_buffer_stock = $child_data["buffer_stock"];
						}

						$available_stock_sku = (int)$child_stock-$child_buffer_stock;
						if($available_stock_sku < 0){
							$available_stock_sku = 0;
						}

						fputcsv($output, array("SKU", $child_data["sku"], $child_stock, $child_buffer_stock, '', '', $child_data["blocked_pickup_count"],  $available_stock_sku));
					}
				}*/
			}
		}else{
			echo "No Data Found";
		}
		
		fclose($output);
		die;
		
	}

	//uom update
	function getProductDetails($request,$storeId){
			$product_name = $request->get("product_name")?$request->get("product_name"):null;
			$bundle_product_input = array("product_name"=>$product_name,"app_installed_store_id"=>$storeId,"page_limit"=>0,"page_offset"=>10);
			//echo "bundle_product_input<pre>";print_r($bundle_product_input);echo "</pre>";

			$bundle_product_outuput = $this->commonModel->getProductDetails($bundle_product_input);

			//echo "bundle_product_outuput<pre>";print_r($bundle_product_outuput);echo "</pre>";die;
			
			$product_details = array();
			if(count($bundle_product_outuput)>0){
				foreach($bundle_product_outuput as $key => $value){
					$product_details[$key]['id']=$key;
					$product_details[$key]['label'] = $value["product_name"];
					$product_details[$key]['price'] = $value["price"];
					$product_details[$key]['product_id'] = $value["product_id"];
					$product_details[$key]['product_sku'] = $value["product_sku"];
					$product_details[$key]['bundle_product_type'] = $value["bundle_product_type"];
				}
			}
			//echo "product_details<pre>";print_r($product_details);echo "</pre>";
			return $product_details;	
	}
	function getSkuDetails($request,$storeId){
			$product_id = $request->get("product_id")?$request->get("product_id"):null;
			$bundle_product_input = array("product_id"=>$product_id,"app_installed_store_id"=>$storeId);
			//echo "bundle_product_input<pre>";print_r($bundle_product_input);echo "</pre>";

			$bundle_product_outuput = $this->commonModel->getSkuDetails($bundle_product_input);

		//	echo "bundle_product_outuput<pre>";print_r($bundle_product_outuput);echo "</pre>";die;
			
		
			//echo "product_details<pre>";print_r($product_details);echo "</pre>";
			return $bundle_product_outuput;	
	}
	function saveBundleKit($request,$storeId){
		$bundle_kit_id = $request->get("bundle_kit_id")?$request->get("bundle_kit_id"):null;
		$bundle_kit_name = $request->get("bundle_kit_name")?$request->get("bundle_kit_name"):null;
		$type = $request->get("type")?$request->get("type"):null;
		$bundle_kit_sku = $request->get("bundle_kit_sku")?$request->get("bundle_kit_sku"):null;
		//product_details
		$product_details = $request->get("product_details")?$request->get("product_details"):null;
		
		 //echo $product_details;die;
		//echo json_decode(array("product_details"=>$product_details));
		$save_bundle_kit_input = array("bundle_kit_id"=>$bundle_kit_id,"bundle_kit_name"=>$bundle_kit_name,"type"=>$type,"product_details"=>$product_details,"app_installed_store_id"=>$storeId,"bundle_sku"=>$bundle_kit_sku);
		//echo "save_bundle_kit_input<pre>";print_r($save_bundle_kit_input);echo "</pre>";
		$save_bundle_kit_output = $this->commonModel->saveBundleKit($save_bundle_kit_input);
		//echo "save_bundle_kit_output<pre>";print_r($save_bundle_kit_output);echo "</pre>";

		return $save_bundle_kit_output;
	}
	function deleteBundleKit($request,$storeId){
		$bundle_kit_id = $request->get("bundle_kit_id")?$request->get("bundle_kit_id"):null;
		$delete_bundle_kit_input = array("bundle_kit_id"=>$bundle_kit_id,"app_installed_store_id"=>$storeId);
		$delete_bundle_kit_output = $this->commonModel->deleteBundleKitList($delete_bundle_kit_input);
		
		//echo "edit_bundle_kit_output<pre>";print_r($edit_bundle_kit_output);echo "</pre>";
		return $delete_bundle_kit_output;	
		
	}
	function deleteProductBundleKit($request,$storeId){
		$bundle_kit_id = $request->get("bundle_kit_id")?$request->get("bundle_kit_id"):null;
		$product_id = $request->get("product_id")?$request->get("product_id"):null;
		$delete_product_bundle_kit_input = array("bundle_kit_id"=>$bundle_kit_id,"product_id"=>$product_id,"app_installed_store_id"=>$storeId);
		//echo "delete_product_bundle_kit_input<pre>";print_r($delete_product_bundle_kit_input);echo "</pre>";
		$delete_product_bundle_kit_output = $this->commonModel->sp_get_delete_bundle_product_kit ($delete_product_bundle_kit_input);
		
		//echo "edit_bundle_kit_output<pre>";print_r($delete_product_bundle_kit_output);echo "</pre>";
		return $delete_product_bundle_kit_output;	
	}
	function deleteBundle($request,$storeId){
		$bundle_kit_id = $request->get("bundle_kit_id")?$request->get("bundle_kit_id"):null;
		$delete_product_bundle_kit_input = array("bundle_kit_id"=>$bundle_kit_id,"app_installed_store_id"=>$storeId);
		//echo "delete_product_bundle_kit_input<pre>";print_r($delete_product_bundle_kit_input);echo "</pre>";
		$delete_product_bundle_kit_output = $this->commonModel->sp_delete_bundle_kit($delete_product_bundle_kit_input);
		
		//echo "edit_bundle_kit_output<pre>";print_r($delete_product_bundle_kit_output);echo "</pre>";die;
		return $delete_product_bundle_kit_output;	
	}
	function getSettings($request,$storeId){

		$input_store_settings["app_installed_store_id"]=$storeId;
		$output_settings = $this->commonModel->getSettings($input_store_settings);
		return $output_settings;
	}
	function manageSettings($request,$storeId){
		$display_title =$request->get("title")?$request->get("title"):null;
		$product_display = $request->get("view")?$request->get("view"):null;
		$custom_field_name = $request->get("custom_field_value")?$request->get("custom_field_value"):null;
		$display_settings = $request->get("display_settings")?$request->get("display_settings"):null;
		$param_is_embedded_display= $request->get("param_is_embedded_display")?$request->get("param_is_embedded_display"):null;
		$add_to_cart_single_product = 1;
		$input_settings = array("display_title"=>$display_title,"product_display"=>$product_display,"add_to_cart_single_product"=>$add_to_cart_single_product,"app_installed_store_id"=>$storeId,"param_bundling_custom_field_name"=>$custom_field_name,"param_is_bundle_product_display"=>$display_settings,"param_is_embedded_display"=>$param_is_embedded_display);
		//echo "input_settings<pre>";print_r($input_settings);echo "</pre>";
		$output_settings = $this->commonModel->manageSettings($input_settings);
		//echo "output_settings<pre>";print_r($output_settings);echo "</pre>";
		return $output_settings;
	}
	function productCartValidation($request,$storeId){
		$destination_product_id =$request->get("destination_product_id")?$request->get("destination_product_id"):null;
		$product_cart_input = array("destination_product_id"=>$destination_product_id,"app_installed_store_id"=>$storeId);
		//echo "product_cart_input<pre>";print_r($product_cart_input);echo "</pre>";
		$product_cart_output = $this->commonModel->productCartValidation($product_cart_input);
		//echo "product_cart_output<pre>";print_r($product_cart_output);echo "</pre>";
		return $product_cart_output;
	}
	function addProductsCart($request,$storeId,$store_hash,$client_id,$access_token){
	//	echo $client_id = "n85s0rlo72r4hb4aiiffy8gp9rf03ei"; //parent_product_qty
		$parent_product_lineitem_id =$request->get("parent_product_lineitem_id")?$request->get("parent_product_lineitem_id"):null;
		$parent_product_id =$request->get("parent_product_id")?$request->get("parent_product_id"):null;
		$parent_product_qty =$request->get("parent_product_qty")?$request->get("parent_product_qty"):null;
		$product_details =$request->get("product_details")?$request->get("product_details"):null;
		$product_details = json_decode($product_details);
		$cart_id = "";
		$main_array = [];
		$line_items = [];
		$line_items_parent = [];
		foreach($product_details as $key => $value){
			foreach($value as $key =>$data){
			//echo "<pre>";print_r($value);echo "</pre>";
			$cart_id = $data->cart_id;
			
			if($data->list_price != null){
				//echo "listing";
				$list_price_data = preg_replace("/([^0-9\\.])/i", "", $data->list_price); 
				array_push($line_items,array("quantity"=>intval($data->quantity),"product_id"=>intval($data->product_id),"list_price"=>$list_price_data,"variant_id"=>intval($data->variant_id)));
			}else{
				array_push($line_items,array("quantity"=>intval($data->quantity),"product_id"=>intval($data->product_id),"variant_id"=>intval($data->variant_id)));
			}
			}
			//echo "<pre>";print_r($value);echo "</pre>";die;
		}
		
		$data = array("line_items"=>$line_items);
		//echo json_encode($data);
		$get_url = "https://api.bigcommerce.com/stores/".$store_hash."/v3/carts/".$cart_id."/items";
		$method = "POST";
		$response = $this->callBigCommerceAPI($get_url,$method,$client_id,$access_token,$data);
		//echo "response<pre>";print_r($response);echo "</pre>";
		$response_data = json_decode($response);
		$temp = json_decode($response,true);
		if(isset($response_data->title) && $response_data->title != ""){
			if($parent_product_lineitem_id != null){
			
			//echo $cart_id;
			//echo $response_data->title;
			$data_parent_update = "";
			//echo "data_parent_update<pre>";print_r($data_parent_update);echo "</pre>";
			 $update_url = "https://api.bigcommerce.com/stores/".$store_hash."/v3/carts/".$cart_id."/items/".$parent_product_lineitem_id;
			$method_update = "DELETE";
			$response_update = $this->callBigCommerceAPI($update_url,$method_update,$client_id,$access_token,$data_parent_update);
			$temp_update = json_decode($response_update,true);
			//echo "temp_update<pre>";print_r($temp_update);echo "</pre>";die;
			}
		}else{
			if($parent_product_lineitem_id != null){
				
				//echo $cart_id;
				$data_parent_update = array("line_item"=>array("quantity"=>intval($parent_product_qty),"product_id"=>intval($parent_product_id),"list_price"=>floatval(0.00),"variant_id"=>null));
				//echo "data_parent_update<pre>";print_r($data_parent_update);echo "</pre>";
				$update_url = "https://api.bigcommerce.com/stores/".$store_hash."/v3/carts/".$cart_id."/items/".$parent_product_lineitem_id;
				$method_update = "PUT";
				$response_update = $this->callBigCommerceAPI($update_url,$method_update,$client_id,$access_token,$data_parent_update);
				$temp_update = json_decode($response_update,true);
				//echo "temp_update<pre>";print_r($temp_update);echo "</pre>";die;
			}
		}


		
	
		//echo "temp<pre>";print_r($temp);echo "</pre>";die;
		return $temp;
	}
	function get_custom_fields_values($request,$storeId,$store_hash,$client_id,$access_token){
		$product_id =$request->get("product_id")?$request->get("product_id"):null;
		$get_url = "https://api.bigcommerce.com/stores/".$store_hash."/v3/catalog/products/".$product_id."/custom-fields";
		$method = "GET";
		$data = "";
		$response = $this->callBigCommerceAPI($get_url,$method,$client_id,$access_token,$data);
		$temp = json_decode($response,true);
		return $temp;
	
	}
	function get_cart_validation($request,$storeId){
		$cart_line_item_id =$request->get("cart_line_item_id")?$request->get("cart_line_item_id"):null;
		//$cart_id =$request->get("cart_id")?$request->get("cart_id"):null;
		$input_cart = array("cart_line_item_id"=>$cart_line_item_id,"app_installed_store_id"=>$storeId);
		//echo "input_cart<pre>";print_r($input_cart);echo "</pre>";
		$output_cart = $this->commonModel->get_cart_validation($input_cart);
		//echo "output_cart<pre>";print_r($output_cart);echo "</pre>";
		return $output_cart;
	}
	function manage_cart_details($request,$storeId){
		$product_details =$request->get("product_details")?$request->get("product_details"):null;
		$cart_id =$request->get("cart_id")?$request->get("cart_id"):null;
		$input_cart = array("bc_cart_id"=>$cart_id,"cart_item_details"=>$product_details,"app_installed_store_id"=>$storeId);
		//echo "input_cart<pre>";print_r($input_cart);echo "</pre>";
		$output_cart = $this->commonModel->manage_cart_details($input_cart);
		//echo "output_cart<pre>";print_r($output_cart);echo "</pre>";die;
		return $output_cart;
	}
	function get_cart_bundle_kit($request,$storeId){
		$cart_id =$request->get("cart_id")?$request->get("cart_id"):null;
		$input_cart = array("bc_cart_id"=>$cart_id,"app_installed_store_id"=>$storeId);
		//echo "input_cart<pre>";print_r($input_cart);echo "</pre>";
		$output_cart = $this->commonModel->get_cart_bundle_kit($input_cart);
		//echo "output_cart<pre>";print_r($output_cart);echo "</pre>";die;
		return $output_cart;
	}
	function cart_line_item_delete($request,$storeId,$storeHash,$client_id,$access_token){
		$product_detail_cart =$request->get("product_detail_cart")?$request->get("product_detail_cart"):null;
		$cart_id =$request->get("cart_id")?$request->get("cart_id"):null;
		$product_detail_cart = json_decode($product_detail_cart);
		$response_array= [];
		foreach($product_detail_cart  as $key => $value){
			$get_url = "https://api.bigcommerce.com/stores/".$storeHash."/v3/carts/".$cart_id."/items/".$value;
			$method = "DELETE";
			$data ='';
			$response = $this->callBigCommerceAPI($get_url,$method,$client_id,$access_token,$data);
			array_push($response_array,$response);
		}
		
	
		return $response_array;
	}
	function get_cron_details($request,$storeId){
		$output_cron_details = $this->commonModel->sp_get_cron_details();
		//echo "output_cron_details<pre>";print_r($output_cron_details);echo "</pre>";
		return $output_cron_details;
	}
	function callBigCommerceAPI($url,$method,$client_id,$access_token,$data){
		$fields='';
		if($data!='' || $data!=null){
		 $fields=json_encode($data);
		}
		$ch = curl_init(); 
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 100); 
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array ('X-Auth-Client: '.$client_id.'','X-Auth-Token: '.$access_token.'','Accept: application/json', 'Content-Type: application/json'));
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $method); 
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 ); 
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );   
		$response = curl_exec($ch);   
		return $response;
	}
}
?>