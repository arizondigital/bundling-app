<?php 
include_once '/var/www/html/Furniture/Bundling_App/models/AdminModel.php';
	class AdminController
	{
		function __construct()
		{
			$this->adminModel = new AdminModel();
		}

		function getStore($storeHash){
			$inputData = Array("store_hash"=>$storeHash);
			return $this->adminModel->getStoredetails($inputData);
		}
		
		function getStoreId($request){
			$storeHash = $request->get('store_hash');
			$inputData = Array("store_hash"=>$storeHash);
			return $this->adminModel->getStoredetails($inputData);
		}

	}