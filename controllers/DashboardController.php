<?php
include_once 'models/DashboardModel.php';
class DashboardController
{
	function __construct()
	{
		$this->dashboardModel = new DashboardModel();
	}

	function getDashboardDetails($request,$storeId)
	{
		$inputData = Array("app_installed_store_id"=>$storeId);
		$result = $this->dashboardModel->getDashboardDetails($inputData);
		//echo "result<pre>";print_r($result);echo "</pre>";die;
		return $result;
	}

}

?>

