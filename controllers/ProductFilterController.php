<?php
include_once 'models/ProductFilterModel.php';
class ProductFilterController
{
	function __construct()
	{
		$this->productFiltermodel = new ProductFilterModel();
	}
	function get_filter_data($request,$storeId){
		$current_page = $request->get("current_page");
		if($request->get('current_page')!=null || $request->get('current_page')!=""){
			if($request->get('current_page') != 1){
				$pageNumber=($request->get('current_page') - 1) * 15 +1;
				$page_end = $pageNumber + 14;
			}else{
				$pageNumber=1;
				$page_end = $pageNumber + 15;
			}
				
			}else{
				$pageNumber=1;
				$page_end = 15;
				$current_page = 1;
		}
		$input_array =array("page_start"=>$pageNumber,"page_end"=>$page_end,"app_installed_store_id"=>$storeId);
		$input =array("page_start"=>null,"page_end"=>null,"app_installed_store_id"=>$storeId);
		/*echo "<pre>";
		print_r($input_array);
		echo "</pre>";*/
		$filter_data = $this->productFiltermodel->get_filter_data($input_array);
		$filter_parent_name = $this->productFiltermodel->Get_filter_parent_name($input);
		$result_data ['filter_data'] = $filter_data;
		$result_data ['filter_parent_name'] = $filter_parent_name;
		$result_data['active_page'] = $current_page;
		/*echo "<pre>";
		print_r($result_data);
		echo "</pre>";*/
		return $result_data;
	}
	function manage_filter_data($request,$storeId){
		$filter_id = $request->get("filter_id")?$request->get("filter_id"):null;
		$filter_display_name = $request->get("filter_display_name")?$request->get("filter_display_name"):null;
		$is_active = $request->get("is_active")?$request->get("is_active"):"true";
		$parent_filter_name = $request->get("parent_filter_name")?$request->get("parent_filter_name"):null;
		$filter_position = $request->get("filter_position")?$request->get("filter_position"):null;
		$input_array =array("filter_id"=>$filter_id,"filter_display_name"=>$filter_display_name,"is_active"=>$is_active,"parent_filter_name"=>$parent_filter_name,"filter_position"=>$filter_position,"app_installed_store_id"=>$storeId);
		/*echo "<pre>";
		print_r($input_array);
		echo "</pre>";*/
		$result_data = $this->productFiltermodel->manage_filter_data($input_array);
		/*echo "<pre>";
		print_r($result_data);
		echo "</pre>";die;*/
		return $result_data;
	}
	function get_filter_value_data($request,$storeId){
		$filter_id = $request->get("filter_id");
		$current_page = $request->get("current_page");
		if($request->get('current_page')!=null || $request->get('current_page')!=""){
			if($request->get('current_page') != 1){
				$pageNumber=($request->get('current_page') - 1) * 15 +1;
				$page_end = $pageNumber + 14;
			}else{
				$pageNumber=1;
				$page_end = $pageNumber + 15;
			}
				
			}else{
				$pageNumber=1;
				$page_end = 15;
				$current_page = 1;
		}
		//echo "filter_id";
		//echo $filter_id;die;
		$input_array = array("page_start"=>$pageNumber,"page_end"=>$page_end,"filter_id"=>$filter_id,"app_installed_store_id"=>$storeId);
		/*echo "<pre>";
		print_r($input_array);
		echo "</pre>";*/
		$result = $this->productFiltermodel->get_filter_value_data($input_array);
		/*echo "<pre>";
		print_r($result);
		echo "</pre>";*/
		$result_data ['filter_value'] = $result;
		$result_data['active_page'] = $current_page;
		return $result_data;
	}
	function manage_filter_value_data($request,$storeId){
		$filter_id = $request->get("filter_id");
		$filter_values = $request->get("filter_value_details");
		$filter_value_details = array("filter_value_details"=>$filter_values);
		//echo "checl";
		$data = json_encode($filter_value_details);
		$input_array = array("filter_id"=>$filter_id,"filter_value"=>$data,"app_installed_store_id"=>$storeId);
		$result_data = $this->productFiltermodel->manage_filter_value_data($input_array);
		return $result_data;

	}
	function filter_visibility_update($request,$storeId){
		$filter_id = $request->get("filter_id");
		$is_active = $request->get("is_active")?$request->get("is_active"):null;
		$is_nested = $request->get("is_nested")?$request->get("is_nested"):null;
		$input_array = array("filter_id"=>$filter_id,"is_active"=>$is_active,"is_nested"=>$is_nested,"app_installed_store_id"=>$storeId);
		/*echo "<pre>";
		print_r($input_array);
		echo "</pre>";*/
		$result_data = $this->productFiltermodel->filter_visibility_update($input_array);
		/*echo "<pre>";
		print_r($result_data);
		echo "</pre>";*/
		return $result_data;
	}
}
?>