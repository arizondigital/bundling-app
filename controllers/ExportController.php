<?php
include_once 'models/CommonModel.php';

class ExportController
{
    function __construct()
    {
        $this->commonModel = new CommonModel();
    }

	    function getcustomercategoryExport($request)
    {
		/*echo "request<pre>";
		print_r($request);
		echo "</pre>";*/
		$customer_group_id = $request->get("customer_group_id")?$request->get("customer_group_id"):null;
		$customer_group_current_page = $request->get("customer_group_current_page");
		if($request->get('customer_group_current_page')!=null || $request->get('customer_group_current_page')!=""){
			if($request->get('customer_group_current_page') != 1){
			$pageNumber=($request->get('customer_group_current_page') - 1) * 100;
		}else{
			$pageNumber=1;
		}
			$page_end = $pageNumber + 99;
		}else{
			$pageNumber=1;
			$page_end = 100;
			$customer_group_current_page = 1;
		}
		$product_name_search = $request->get("product_name_search")?$request->get("product_name_search"):null;
		$category_name_search = $request->get("category_name_search")?$request->get("category_name_search"):null;
		$brand_name_search = $request->get("brand_name_search")?$request->get("brand_name_search"):null;
		$visibility = ($request->get("visibility_search")!="")?$request->get("visibility_search"):null;
		$product_sku_search = ($request->get("product_sku_search")!="")?$request->get("product_sku_search"):null;
		$input_data = array("page_start"=>null,"page_end"=>null,"bc_customer_group_id"=>$customer_group_id,"bc_category_id"=>null,"product_name_search"=>$product_name_search,"category_name_search"=>$category_name_search,"brand_name_search"=>$brand_name_search,"product_is_visible"=>$visibility,"product_sku_search"=>$product_sku_search);
		/*echo "<pre>";
		print_r($input_data);
		echo "</pre>";*/
		$result_data = $this->commonModel->getProductCustomergroups($input_data);
			/*echo "<pre>";
		print_r($result_data);
		echo "</pre>";*/
		$result = Array();
		$product_array = Array();
		foreach($result_data as $key => $products){
			$product_array[$products['product_id']]['product_name'] = $products['product_name'];
			$product_array[$products['product_id']]['is_active'] = $products['is_active'];
			$product_array[$products['product_id']]['category_name'] = $products['category_name'];
			$product_array[$products['product_id']]['brand_name'] = $products['brand_name'];
		}

		/*echo "<pre>";
		print_r($product_array);
		echo "</pre>";*/
		$customer_group_name='';
		$input = array("bc_customer_group_id"=>$customer_group_id);
		$customer_group_name=$this->commonModel->getCustomerGroupName($input);
		$customer_group_name=$customer_group_name['0']['param_customer_group_name'];

	require 'include/PHPExcel/Classes/PHPExcel.php';
	require 'include/PHPExcel/Classes/PHPExcel/Writer/Excel5.php'; 
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()
	->setCreator("Sample XLS Creator")
	->setLastModifiedBy("ModifiedBy Sample XLS")
	->setTitle("Sample XLS Title")
	->setSubject("Sample XLS SUbject")
	->setDescription("This is the description of the file")
	->setKeywords("Keyword");
	$objPHPSheet = $objPHPExcel->getActiveSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPSheet->mergeCells('A1:E1');
	$objPHPSheet->SetCellValue('A1', "Category Products Details");
	$objPHPSheet->setTitle("Category Products Details");
	$filter_row_count=0;

	$objPHPSheet->SetCellValue('A'.($filter_row_count+5), "PRODUCT NAME");
	$objPHPSheet->SetCellValue('B'.($filter_row_count+5), "CATEGORY NAME");
	$objPHPSheet->SetCellValue('C'.($filter_row_count+5), "BRAND NAME");
	$objPHPSheet->SetCellValue('D'.($filter_row_count+5), "VISIBILITY");
	$objPHPSheet->SetCellValue('E'.($filter_row_count+5), "CUSTOMER GROUP NAME");

	
	
	$index=6;
	if(COUNT($product_array)>0){
	foreach($product_array as $key=>$value){ 
			//$sales_person_name = $value['sales_person_first_name']." ".$value['sales_person_last_name'];

			$objPHPSheet->SetCellValue('A'.($filter_row_count+$index),$value['product_name']);
			$objPHPSheet->SetCellValue('B'.($filter_row_count+$index),$value['category_name']);
			$objPHPSheet->SetCellValue('C'.($filter_row_count+$index),$value['brand_name']);
			$objPHPSheet->SetCellValue('D'.($filter_row_count+$index),$value['is_active']);
			$objPHPSheet->SetCellValue('E'.($filter_row_count+$index),$customer_group_name);
			$index++;
		}
	}else{
		$objPHPSheet->SetCellValue('A'.($filter_row_count+$index),"No Data Found");
		$objPHPSheet->mergeCells('A'.($filter_row_count+$index).':E'.($filter_row_count+$index));
		$objPHPSheet->getStyle('A'.($filter_row_count+$index))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPSheet->getStyle('A'.($filter_row_count+$index))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	}
	
	//Font
	$objPHPSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPSheet->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPSheet->getStyle('A1')->getFont()->setName('Calibri')->setSize(18);
	$objPHPSheet->getStyle('A2:E10000')->getFont()->setName('Calibri')->setSize(12);
	$objPHPSheet->getStyle('A'.($filter_row_count+5).':E'.($filter_row_count+5))->getFont()->setBold(true);
	$objPHPSheet->getStyle('A4:A'.($filter_row_count+5))->getFont()->setBold(true);
	$objPHPSheet->getStyle('A'.($filter_row_count+3))->getFont()->setBold(true);

	//Width & Height
	$objPHPSheet->getColumnDimension('A')->setWidth(45);
	$objPHPSheet->getColumnDimension('B')->setWidth(20);
	$objPHPSheet->getColumnDimension('C')->setWidth(40);
	$objPHPSheet->getColumnDimension('D')->setWidth(40);
	$objPHPSheet->getColumnDimension('E')->setWidth(40);


	//Justify Alignment
	$objPHPSheet->getStyle('A1:A10000')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
	$objPHPSheet->getStyle('B1:B10000')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
	$objPHPSheet->getStyle('C1:C10000')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
	$objPHPSheet->getStyle('D1:D10000')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
	$objPHPSheet->getStyle('E1:E10000')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
	

	$objPHPSheet->getStyle('A'.($filter_row_count+5).':E'.($filter_row_count+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


	$objPHPSheet->getStyle('A'.($filter_row_count+5).':E'.($filter_row_count+$index))->applyFromArray(
	array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')
				)	
			)
		)
	);

	$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');	
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="CATEGORY PRODUCTS DETAILS.xls"');
	ob_clean();
	ob_end_clean();
	$writer->save('php://output');									
	exit();
	}
}