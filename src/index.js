import $ from "jquery";
var stencilUtils=function(t){var e={};function r(n){if(e[n])return e[n].exports;var o=e[n]={i:n,l:!1,exports:{}};return t[n].call(o.exports,o,o.exports,r),o.l=!0,o.exports}return r.m=t,r.c=e,r.d=function(t,e,n){r.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:n})},r.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},r.t=function(t,e){if(1&e&&(t=r(t)),8&e)return t;if(4&e&&"object"==typeof t&&t&&t.__esModule)return t;var n=Object.create(null);if(r.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:t}),2&e&&"string"!=typeof t)for(var o in t)r.d(n,o,function(e){return t[e]}.bind(null,o));return n},r.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return r.d(e,"a",e),e},r.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},r.p="",r(r.s=15)}([function(t,e){function r(e){return t.exports=r=Object.setPrototypeOf?Object.getPrototypeOf:function(t){return t.__proto__||Object.getPrototypeOf(t)},r(e)}t.exports=r},function(t,e){t.exports=function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}},function(t,e){function r(t,e){for(var r=0;r<e.length;r++){var n=e[r];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(t,n.key,n)}}t.exports=function(t,e,n){return e&&r(t.prototype,e),n&&r(t,n),t}},function(t,e,r){var n=r(17);t.exports=function(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function");t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,writable:!0,configurable:!0}}),e&&n(t,e)}},function(t,e,r){var n=r(5),o=r(18);t.exports=function(t,e){return!e||"object"!==n(e)&&"function"!=typeof e?o(t):e}},function(t,e){function r(e){return"function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?t.exports=r=function(t){return typeof t}:t.exports=r=function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},r(e)}t.exports=r},function(t,e,r){"use strict";var n=r(1),o=r.n(n),i=r(2),c=r.n(i),s=r(3),u=r.n(s),a=r(4),f=r.n(a),l=r(0),p=r.n(l),h=r(12);function y(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=p()(t);if(e){var o=p()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return f()(this,r)}}var d=function(t){u()(r,t);var e=y(r);function r(){return o()(this,r),e.apply(this,arguments)}return c()(r,[{key:"subscribe",value:function(t,e,r){document.addEventListener(t,(function(t){for(var n=t.target;n&&n!==this;n=n.parentNode)if(n.matches(e)){r.call(n,t,n);break}}),!1)}}]),r}(r.n(h).a);function v(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=p()(t);if(e){var o=p()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return f()(this,r)}}var m=function(t){u()(r,t);var e=v(r);function r(){var t;return o()(this,r),(t=e.call(this)).itemAdd(),t}return c()(r,[{key:"itemAdd",value:function(){var t=this;this.subscribe("submit","[data-cart-item-add]",(function(e,r){t.emit("cart-item-add",e,r)}))}}]),r}(d);function b(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=p()(t);if(e){var o=p()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return f()(this,r)}}var g=function(t){u()(r,t);var e=b(r);function r(){return o()(this,r),e.apply(this,arguments)}return r}(d);function w(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=p()(t);if(e){var o=p()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return f()(this,r)}}var O=function(t){u()(r,t);var e=w(r);function r(){var t;return o()(this,r),(t=e.call(this)).currencySelector(),t}return c()(r,[{key:"currencySelector",value:function(){var t=this;this.subscribe("input","[data-currency-selector-toggle]",(function(e){t.emit("currencySelector-toggle",e)}))}}]),r}(d);function R(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=p()(t);if(e){var o=p()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return f()(this,r)}}var x=function(t){u()(r,t);var e=R(r);function r(){var t;return o()(this,r),(t=e.call(this)).optionsChange(),t}return c()(r,[{key:"optionsChange",value:function(){var t=this;this.subscribe("change","[data-product-option-change]",(function(e,r){t.emit("product-option-change",e,r)}))}}]),r}(d);function _(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=p()(t);if(e){var o=p()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return f()(this,r)}}var E=function(t){u()(r,t);var e=_(r);function r(){var t;return o()(this,r),(t=e.call(this)).quickSearch(),t}return c()(r,[{key:"quickSearch",value:function(){var t=this;this.subscribe("input","[data-search-quick]",(function(e,r){t.emit("search-quick",e,r)}))}}]),r}(d);function j(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=p()(t);if(e){var o=p()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return f()(this,r)}}var k=function(t){u()(r,t);var e=j(r);function r(){var t;return o()(this,r),(t=e.call(this)).searchEvents(),t}return c()(r,[{key:"searchEvents",value:function(){var t=this;this.subscribe("click","[data-faceted-search-facet]",(function(e,r){t.emit("facetedSearch-facet-clicked",e,r)})),this.subscribe("submit","[data-faceted-search-range]",(function(e,r){t.emit("facetedSearch-range-submitted",e,r)}))}}]),r}(d);function T(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=p()(t);if(e){var o=p()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return f()(this,r)}}Element.prototype.matches||(Element.prototype.matches=Element.prototype.msMatchesSelector||Element.prototype.webkitMatchesSelector),Element.prototype.closest||(Element.prototype.closest=function(t){var e=this;do{if(e.matches(t))return e;e=e.parentElement||e.parentNode}while(null!==e&&1===e.nodeType);return null});var A=function(t){u()(r,t);var e=T(r);function r(){var t;return o()(this,r),(t=e.call(this)).sortByEvents(),t}return c()(r,[{key:"sortByEvents",value:function(){var t=this;this.subscribe("submit","[data-sort-by]",(function(e,r){t.emit("sortBy-submitted",e,r)})),this.subscribe("change","[data-sort-by] select",(function(e,r){t.emit("sortBy-select-changed",e,r),e.defaultPrevented||t.emit("sortBy-submitted",e,r)}))}}]),r}(d),S={};S.classes={cart:new m,cookie:new g,currencySelector:new O,product:new x,search:new E,facetedSearch:new k,sortBy:new A},S.parseHooks=function(t){var e=t.split("-")[0];if(void 0===S.classes[e])throw new Error("".concat(e," is not a valid hookType"));return S.classes[e]};var P=function(){function t(){o()(this,t)}return c()(t,[{key:"on",value:function(t,e){return S.parseHooks(t).on(t,e)}},{key:"off",value:function(t,e){return S.parseHooks(t).off(t,e)}},{key:"emit",value:function(t){var e=S.parseHooks(t);return e.emit.apply(e,arguments)}}]),t}();e.a=new P},function(t,e,r){var n=r(22),o=r(23),i=r(10),c=r(24);t.exports=function(t){return n(t)||o(t)||i(t)||c()}},function(t,e,r){"use strict";var n=r(13),o=r.n(n),i=r(5),c=r.n(i),s="undefined"!=typeof globalThis&&globalThis||"undefined"!=typeof self&&self||void 0!==s&&s,u="URLSearchParams"in s,a="Symbol"in s&&"iterator"in Symbol,f="FileReader"in s&&"Blob"in s&&function(){try{return new Blob,!0}catch(t){return!1}}(),l="FormData"in s,p="ArrayBuffer"in s;if(p)var h=["[object Int8Array]","[object Uint8Array]","[object Uint8ClampedArray]","[object Int16Array]","[object Uint16Array]","[object Int32Array]","[object Uint32Array]","[object Float32Array]","[object Float64Array]"],y=ArrayBuffer.isView||function(t){return t&&h.indexOf(Object.prototype.toString.call(t))>-1};function d(t){if("string"!=typeof t&&(t=String(t)),/[^a-z0-9\-#$%&'*+.^_`|~!]/i.test(t)||""===t)throw new TypeError("Invalid character in header field name");return t.toLowerCase()}function v(t){return"string"!=typeof t&&(t=String(t)),t}function m(t){var e={next:function(){var e=t.shift();return{done:void 0===e,value:e}}};return a&&(e[Symbol.iterator]=function(){return e}),e}function b(t){this.map={},t instanceof b?t.forEach((function(t,e){this.append(e,t)}),this):Array.isArray(t)?t.forEach((function(t){this.append(t[0],t[1])}),this):t&&Object.getOwnPropertyNames(t).forEach((function(e){this.append(e,t[e])}),this)}function g(t){if(t.bodyUsed)return Promise.reject(new TypeError("Already read"));t.bodyUsed=!0}function w(t){return new Promise((function(e,r){t.onload=function(){e(t.result)},t.onerror=function(){r(t.error)}}))}function O(t){var e=new FileReader,r=w(e);return e.readAsArrayBuffer(t),r}function R(t){if(t.slice)return t.slice(0);var e=new Uint8Array(t.byteLength);return e.set(new Uint8Array(t)),e.buffer}function x(){return this.bodyUsed=!1,this._initBody=function(t){var e;this.bodyUsed=this.bodyUsed,this._bodyInit=t,t?"string"==typeof t?this._bodyText=t:f&&Blob.prototype.isPrototypeOf(t)?this._bodyBlob=t:l&&FormData.prototype.isPrototypeOf(t)?this._bodyFormData=t:u&&URLSearchParams.prototype.isPrototypeOf(t)?this._bodyText=t.toString():p&&f&&((e=t)&&DataView.prototype.isPrototypeOf(e))?(this._bodyArrayBuffer=R(t.buffer),this._bodyInit=new Blob([this._bodyArrayBuffer])):p&&(ArrayBuffer.prototype.isPrototypeOf(t)||y(t))?this._bodyArrayBuffer=R(t):this._bodyText=t=Object.prototype.toString.call(t):this._bodyText="",this.headers.get("content-type")||("string"==typeof t?this.headers.set("content-type","text/plain;charset=UTF-8"):this._bodyBlob&&this._bodyBlob.type?this.headers.set("content-type",this._bodyBlob.type):u&&URLSearchParams.prototype.isPrototypeOf(t)&&this.headers.set("content-type","application/x-www-form-urlencoded;charset=UTF-8"))},f&&(this.blob=function(){var t=g(this);if(t)return t;if(this._bodyBlob)return Promise.resolve(this._bodyBlob);if(this._bodyArrayBuffer)return Promise.resolve(new Blob([this._bodyArrayBuffer]));if(this._bodyFormData)throw new Error("could not read FormData body as blob");return Promise.resolve(new Blob([this._bodyText]))},this.arrayBuffer=function(){if(this._bodyArrayBuffer){var t=g(this);return t||(ArrayBuffer.isView(this._bodyArrayBuffer)?Promise.resolve(this._bodyArrayBuffer.buffer.slice(this._bodyArrayBuffer.byteOffset,this._bodyArrayBuffer.byteOffset+this._bodyArrayBuffer.byteLength)):Promise.resolve(this._bodyArrayBuffer))}return this.blob().then(O)}),this.text=function(){var t,e,r,n=g(this);if(n)return n;if(this._bodyBlob)return t=this._bodyBlob,e=new FileReader,r=w(e),e.readAsText(t),r;if(this._bodyArrayBuffer)return Promise.resolve(function(t){for(var e=new Uint8Array(t),r=new Array(e.length),n=0;n<e.length;n++)r[n]=String.fromCharCode(e[n]);return r.join("")}(this._bodyArrayBuffer));if(this._bodyFormData)throw new Error("could not read FormData body as text");return Promise.resolve(this._bodyText)},l&&(this.formData=function(){return this.text().then(j)}),this.json=function(){return this.text().then(JSON.parse)},this}b.prototype.append=function(t,e){t=d(t),e=v(e);var r=this.map[t];this.map[t]=r?r+", "+e:e},b.prototype.delete=function(t){delete this.map[d(t)]},b.prototype.get=function(t){return t=d(t),this.has(t)?this.map[t]:null},b.prototype.has=function(t){return this.map.hasOwnProperty(d(t))},b.prototype.set=function(t,e){this.map[d(t)]=v(e)},b.prototype.forEach=function(t,e){for(var r in this.map)this.map.hasOwnProperty(r)&&t.call(e,this.map[r],r,this)},b.prototype.keys=function(){var t=[];return this.forEach((function(e,r){t.push(r)})),m(t)},b.prototype.values=function(){var t=[];return this.forEach((function(e){t.push(e)})),m(t)},b.prototype.entries=function(){var t=[];return this.forEach((function(e,r){t.push([r,e])})),m(t)},a&&(b.prototype[Symbol.iterator]=b.prototype.entries);var _=["DELETE","GET","HEAD","OPTIONS","POST","PUT"];function E(t,e){if(!(this instanceof E))throw new TypeError('Please use the "new" operator, this DOM object constructor cannot be called as a function.');var r,n,o=(e=e||{}).body;if(t instanceof E){if(t.bodyUsed)throw new TypeError("Already read");this.url=t.url,this.credentials=t.credentials,e.headers||(this.headers=new b(t.headers)),this.method=t.method,this.mode=t.mode,this.signal=t.signal,o||null==t._bodyInit||(o=t._bodyInit,t.bodyUsed=!0)}else this.url=String(t);if(this.credentials=e.credentials||this.credentials||"same-origin",!e.headers&&this.headers||(this.headers=new b(e.headers)),this.method=(r=e.method||this.method||"GET",n=r.toUpperCase(),_.indexOf(n)>-1?n:r),this.mode=e.mode||this.mode||null,this.signal=e.signal||this.signal,this.referrer=null,("GET"===this.method||"HEAD"===this.method)&&o)throw new TypeError("Body not allowed for GET or HEAD requests");if(this._initBody(o),!("GET"!==this.method&&"HEAD"!==this.method||"no-store"!==e.cache&&"no-cache"!==e.cache)){var i=/([?&])_=[^&]*/;if(i.test(this.url))this.url=this.url.replace(i,"$1_="+(new Date).getTime());else{this.url+=(/\?/.test(this.url)?"&":"?")+"_="+(new Date).getTime()}}}function j(t){var e=new FormData;return t.trim().split("&").forEach((function(t){if(t){var r=t.split("="),n=r.shift().replace(/\+/g," "),o=r.join("=").replace(/\+/g," ");e.append(decodeURIComponent(n),decodeURIComponent(o))}})),e}function k(t,e){if(!(this instanceof k))throw new TypeError('Please use the "new" operator, this DOM object constructor cannot be called as a function.');e||(e={}),this.type="default",this.status=void 0===e.status?200:e.status,this.ok=this.status>=200&&this.status<300,this.statusText="statusText"in e?e.statusText:"",this.headers=new b(e.headers),this.url=e.url||"",this._initBody(t)}E.prototype.clone=function(){return new E(this,{body:this._bodyInit})},x.call(E.prototype),x.call(k.prototype),k.prototype.clone=function(){return new k(this._bodyInit,{status:this.status,statusText:this.statusText,headers:new b(this.headers),url:this.url})},k.error=function(){var t=new k(null,{status:0,statusText:""});return t.type="error",t};var T=[301,302,303,307,308];k.redirect=function(t,e){if(-1===T.indexOf(e))throw new RangeError("Invalid status code");return new k(null,{status:e,headers:{location:t}})};var A=s.DOMException;try{new A}catch(t){(A=function(t,e){this.message=t,this.name=e;var r=Error(t);this.stack=r.stack}).prototype=Object.create(Error.prototype),A.prototype.constructor=A}function S(t,e){return new Promise((function(r,n){var o=new E(t,e);if(o.signal&&o.signal.aborted)return n(new A("Aborted","AbortError"));var i=new XMLHttpRequest;function c(){i.abort()}i.onload=function(){var t,e,n={status:i.status,statusText:i.statusText,headers:(t=i.getAllResponseHeaders()||"",e=new b,t.replace(/\r?\n[\t ]+/g," ").split("\r").map((function(t){return 0===t.indexOf("\n")?t.substr(1,t.length):t})).forEach((function(t){var r=t.split(":"),n=r.shift().trim();if(n){var o=r.join(":").trim();e.append(n,o)}})),e)};n.url="responseURL"in i?i.responseURL:n.headers.get("X-Request-URL");var o="response"in i?i.response:i.responseText;setTimeout((function(){r(new k(o,n))}),0)},i.onerror=function(){setTimeout((function(){n(new TypeError("Network request failed"))}),0)},i.ontimeout=function(){setTimeout((function(){n(new TypeError("Network request failed"))}),0)},i.onabort=function(){setTimeout((function(){n(new A("Aborted","AbortError"))}),0)},i.open(o.method,function(t){try{return""===t&&s.location.href?s.location.href:t}catch(e){return t}}(o.url),!0),"include"===o.credentials?i.withCredentials=!0:"omit"===o.credentials&&(i.withCredentials=!1),"responseType"in i&&(f?i.responseType="blob":p&&o.headers.get("Content-Type")&&-1!==o.headers.get("Content-Type").indexOf("application/octet-stream")&&(i.responseType="arraybuffer")),!e||"object"!=typeof e.headers||e.headers instanceof b?o.headers.forEach((function(t,e){i.setRequestHeader(e,t)})):Object.getOwnPropertyNames(e.headers).forEach((function(t){i.setRequestHeader(t,v(e.headers[t]))})),o.signal&&(o.signal.addEventListener("abort",c),i.onreadystatechange=function(){4===i.readyState&&o.signal.removeEventListener("abort",c)}),i.send(void 0===o._bodyInit?null:o._bodyInit)}))}S.polyfill=!0,s.fetch||(s.fetch=S,s.Headers=b,s.Request=E,s.Response=k);var P=r(14),D=r.n(P),q=r(7),B=r.n(q),I=function(t){return encodeURIComponent(t).replace(/[!'()*]/g,(function(t){return"%".concat(t.charCodeAt(0).toString(16).toUpperCase())}))},C=function(t){return function(e){return function(r,n){if(void 0===n)return r;if(null===n)return[].concat(B()(r),[I(e)]);if(t.includeArrayIndex){if("object"===c()(n)){var o=r.length/Object.keys(n).length;return r.concat(Object.keys(n).map((function(t){return[I(e),"[",I(o),"]","[",I(t),"]=",I(n[t])].join("")})))}var i=r.length;return[].concat(B()(r),[[I(e),"[",I(i),"]=",I(n)].join("")])}return[].concat(B()(r),[[I(e),"=",I(n)].join("")])}}};function U(t){var e=Object.create(null);if("string"!=typeof t)return e;var r=t.trim().replace(/^[?#&]/,"");return r?(r.split("&").forEach((function(t){var r,n,o,i=(r=t.replace(/\+/g," "),n="=",-1===(o=r.indexOf(n))?[r]:[r.slice(0,o),r.slice(o+n.length)]),c=D()(i,2),s=c[0],u=c[1];s=decodeURIComponent(s),u=void 0===u?null:decodeURIComponent(u),e[s]=function(t,e,r){return void 0===r[t]?e:[].concat(r[t],e)}(s,u,e)})),Object.keys(e).sort().reduce((function(t,r){var n=e[r];return Boolean(n)&&"object"===c()(n)&&!Array.isArray(n)?t[r]=function t(e){return Array.isArray(e)?e.sort():"object"===c()(e)?t(Object.keys(e)).sort((function(t,e){return Number(t)-Number(e)})).map((function(t){return e[t]})):e}(n):t[r]=n,t}),Object.create(null))):e}function G(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{filterValues:!1,arrayIndex:!1};if(!t)return"";var r={},n=function(r){return e.filterValues&&(""===t[r]||void 0===t[r])};Object.keys(t).forEach((function(e){n(e)||(r[e]=t[e])}));var o=Object.keys(r);return o.sort(),o.map((function(r){var n=t[r];return void 0===n?"":null===n?I(r):Array.isArray(n)?n.reduce(C(e)(r),[]).join("&"):"".concat(I(r),"=").concat(I(n))})).filter((function(t){return t.length>0})).join("&")}function L(t,e){var r=Object.keys(t);if(Object.getOwnPropertySymbols){var n=Object.getOwnPropertySymbols(t);e&&(n=n.filter((function(e){return Object.getOwnPropertyDescriptor(t,e).enumerable}))),r.push.apply(r,n)}return r}function F(t){for(var e=1;e<arguments.length;e++){var r=null!=arguments[e]?arguments[e]:{};e%2?L(Object(r),!0).forEach((function(e){o()(t,e,r[e])})):Object.getOwnPropertyDescriptors?Object.defineProperties(t,Object.getOwnPropertyDescriptors(r)):L(Object(r)).forEach((function(e){Object.defineProperty(t,e,Object.getOwnPropertyDescriptor(r,e))}))}return t}var H=function(t){return"object"===c()(t)&&!Array.isArray(t)},N=function(t,e,r){var n,o=F(F({},{method:"GET",remote:!1,requestOptions:{baseUrl:null,formData:null,params:{},config:{},template:[]}}),e),i=o.requestOptions.formData?o.requestOptions.formData:o.requestOptions.params,s={"stencil-config":o.requestOptions.config?JSON.stringify(o.requestOptions.config):"{}","stencil-options":"{}","x-xsrf-token":window.BCData&&window.BCData.csrf_token?window.BCData.csrf_token:"","x-requested-with":"stencil-utils"};if(n=o.method,-1===["GET","POST","PUT","DELETE"].indexOf(n))return r(new Error("Not a valid HTTP method"));var u=function(t){var e=[];return H(t)?e=Object.values(t):"string"==typeof t?e=[t]:Array.isArray(t)&&t.length>0&&(e=t),e}(o.requestOptions.template),a=H(o.requestOptions.template),f=u.length>0;o.requestOptions.formData||(s["content-type"]="application/x-www-form-urlencoded; charset=UTF-8"),f&&(s["stencil-options"]=JSON.stringify({render_with:u.join(",")}));var l={method:o.method,headers:s,credentials:"include"},p=o.requestOptions.baseUrl?"".concat(o.requestOptions.baseUrl).concat(t):t;if(-1===["GET","HEAD"].indexOf(l.method))l.body=o.requestOptions.formData?i:G(i,{includeArrayIndex:!0});else if(i){var h=p.includes("?")?"&":"?";p+="".concat(h).concat(G(i))}return fetch(p,l).then((function(t){return-1!==t.headers.get("content-type").indexOf("application/json")?t.json():t.text()})).then((function(t){var e=o.remote?t.content:t,n=t;if(f){if("object"===c()(e)&&Object.keys(e).forEach((function(t){var r=t.replace(/^components\//,"");e[r]=e[t],delete e[t]})),a){var i=o.requestOptions.template;Object.keys(i).forEach((function(t){e[t]=e[i[t]],delete e[i[t]]}))}o.remote||(n=e)}r(null,n)})).catch((function(t){return r(t)}))},M=r(1),$=r.n(M),V=r(2),z=r.n(V),Q=r(3),J=r.n(Q),W=r(4),X=r.n(W),K=r(0),Y=r.n(K),Z=function(){function t(e){$()(this,t),this.remoteVersion=e||"v1",this.remoteBaseEndpoint="/remote/"}return z()(t,[{key:"makeRequest",value:function(t,e,r,n,o){N(t,{method:e,remote:n,requestOptions:r},o)}},{key:"remoteRequest",value:function(t,e,r,n){var o=this.remoteBaseEndpoint+this.remoteVersion+t;this.makeRequest(o,e,r,!0,n)}}]),t}();function tt(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=Y()(t);if(e){var o=Y()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return X()(this,r)}}var et=function(t){J()(r,t);var e=tt(r);function r(t){var n;return $()(this,r),(n=e.call(this,t)).endpoint="/country-states/",n}return z()(r,[{key:"getById",value:function(t,e){var r=this.endpoint+t;this.remoteRequest(r,"GET",{},e)}},{key:"getByName",value:function(t,e){var r=this.endpoint+t;this.remoteRequest(r,"GET",{},e)}}]),r}(Z);function rt(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=Y()(t);if(e){var o=Y()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return X()(this,r)}}var nt=function(t){J()(r,t);var e=rt(r);function r(t){var n;return $()(this,r),(n=e.call(this,t)).endpoint="/products.php?productId=",n}return z()(r,[{key:"getById",value:function(t,e,r){var n=this.endpoint+t,o=e,i=r;"function"==typeof o&&(i=o,o={}),this.makeRequest(n,"GET",o,!1,i)}}]),r}(Z),ot=r(6);function it(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=Y()(t);if(e){var o=Y()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return X()(this,r)}}var ct=function(t){J()(r,t);var e=it(r);function r(t){var n;return $()(this,r),(n=e.call(this,t)).endpoint="/product-attributes/",n.inCartEndpoint="/configure-options/",n}return z()(r,[{key:"optionChange",value:function(t,e){var r=arguments.length>2&&void 0!==arguments[2]?arguments[2]:null,n=arguments.length>3?arguments[3]:void 0,o=r,i=n;"function"==typeof o&&(i=o,o=null),this.remoteRequest(this.endpoint+t,"POST",{params:U(e),template:o},(function(t,e){var r={err:t,response:e};ot.a.emit("product-options-change-remote",r),i(t,e)}))}},{key:"configureInCart",value:function(t,e,r){this.remoteRequest(this.inCartEndpoint+t,"GET",e,(function(t,e){r(t,e)}))}}]),r}(Z);function st(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=Y()(t);if(e){var o=Y()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return X()(this,r)}}var ut=function(t){J()(r,t);var e=st(r);function r(t){var n;return $()(this,r),(n=e.call(this,t)).endpoint="/search.php?search_query=",n}return z()(r,[{key:"search",value:function(t,e,r){var n=this.endpoint+encodeURIComponent(t),o=e,i=r;"function"==typeof o&&(i=o,o={}),ot.a.emit("search-quick-remote",t),this.makeRequest(n,"GET",o,!1,i)}}]),r}(Z);function at(t){var e=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var r,n=Y()(t);if(e){var o=Y()(this).constructor;r=Reflect.construct(n,arguments,o)}else r=n.apply(this,arguments);return X()(this,r)}}var ft=function(t){J()(r,t);var e=at(r);function r(){return $()(this,r),e.apply(this,arguments)}return z()(r,[{key:"getCarts",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},e=arguments.length>1?arguments[1]:void 0,r="/api/storefront/carts";t.includeOptions&&(r=this.includeOptions(r)),this.makeRequest(r,"GET",t,!0,(function(t,r){e(t,r)}))}},{key:"getCart",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},e=arguments.length>1?arguments[1]:void 0;if(!t.cartId)return this.getCarts(t,(function(t,r){return e(t,r[0])}));var r="/api/storefront/carts/".concat(t.cartId);t.includeOptions&&(r=this.includeOptions(r)),this.makeRequest(r,"GET",t,!0,(function(t,r){e(t,r)}))}},{key:"includeOptions",value:function(t){return"".concat(t,"?include=lineItems.physicalItems.options,lineItems.digitalItems.options")}},{key:"getCartQuantity",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},e=arguments.length>1?arguments[1]:void 0;this.getCart(t,(function(t,r){if(t)return e(t);var n=0;if(r&&404!==r.status){var o=r;n=[o.lineItems.physicalItems,o.lineItems.digitalItems,o.lineItems.customItems].reduce((function(t,e){return t.concat(e)})).filter((function(t){return!t.parentId})).map((function(t){return t.quantity})).reduce((function(t,e){return t+e}),0)+o.lineItems.giftCertificates.length}e(null,n)}))}},{key:"itemAdd",value:function(t,e){this.remoteRequest("/cart/add","POST",{formData:t},(function(t,r){var n={err:t,response:r};ot.a.emit("cart-item-add-remote",n),e(t,r)}))}},{key:"itemUpdate",value:function(t,e,r){var n,o=r;Array.isArray(t)&&"function"==typeof e?(o=e,n=t):n=[{id:t,quantity:e}],this.update(n,(function(t,e){var r={items:n,err:t,response:e};ot.a.emit("cart-item-update-remote",r),o(t,e)}))}},{key:"itemRemove",value:function(t,e){var r=[{id:t,quantity:0}];this.update(r,(function(t,n){var o={items:r,err:t,response:n};ot.a.emit("cart-item-remove-remote",o),e(t,n)}))}},{key:"getItemGiftWrappingOptions",value:function(t,e,r){var n=e||{},o=r;"function"==typeof n&&(o=n,n={}),this.remoteRequest("/gift-wrapping/".concat(t),"GET",n,o)}},{key:"submitItemGiftWrappingOption",value:function(t,e,r){this.remoteRequest("/gift-wrapping/".concat(t),"POST",{params:e},r)}},{key:"update",value:function(t,e){var r={items:t};this.remoteRequest("/cart/update","POST",{params:r},e)}},{key:"getContent",value:function(t,e){var r=t||{},n=e;"function"==typeof r&&(n=r,r={}),this.makeRequest("/cart.php","GET",r,!1,n)}},{key:"getShippingQuotes",value:function(t,e,r){var n={params:t},o=r,i=e;"function"!=typeof o&&(o=i,i=null),i&&(n.template=i),this.remoteRequest("/shipping-quote","GET",n,o)}},{key:"submitShippingQuote",value:function(t,e){var r={params:{shipping_method:t}};this.remoteRequest("/shipping-quote","POST",r,e)}},{key:"applyCode",value:function(t,e){var r={params:{code:t}};this.remoteRequest("/apply-code","POST",r,e)}},{key:"applyGiftCertificate",value:function(t,e){var r={params:{code:t}};this.remoteRequest("/gift-certificates","POST",r,e)}}]),r}(Z),lt={getPage:function(t,e,r){N(t,{method:"GET",requestOptions:e},r)}};e.a={country:new et,productAttributes:new ct,product:new nt,search:new ut,cart:new ft,getPage:lt.getPage}},function(t,e,r){"use strict";var n=r(5),o=r.n(n),i=r(1),c=r.n(i),s=r(2),u=r.n(s),a=function(){function t(){c()(this,t)}return u()(t,[{key:"getSrc",value:function(t,e){var r;if("object"===o()(e)){var n=e.width||100,i=e.height||100;r="".concat(n,"x").concat(i)}else r="string"==typeof e&&/(^\d+w$)|(^(\d+?)x(\d+?)$)/g.test(e)?e:"original";return t.replace("{:size}",r)}}]),t}(),f=function(){function t(){c()(this,t)}return u()(t,[{key:"getSrcset",value:function(t,e){var r=/(^\d+w$)|(^(\d+?)x(\d+?)$)/,n=/(^\d+w$)|(^([0-9](\.[0-9]+)?)x)$/,o={};if(e){if(e!==Object(e)||Object.keys(e).some((function(t){return!(n.test(t)&&r.test(e[t]))})))throw new Error("Invalid srcset descriptor or size");if(o=e,1===Object.keys(o).length)return t.replace("{:size}",o[Object.keys(o)[0]])}else o={"80w":"80w","160w":"160w","320w":"320w","640w":"640w","960w":"960w","1280w":"1280w","1920w":"1920w","2560w":"2560w"};return Object.keys(o).map((function(e){return[t.replace("{:size}",o[e]),e].join(" ")})).join(", ")}}]),t}(),l=function(){function t(){c()(this,t)}return u()(t,[{key:"storageAvailable",value:function(t){var e=window[t];try{var r="__storage_test__";return e.setItem(r,r),e.removeItem(r),!0}catch(t){return t instanceof DOMException&&(22===t.code||1014===t.code||"QuotaExceededError"===t.name||"NS_ERROR_DOM_QUOTA_REACHED"===t.name)&&0!==e.length}}},{key:"localStorageAvailable",value:function(){return this.storageAvailable("localStorage")}}]),t}(),p={image:new a,imageSrcset:new f,storage:new l};e.a=p},function(t,e,r){var n=r(11);t.exports=function(t,e){if(t){if("string"==typeof t)return n(t,e);var r=Object.prototype.toString.call(t).slice(8,-1);return"Object"===r&&t.constructor&&(r=t.constructor.name),"Map"===r||"Set"===r?Array.from(t):"Arguments"===r||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(r)?n(t,e):void 0}}},function(t,e){t.exports=function(t,e){(null==e||e>t.length)&&(e=t.length);for(var r=0,n=new Array(e);r<e;r++)n[r]=t[r];return n}},function(t,e,r){"use strict";var n=Object.prototype.hasOwnProperty,o="~";function i(){}function c(t,e,r){this.fn=t,this.context=e,this.once=r||!1}function s(t,e,r,n,i){if("function"!=typeof r)throw new TypeError("The listener must be a function");var s=new c(r,n||t,i),u=o?o+e:e;return t._events[u]?t._events[u].fn?t._events[u]=[t._events[u],s]:t._events[u].push(s):(t._events[u]=s,t._eventsCount++),t}function u(t,e){0==--t._eventsCount?t._events=new i:delete t._events[e]}function a(){this._events=new i,this._eventsCount=0}Object.create&&(i.prototype=Object.create(null),(new i).__proto__||(o=!1)),a.prototype.eventNames=function(){var t,e,r=[];if(0===this._eventsCount)return r;for(e in t=this._events)n.call(t,e)&&r.push(o?e.slice(1):e);return Object.getOwnPropertySymbols?r.concat(Object.getOwnPropertySymbols(t)):r},a.prototype.listeners=function(t){var e=o?o+t:t,r=this._events[e];if(!r)return[];if(r.fn)return[r.fn];for(var n=0,i=r.length,c=new Array(i);n<i;n++)c[n]=r[n].fn;return c},a.prototype.listenerCount=function(t){var e=o?o+t:t,r=this._events[e];return r?r.fn?1:r.length:0},a.prototype.emit=function(t,e,r,n,i,c){var s=o?o+t:t;if(!this._events[s])return!1;var u,a,f=this._events[s],l=arguments.length;if(f.fn){switch(f.once&&this.removeListener(t,f.fn,void 0,!0),l){case 1:return f.fn.call(f.context),!0;case 2:return f.fn.call(f.context,e),!0;case 3:return f.fn.call(f.context,e,r),!0;case 4:return f.fn.call(f.context,e,r,n),!0;case 5:return f.fn.call(f.context,e,r,n,i),!0;case 6:return f.fn.call(f.context,e,r,n,i,c),!0}for(a=1,u=new Array(l-1);a<l;a++)u[a-1]=arguments[a];f.fn.apply(f.context,u)}else{var p,h=f.length;for(a=0;a<h;a++)switch(f[a].once&&this.removeListener(t,f[a].fn,void 0,!0),l){case 1:f[a].fn.call(f[a].context);break;case 2:f[a].fn.call(f[a].context,e);break;case 3:f[a].fn.call(f[a].context,e,r);break;case 4:f[a].fn.call(f[a].context,e,r,n);break;default:if(!u)for(p=1,u=new Array(l-1);p<l;p++)u[p-1]=arguments[p];f[a].fn.apply(f[a].context,u)}}return!0},a.prototype.on=function(t,e,r){return s(this,t,e,r,!1)},a.prototype.once=function(t,e,r){return s(this,t,e,r,!0)},a.prototype.removeListener=function(t,e,r,n){var i=o?o+t:t;if(!this._events[i])return this;if(!e)return u(this,i),this;var c=this._events[i];if(c.fn)c.fn!==e||n&&!c.once||r&&c.context!==r||u(this,i);else{for(var s=0,a=[],f=c.length;s<f;s++)(c[s].fn!==e||n&&!c[s].once||r&&c[s].context!==r)&&a.push(c[s]);a.length?this._events[i]=1===a.length?a[0]:a:u(this,i)}return this},a.prototype.removeAllListeners=function(t){var e;return t?(e=o?o+t:t,this._events[e]&&u(this,e)):(this._events=new i,this._eventsCount=0),this},a.prototype.off=a.prototype.removeListener,a.prototype.addListener=a.prototype.on,a.prefixed=o,a.EventEmitter=a,t.exports=a},function(t,e){t.exports=function(t,e,r){return e in t?Object.defineProperty(t,e,{value:r,enumerable:!0,configurable:!0,writable:!0}):t[e]=r,t}},function(t,e,r){var n=r(19),o=r(20),i=r(10),c=r(21);t.exports=function(t,e){return n(t)||o(t,e)||i(t,e)||c()}},function(t,e,r){"use strict";r.r(e),function(t){var n=r(5),o=r.n(n),i=r(6);r.d(e,"hooks",(function(){return i.a}));var c=r(8);r.d(e,"api",(function(){return c.a}));var s=r(9);r.d(e,"tools",(function(){return s.a}));var u,a={hooks:i.a,api:c.a,tools:s.a};e.default=a,u=void 0,"function"==typeof define&&r(25)&&u?define((function(){u.stencilUtils=a})):"object"===o()(t)&&t.exports?t.exports=a:window.stencilUtils=a}.call(this,r(16)(t))},function(t,e){t.exports=function(t){if(!t.webpackPolyfill){var e=Object.create(t);e.children||(e.children=[]),Object.defineProperty(e,"loaded",{enumerable:!0,get:function(){return e.l}}),Object.defineProperty(e,"id",{enumerable:!0,get:function(){return e.i}}),Object.defineProperty(e,"exports",{enumerable:!0}),e.webpackPolyfill=1}return e}},function(t,e){function r(e,n){return t.exports=r=Object.setPrototypeOf||function(t,e){return t.__proto__=e,t},r(e,n)}t.exports=r},function(t,e){t.exports=function(t){if(void 0===t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return t}},function(t,e){t.exports=function(t){if(Array.isArray(t))return t}},function(t,e){t.exports=function(t,e){if("undefined"!=typeof Symbol&&Symbol.iterator in Object(t)){var r=[],n=!0,o=!1,i=void 0;try{for(var c,s=t[Symbol.iterator]();!(n=(c=s.next()).done)&&(r.push(c.value),!e||r.length!==e);n=!0);}catch(t){o=!0,i=t}finally{try{n||null==s.return||s.return()}finally{if(o)throw i}}return r}}},function(t,e){t.exports=function(){throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}},function(t,e,r){var n=r(11);t.exports=function(t){if(Array.isArray(t))return n(t)}},function(t,e){t.exports=function(t){if("undefined"!=typeof Symbol&&Symbol.iterator in Object(t))return Array.from(t)}},function(t,e){t.exports=function(){throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}},function(t,e){(function(e){t.exports=e}).call(this,{})}]);
import Swal from 'sweetalert2';
import {normalizeFormData } from '/var/www/html/Furniture/Bundle/src/api.js';
require('/var/www/html/Furniture/Bundle/src/grid.css');
console.log("Entered in server side bundle script");


var CartProducts=null;
var CartProducts_bundle = null;
var store_hash = $("#bundle").attr('data-site');
var product_id = $("#bundle").attr('data-product-id');
var main_product_name = $("#bundle").attr('data-product-name');

var fetch_bundle_kit_details = "https://bundling.arizonreports.cloud/Furniture/Bundling_App/product-cart-validation";
var get_cart_details = "https://bundling.arizonreports.cloud/Furniture/Bundling_App/get-cart-bundle-kit";
var add_products_to_cart = "https://bundling.arizonreports.cloud/Furniture/Bundling_App/add-products-cart";
var cart_validation = "https://bundling.arizonreports.cloud/Furniture/Bundling_App/get-cart-validation";
var cart_line_item_delete =  "https://bundling.arizonreports.cloud/Furniture/Bundling_App/cart-line-item-delete";
var manage_cart_details ="https://bundling.arizonreports.cloud/Furniture/Bundling_App/manage_cart_details";
var fetch_settings = "https://bundling.arizonreports.cloud/Furniture/Bundling_App/get-settings";

console.log("store_hash");
console.log(store_hash);
var button_status = false;
var product_bundle_kit_details = false;
const $form = $('form[data-cart-item-add]');
console.log($form);
console.log("add_to_cart_from");
const $cartContent = $('[data-cart-content]');
const $cartMessages = $('[data-cart-status]');
const $cartTotals = $('[data-cart-totals]');
var $overlay = $('[data-cart-item-add] .loadingOverlay');
var $overlay_cart = $('[data-cart] .loadingOverlay').hide();
var previewModal = $('#previewModal');
var view_settings = "";
var cart_id;
var mandatory;
var type = null;
var main_product_qty = null;
var backend_cart_details = [];
var main_product_cart_details = [];
var total_kit_price = "";


$(document).on("click", "button.button.button--icon", function() {
    if (location.pathname == "/cart.php") {
        setTimeout(function() {
            cart_off();
        }, 1000);
        setTimeout(function() {
            cart_off();
        }, 3000);
        setTimeout(function() {
            cart_off();
        }, 2000);
        setTimeout(function() {
            cart_off();
        }, 4000);

		  /* var cart_btn_disabled = setInterval(function() {
     //console.log('true 1');
		 cart_off();
    }, 100);
    setTimeout(function() {
     clearInterval(cart_btn_disabled);
    }, 7000);*/
        /// window.location.reload();

    }
});

if (location.pathname == "/cart.php") {
    cart_off();

}

function cart_off() {

    $("td.cart-item-block.cart-item-info").find("a").removeClass("cart-remove");
    $("td.cart-item-block.cart-item-info").find("a").addClass("cart-remove-custom");
    console.log("get-cart-bundle-kit");
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState !== 4) return; {
            console.log(JSON.parse(xhr.responseText));
            var cart_data = JSON.parse(xhr.responseText);
            if (cart_data.length) {
                var cart_id = cart_data[0].id;
                $.ajax({
                    url: get_cart_details,
                    type: "get",
                    cache: false,
                    async: false,
                    data: {
                        cart_id: cart_id,
						store:store_hash,
		store_hash:store_hash
                    },
                    success: function(result) {
                        console.log(result);
                        var result = JSON.parse(result);
                        var line_item_details = result[0]["param_cart_lineitem_id"];
                        console.log("line_item_details");
                        console.log(line_item_details);
                        console.log("result_kit");
                        if (line_item_details != null) {
                            line_item_details = line_item_details.split(',');
                            $.each(line_item_details, function(index, value) {
                                console.log("result_kit1");
                                console.log(value);
                                $('button[data-cart-itemid=' + value + ']').prop("disabled", true);
                                $('input#qty-' + value).prop("disabled", true);
								if(result[0]['param_main_product_cart_lineitem_id'].includes(value)){}
                                //if (result[0]['param_main_product_cart_lineitem_id'] == value) {} 
								else {
                                    console.log("cart_kit_test");
                                    $('.cart-remove-custom[data-cart-itemid=' + value + ']').remove();
                                }
                            });
                        }
                    }
                });
            }
        }
    };
    xhr.open('GET', '/api/storefront/carts?include=lineItems.digitalItems.options,lineItems.physicalItems.options');
    xhr.send();
}
$.ajax({
    url: fetch_bundle_kit_details,
    type: "get",
    cache: false,
    async: false,
    data: {
        destination_product_id: product_id,
		store:store_hash,
		store_hash:store_hash
    },
    success: function(result) {
        console.log("bundle_kit_checking");
        var result = JSON.parse(result);
        console.log(result);
        if (result.length != 0) {
            var mandatory_load;
            type = result[0]['bundlekit_type'];
            mandatory_load = result[0]['product_mandatory'];
            total_kit_price = result[0]['total_bundle_price'];
            console.log("mandatory_load");
            console.log(mandatory_load);
            console.log(type);
            console.log("type_type");
            product_bundle_kit_details = JSON.parse(result[0]['product_details']);
            console.log("product_bundle_kit_details " + product_bundle_kit_details);
            if (type == "Kit") {
                console.log("Kit products");
               
                $(".form-action").css("margin-right", "0");
           
            } else if (type == "Bundle") {
                if (mandatory_load && mandatory_load == 'true') {
                    
                }
            }
        }
    }
});
//$(document).on("click", "#form-action-addToCart", function(event) {
$form.on('submit', event => {
    console.log(event);
    event.preventDefault();
    console.log("add_to_cart_submit");
    /*if (type == "Kit") {
        var validate = validation(null);
        if (validate == false) {
            var bundle_option_mandatory_alert = "<p >Please Select the options in products<br /></p>";
            return Swal.fire({
                customClass: 'bundle_product_option_details',
                html: '<strong>' + bundle_option_mandatory_alert + '</strong>',
                showConfirmButton: false,
                showCloseButton: true
            });

        }
    }
    if (type == "Bundle") {
        console.log("testBundle");
        var validate = validation(null);
        if (validate == false) {
            var bundle_option_mandatory_alert = "<p >Please Select the options in products<br /></p>";
            return Swal.fire({
                customClass: 'bundle_product_option_details',
                html: '<strong>' + bundle_option_mandatory_alert + '</strong>',
                showConfirmButton: false,
                showCloseButton: true
            });

        }
    }*/
    addProductToCart(event, $form[0], $overlay, previewModal);
});

function addProductToCart(event, form, $overlay, previewModal) {
    const $addToCartBtn = $('#form-action-addToCart', $(event.target));
    const originalBtnVal = $addToCartBtn.val();
    const waitMessage = $addToCartBtn.data('waitMessage');
    var mandatory;
    var bundlekit_id = null;
    var physical_items;
    var cart_response = null;

    console.log("previewModal");
    console.log(previewModal);

    // Do not do AJAX if browser doesn't support FormData
    if (window.FormData === undefined) {
        return;
    }
    var that = this;
    $addToCartBtn
        .val(waitMessage)
        .prop('disabled', true);
    $overlay.show();


    var bundle_mandatory_alert;
    $.ajax({
        url: fetch_bundle_kit_details,
        type: "get",
        cache: false,
        async: false,
        data: {
            destination_product_id: product_id,
				store:store_hash,
		store_hash:store_hash
        },
        success: function(result) {
            console.log("result");
            console.log(result);

            var result = JSON.parse(result);
            console.log(result);
            console.log(result.length);
            var product_details;
            if (result.length != 0) {
                mandatory = result[0]['product_mandatory'];
                product_details = JSON.parse(result[0]['product_details']);
                bundlekit_id = result[0]['bundlekit_id'];
                console.log("mandatory");
                console.log(mandatory);
                console.log(product_details);
                console.log("product_details");
            }

            if (mandatory != "true" || result.length == 0) {
                console.log("inside_if");
                // Add item to cart
                window.stencilUtils.api.cart.itemAdd(normalizeFormData(new FormData(form)), (err, response) => {
                    console.log("cart_response123567");
                    console.log(response);
                    cart_response = response;

                    main_product_cart_details.push({
                        "main_product_id": response.data.cart_item.product_id,
                        "line_item": response.data.cart_item.id,
                        "type": type,
                        "bundlekit_id": bundlekit_id
                    });
                    console.log("main_product_cart_details");
                    console.log(main_product_cart_details);
                    console.log("errorMessage");
                    console.log(errorMessage);

                    const errorMessage = err || response.data.error;

                    $addToCartBtn
                        .val(originalBtnVal)
                        .prop('disabled', false);

                    $overlay.hide();

                    // Guard statement
                    if (errorMessage) {
                        // Strip the HTML from the error message
                        const tmp = document.createElement('DIV');
                        tmp.innerHTML = errorMessage;

                        return showAlertModal(tmp.textContent || tmp.innerText);
                    }

                   /* var productAddOn = localStorage.getItem("CartProducts");
                    if (localStorage.getItem('CartProducts') !== null) {
                        productAddOnData = JSON.parse(productAddOn);
                    }*/
					console.log(view_settings);
					console.log("view_settings");
					console.log("test"+ CartProducts.length);
					console.log("CartProducts");
					
					if(view_settings == 'embedded' && CartProducts.length == 0){
						console.log(CartProducts_bundle);
						console.log("import-CartProducts_bundle");
					}
					var productAddOnData = JSON.parse(CartProducts);
                    console.log("productAddOn");
                    console.log(productAddOnData);


                    var cart_id = null;
                    console.log("lenght");
                    var details = [];

                    //console.log(Object.keys(productAddOnData).length > 0);
                    if (productAddOnData && Object.keys(productAddOnData).length > 0) {
                        var xhr = new XMLHttpRequest();
                        var parent_product_lineitem_id = "";
                        var parent_product_id = "";
                        var main_product_qty;

                        xhr.onreadystatechange = function() {
                            if (xhr.readyState !== 4) return; {
                                console.log("cart_response11");
                                console.log(JSON.parse(xhr.responseText));
                                var cart_data = JSON.parse(xhr.responseText);
                                if (cart_data.length) {
                                    cart_id = cart_data[0].id;
                                    localStorage.setItem("CartID", cart_id);
                                    $.each(cart_data[0].lineItems.physicalItems, function(index, val) {
                                        if (product_id == val["productId"]) {
                                            parent_product_lineitem_id = val["id"];
                                            parent_product_id = val["productId"];
                                            console.log("parent_product_id " + parent_product_id);
                                        }
                                    });


                                    var index_value = 0;

                                    console.log(cart_id);
                                    console.log("cart_id");
                                    var parent_product_qty = $('[name=qty\\[\\]]').val();
                                    if (parent_product_qty == 1) {
                                        main_product_qty = 1;
                                    } else {
                                        main_product_qty = parent_product_qty;
                                    }
                                    $.each(productAddOnData, function(key, val) {

                                        index_value = index_value + 1;
                                        console.log(key + val);
                                        var product_id = key;
                                        console.log(product_id);
                                        var list_price = null;
                                        // var sku = val.split(',')[0].trim();
                                        var quantity;
                                        var variant_id;
                                        console.log(key + val);
                                        var product_id = key;
                                        console.log(product_id);
                                        var list_price = null;
                                        if (val && val.toString().indexOf(",") != -1) {
                                            // var sku = val.split(',')[0].trim();
                                            var quantity = val.split(',')[0].trim();
                                            quantity = parseInt(quantity) * parseInt(main_product_qty);

                                            if (val.split(',')[1] && val.split(',')[1] != "") {
                                                variant_id = val.split(',')[1].trim();
                                            } else {
                                                variant_id = null;
                                            }
                                        } else if (val) {

                                            quantity = val * parseInt(main_product_qty);
                                            variant_id = null;
                                        } else {
                                            quantity = 1 * parseInt(main_product_qty);
                                            variant_id = null;
                                        }
                                       
                                        $.each(product_details, function(index, data) {
                                            if (data.product_id == product_id) {
                                                console.log("inside");
                                                list_price = data.adjusted_price;
                                                console.log("list_price");
                                                console.log(list_price);
                                                val = val + "," + data.adjusted_price;
                                               
                                                details.push({
                                                    "product_id": product_id,
                                                    "quantity": quantity,
                                                    "variant_id": variant_id,
                                                    "list_price": list_price,
                                                    "cart_id": cart_id
                                                });
                                                console.log("details_array");
                                                console.log(details);
                                            }
                                        });

                                    });
                                    var cart_id_data = localStorage.getItem("CartID");
                                    console.log(cart_id_data)
                                    console.log("cart_id");
                                    console.log(productAddOnData)
                                    console.log("productAddOnData");
                                    var data_array = [];
                                    var arr = {
                                        "product_details": details
                                    };
                                   
                                    console.log("product_details");
                                    console.log(product_details);
                                    var json = JSON.stringify(arr);


                                    $.ajax({
                                        url: add_products_to_cart,
                                        type: "post",
                                        cache: false,
                                        async: false,
                                        data: {
                                            parent_product_lineitem_id: parent_product_lineitem_id,
                                            parent_product_id: parent_product_id,
                                            parent_product_qty: parent_product_qty,
                                            product_details: json,
											store:store_hash,
											store_hash:store_hash
                                        },
                                        success: function(result) {


                                            var result = JSON.parse(result);
                                            console.log("result_important");
                                            console.log(result);
                                            var line_items = result["data"]["line_items"]["physical_items"];
                                            var cart_id = result["data"]["id"];

                                            console.log(line_items);
                                            $.each(line_items, function(index, value) {
                                                console.log(value);
                                                console.log(value.id);
                                                console.log("physical_items");
                                                backend_cart_details.push({
                                                    "bundle_kit_id": bundlekit_id,
                                                    "product_id": value.product_id,
                                                    "adjusted_price": value.list_price,
                                                    "cart_line_item_id": value.id,
                                                    "bundle_kit_type": type
                                                });

                                            });
											var backend_cart_details_json = JSON.stringify(backend_cart_details);
                                            $.ajax({
                                                url: manage_cart_details,
                                                type: "post",
                                                cache: false,
                                                async: false,
                                                data: {
                                                    product_details: backend_cart_details_json,
                                                    cart_id: cart_id,
													store:store_hash,
		store_hash:store_hash
                                                },
                                                success: function(data) {
                                                    console.log("manage_data");
                                                    console.log(data);
                                                }
                                            });
                                            console.log(Object.keys(productAddOnData).length);
                                            console.log(index_value + 1);
                                            if ((index_value) == Object.keys(productAddOnData).length) {
                                                // Open preview modal and update content
                                                if (previewModal) {
                                                    updateCartContent(previewModal, response.data.cart_item.id);
                                                    localStorage.removeItem("productOptionAddOns");
                                                } else {
                                                    $overlay.show();
                                                    // if no modal, redirect to the cart page
                                                    redirectTo(response.data.cart_item.cart_url);
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        };
                        xhr.open('GET', '/api/storefront/carts?include=lineItems.digitalItems.options,lineItems.physicalItems.options');
                        xhr.send();

                    } else {
                        //   main_product_cart_addition(type, bundlekit_id);
                        // Open preview modal and update content
                        if (previewModal) {


                            updateCartContent(previewModal, response.data.cart_item.id);
                            localStorage.removeItem("productOptionAddOns");
                        } else {
                            $overlay.show();
                            // if no modal, redirect to the cart page
                            redirectTo(response.data.cart_item.cart_url);
                        }
                        console.log("main_product_cart_addition");

                    }

                    // Open preview modal and update content

                });
            } else {
                console.log("outside_if");
                var mandatory_products = [];
                var cart_products = [];
                var status = false;
                var mandatory_products_ids = [];
                $.each(product_details, function(index, data) {
                    console.log(data);
                    console.log(data.is_mandatory);
                    console.log(data.product_id);
                    console.log(data.product_name);
                    if (data.is_mandatory == true) {
                        console.log("yes");
                        mandatory_products_ids.push(data.product_id);
                        mandatory_products.push(data.product_name);
                    } else {
                        console.log("no");
                    }
                });
                console.log(mandatory_products_ids);
                console.log("mandatory_products_ids");
                var myNewArray = mandatory_products.filter(function(elem, index, self) {
                    return index === self.indexOf(elem);
                });
                //var productAddOn = localStorage.getItem("CartProducts");
                //var productAddOnData = JSON.parse(productAddOn);
				var  productAddOnData = JSON.parse(CartProducts);
				console.log("productAddOnData_add_to_cart");
				console.log(productAddOnData);
                if (productAddOnData && Object.keys(productAddOnData).length > 0) {

                    $.each(productAddOnData, function(key, val) {
                        var product_id = key;
                        cart_products.push(parseInt(product_id));
                        console.log(key + val);


                    });
                    console.log(cart_products);
                    console.log("cart_products");
                    $.each(mandatory_products_ids, function(key, val) {
                        console.log("val");
                        console.log(val);
                        if (cart_products.indexOf(val) !== -1) {
                            console.log("is in array");
                            status = true;
                        } else {
                            console.log("is not in array");
                            status = false;
                            return false;
                        }
                    });
                    if (status == true) {
                        window.stencilUtils.api.cart.itemAdd(normalizeFormData(new FormData(form)), (err, response) => {
                            console.log(response);
                            console.log("response_first");
                            const errorMessage = err || response.data.error;
                            console.log(errorMessage);
                            console.log("errorMessage");

                            $addToCartBtn
                                .val(originalBtnVal)
                                .prop('disabled', false);

                            $overlay.hide();

                            // Guard statement
                            if (errorMessage) {
                                // Strip the HTML from the error message
                                const tmp = document.createElement('DIV');
                                tmp.innerHTML = errorMessage;

                                // return showAlertModal(tmp.textContent || tmp.innerText);
                            }
                            //var productAddOn = localStorage.getItem("CartProducts");
                            //var productAddOnData = JSON.parse(productAddOn);
							var  productAddOnData = JSON.parse(CartProducts);
                            console.log("productAddOn");
                            console.log(productAddOnData);


                            var cart_id = null;
                            console.log("lenght");
                            var details = [];
                            //console.log(Object.keys(productAddOnData).length > 0);
                            if (productAddOnData && Object.keys(productAddOnData).length > 0) {
                                var xhr = new XMLHttpRequest();
                                var parent_product_lineitem_id = "";
                                var parent_product_id = "";
                                var main_product_qty;
                                xhr.onreadystatechange = function() {
                                    if (xhr.readyState !== 4) return; {
                                        console.log("cart_response12");
                                        console.log(product_id);
                                        console.log(JSON.parse(xhr.responseText));
                                        var cart_data = JSON.parse(xhr.responseText);
                                        if (cart_data.length) {
                                            cart_id = cart_data[0].id;
                                            localStorage.setItem("CartID", cart_id);
                                            $.each(cart_data[0].lineItems.physicalItems, function(index, val) {
                                                if (product_id == val["productId"]) {
                                                    parent_product_lineitem_id = val["id"];
                                                    parent_product_id = val["productId"];
                                                    console.log("parent_product_id " + parent_product_id);
                                                }
                                            });

                                            var index_value = 0;

                                            console.log(cart_id);
                                            console.log("cart_id");
                                            var parent_product_qty = $('[name=qty\\[\\]]').val();
                                            if (parent_product_qty == 1) {
                                                main_product_qty = 1;
                                            } else {
                                                main_product_qty = parent_product_qty;
                                            }
                                            $.each(productAddOnData, function(key, val) {

                                                index_value = index_value + 1;
                                                var quantity;
                                                var variant_id;
                                                console.log(key + val);
                                                var product_id = key;
                                                console.log(product_id);
                                                var list_price = null;
                                                if (val && val.toString().indexOf(",") != -1) {
                                                    // var sku = val.split(',')[0].trim();
                                                    var quantity = val.split(',')[0].trim();
                                                    quantity = parseInt(quantity) * parseInt(main_product_qty);

                                                    if (val.split(',')[1] && val.split(',')[1] != "") {
                                                        variant_id = val.split(',')[1].trim();
                                                    } else {
                                                        variant_id = null;
                                                    }
                                                } else if (val) {
                                                    quantity = val * parseInt(main_product_qty);
                                                    variant_id = null;
                                                } else {
                                                    quantity = 1 * parseInt(main_product_qty);
                                                    variant_id = null;
                                                }
                                                // details["sku"] = sku;
                                                //details["quantity"] = quantity;
                                                //details["variant_id"] = variant_id;
                                                $.each(product_details, function(index, data) {
                                                    if (data.product_id == product_id) {
                                                        console.log("inside");
                                                        list_price = data.adjusted_price;
                                                        console.log("list_price");
                                                        console.log(list_price);

                                                        // details["adjusted_price"] = data.adjusted_price;

                                                        //productAddOnData[product_id] = details;

                                                        details.push({
                                                            "product_id": product_id,
                                                            "quantity": quantity,
                                                            "variant_id": variant_id,
                                                            "list_price": list_price,
                                                            "cart_id": cart_id
                                                        });
                                                        console.log("details_array");
                                                        console.log(details);
                                                    }
                                                });

                                                //var sku = val.split(',')[0].trim();
                                                //var quantity = val.split(',')[1].trim();

                                                //Bundling_cart(productAddOnData);
                                                /**/
                                            });
                                            var cart_id_data = localStorage.getItem("CartID");
                                            console.log(cart_id_data)
                                            console.log("cart_id");
                                            console.log(productAddOnData)
                                            console.log("productAddOnData");
                                            var data_array = [];
                                            var arr = {
                                                "product_details": details
                                            };
                                            //var myJSON = JSON.stringify(product_details);
                                            console.log("product_details");
                                            console.log(product_details);
                                            var json = JSON.stringify(arr);

                                            /*$.post("https://bundling.arizonreports.cloud/Furniture/Bundling_App/add-products-cart?store=" + store, {
                                                    "product_details": data_array
                                                },
                                                function(data, status) {});*/


                                            $.ajax({
                                                url: add_products_to_cart,
                                                type: "post",
                                                cache: false,
                                                async: false,
                                                data: {
                                                    parent_product_lineitem_id: parent_product_lineitem_id,
                                                    parent_product_id: parent_product_id,
                                                    parent_product_qty: parent_product_qty,
                                                    product_details: json,
													store:store_hash,
													store_hash:store_hash
                                                },
                                                success: function(result) {

                                                    console.log("result-cart_api123");

                                                    var result = JSON.parse(result);
                                                    console.log(result);
													console.log(result.status);
													console.log(result.title);
													var message_alert = "";
													if(result.title){ message_alert = result.title;}

													console.log("message_alert");
													console.log(message_alert);
													if(message_alert != ""){
														$(".bliss-dark-bk").css("display","none");
														$(".navUser-item--cart .dropdown-menu").removeClass("is-open")
														 console.log("result_cart_result_inventory");
														 console.log(result);
														 return Swal.fire({
															customClass: 'bundle_product_details',
															html: "<strong>The product '"+main_product_name+"' is currently out of stock. Hence you can't add this Bundle to the Cart</strong>",
															icon: 'warning',
															showCancelButton: false
														});
															
													}else{
                                                    var line_items = result["data"]["line_items"]["physical_items"];
                                                    var cart_id = result["data"]["id"];

                                                    console.log(line_items);
                                                    $.each(line_items, function(index, value) {
                                                        console.log(value);
                                                        console.log(value.id);
                                                        console.log("physical_items");
                                                        backend_cart_details.push({
                                                            "bundle_kit_id": bundlekit_id,
                                                            "product_id": value.product_id,
                                                            "adjusted_price": value.list_price,
                                                            "cart_line_item_id": value.id,
                                                            "bundle_kit_type": type
                                                        });
                                                    });
                                                    var backend_cart_details_json = JSON.stringify(backend_cart_details);
                                                    $.ajax({
                                                        url: manage_cart_details,
                                                        type: "post",
                                                        cache: false,
                                                        async: false,
                                                        data: {
                                                            product_details: backend_cart_details_json,
                                                            cart_id: cart_id,
															store:store_hash,
		store_hash:store_hash
                                                        },
                                                        success: function(data) {
                                                            console.log("manage_data");
                                                            console.log(data);
                                                        }
                                                    });

                                                    console.log(backend_cart_details);
                                                    console.log("backend_cart_details1");
                                                    console.log(Object.keys(productAddOnData).length);
                                                    console.log(index_value + 1);
                                                    if ((index_value) == Object.keys(productAddOnData).length) {
                                                        // Open preview modal and update content
                                                        if (previewModal) {

                                                            console.log("response");
                                                            console.log(response);
                                                            updateCartContent(previewModal, response.data.cart_item.id);
                                                            localStorage.removeItem("productOptionAddOns");
                                                        } else {
                                                            $overlay.show();
                                                            // if no modal, redirect to the cart page
                                                            redirectTo(response.data.cart_item.cart_url);
                                                        }
                                                    }
                                                }

											}
                                            });
                                        }
                                    }
                                };
                                xhr.open('GET', '/api/storefront/carts?include=lineItems.digitalItems.options,lineItems.physicalItems.options');
                                xhr.send();

                            } else {
                                // Open preview modal and update content
                                if (previewModal) {


                                    updateCartContent(previewModal, response.data.cart_item.id);
                                    localStorage.removeItem("productOptionAddOns");
                                } else {
                                    $overlay.show();
                                    // if no modal, redirect to the cart page
                                    redirectTo(response.data.cart_item.cart_url);
                                }
                            }

                            // Open preview modal and update content

                        });

                    } else {

                        mandatory_products = myNewArray.toString().replace(/,/g, "<br /><br/>");
                        bundle_mandatory_alert = "<div class='bundle_mandatory_alert'><p >This bundle has mandatory products.  If you would like to shop further, please add these mandatory products <br /><p style='text-align:left;'> " + mandatory_products + ".</p></p></div>";
                        $addToCartBtn
                            .val(originalBtnVal)
                            .prop('disabled', false);
                        return Swal.fire({
                            customClass: 'bundle_product_details',
                            html: '<strong>' + bundle_mandatory_alert + '</strong>',
                            showConfirmButton: false,
                            showCloseButton: true
                        });
                    }
                } else {
                    mandatory_products = myNewArray.toString().replace(/,/g, "<br /><br/>");
                    bundle_mandatory_alert = "<div class='bundle_mandatory_alert'><p >This bundle has mandatory products.  If you would like to shop further, please add these mandatory products <br /><p style='text-align:left;'> " + mandatory_products + ".</p></p></div>";
                    $addToCartBtn
                        .val(originalBtnVal)
                        .prop('disabled', false);
                    return Swal.fire({
                        customClass: 'bundle_product_details',
                        html: '<strong>' + bundle_mandatory_alert + '</strong>',
                        showConfirmButton: false,
                        showCloseButton: true
                    });
                }


            }
        }
    });

}
//$(document).ready(function() {	
var product_details = {};
var product_details_bundle_kit = {};
var product_string = '';
var display_string = "";
var custom_fields_name = "";
var collection_skus = null;
var bundle_kit_price = 0;
var param_is_bundle_product_display = "";
var param_is_embedded_display = "";
var embed_content = "";


if (product_bundle_kit_details != false) {
    console.log(type);
    console.log("type_test");
    embed_content = embed_content + '<div id="bliss-long-description" class="hidden-xs">This is a Bundle product..<ul>This Bundle Contains -&nbsp;';
    $.each(product_bundle_kit_details, function(index, data) {
        console.log(product_bundle_kit_details);
        if (type == "Kit") {
            console.log(data);
            console.log("bundle_kit_data123");
            if (data.product_id) {
                console.log("inside");
                //	list_price = data.adjusted_price;
                //	console.log("list_price");
                //	console.log(list_price);
                var text = data.qty;
                if (data.qty != null) {
                    text = data.qty;
                } else {
                    text = 1;
                }
                if (data.destination_variant_id) {
                    product_details_bundle_kit[data.product_id] = text + "," + data.destination_variant_id;
                } else {
                    product_details_bundle_kit[data.product_id] = text;
                }
                product_details[data.product_id] = text;
                console.log(product_details_bundle_kit);
                console.log("product_details_bundle_kit");
				CartProducts = JSON.stringify(product_details_bundle_kit);
               // localStorage.setItem("CartProducts", JSON.stringify(product_details_bundle_kit));
            }
        }
        if (type == "Bundle") {
            console.log(data);
            console.log("bundle_kit_data");
            if (data.product_id && data.is_mandatory == true) {
                console.log("inside_bundle_kit_data");
                //	list_price = data.adjusted_price;
                //	console.log("list_price");
                //	console.log(list_price);
                var text = null;
                if (data.qty != null) {
                    text = data.qty;
                } else {
                    text = 1;
                }
                if (data.destination_variant_id) {
                    product_details_bundle_kit[data.product_id] = text + "," + data.destination_variant_id;
                } else {
                    product_details_bundle_kit[data.product_id] = text;
                }
                console.log(product_details_bundle_kit);
                console.log("product_details_bundle_kit");
				CartProducts_bundle = JSON.stringify(product_details_bundle_kit);
				CartProducts = JSON.stringify(product_details_bundle_kit);
                //localStorage.setItem("CartProducts_bundle", JSON.stringify(product_details_bundle_kit));
                //	localStorage.setItem("CartProducts", JSON.stringify(product_details_bundle_kit));
            }else{
				console.log("inside_bundle_kit_data");
                //	list_price = data.adjusted_price;
                //	console.log("list_price");
                //	console.log(list_price);
                var text = null;
                if (data.qty != null) {
                    text = data.qty;
                } else {
                    text = 1;
                }
                if (data.destination_variant_id) {
                    product_details_bundle_kit[data.product_id] = text + "," + data.destination_variant_id;
                } else {
                    product_details_bundle_kit[data.product_id] = text;
                }
                console.log(product_details_bundle_kit);
                console.log("product_details_bundle_kit");
				CartProducts_bundle = JSON.stringify(product_details_bundle_kit);
				
			}
        }

        if (data.product_name) {
            if (data.qty != null) {
                text = data.qty;
            } else {
                text = 1;
            }

            embed_content = embed_content + '<li class="embed_description">' + text + ' X &nbsp;' + data.product_name + '</li>';
            if (data.option_details != null) {
                embed_content = embed_content + '<li class="embed_description_option" style="list-style:inside;"> Variant - ' + data.option_details + '</li>';

            }
        }

    });
    embed_content = embed_content + '</ul></div>';
	
    $.ajax({
        url: fetch_settings,
        type: 'get',
        crossDomain: true,
			data: {
					store:store_hash,
		store_hash:store_hash
            },
        success: function(data) {
            var result = JSON.parse(data);
            console.log(result);
            console.log("result-checking");
            display_string = result[0]['param_display_title'];
            console.log(display_string);
            view_settings = result[0]['param_product_display'];
            custom_fields_name = result[0]['param_bundling_custom_field_name'];
            console.log(custom_fields_name);
            console.log("custom_fields_name");
            param_is_bundle_product_display = result[0]['param_is_bundle_product_display'];
            console.log(param_is_bundle_product_display);
            console.log("param_is_bundle_product_display");
            param_is_embedded_display = result[0]['param_is_embedded_display'];
            console.log(param_is_embedded_display);
            console.log("param_is_embedded_display");
            if (view_settings == "embedded") {
                console.log("param_is_embedded_display");
                console.log(embed_content);
                $(".productView-product").append(embed_content);
            }

            console.log(view_settings);
            if (view_settings == "grid") {
                $('.bundling_products').removeClass("list");
                $('.bundling_products').addClass("grid");

            } else {
                $('.bundling_products').removeClass("grid");
                $('.bundling_products').addClass("list");
            }
            console.log("window_width");
            console.log($(window).width());
            if ($(window).width() < 600) {
                console.log($(window).width());
                $('.bundling_products').removeClass("list");
                $('.bundling_products').addClass("grid");
            }
            console.log(result);
            if (view_settings != "embedded") {
                console.log("display_string");
                console.log(display_string);
                $.ajax({
                    url: fetch_bundle_kit_details,
                    type: 'get',
                    data: {
                        destination_product_id: product_id,
							store:store_hash,
		store_hash:store_hash
                    },
                    crossDomain: true,
                    success: function(data) {
                        var custom_fields_result = JSON.parse(data);
                        console.log(custom_fields_result);
                        console.log("custom_fields_result");
                        if (custom_fields_result.length != 0) {
                            main_product_qty = custom_fields_result[0]['main_product_qty'];
                            console.log("main_product_qty");
                            console.log(main_product_qty);
                            collection_skus = custom_fields_result[0]['product_skus'];
                        }
                        var results;
                        var increments = 0;
                        var increments_sku = 0;
                        if (collection_skus != null) {
                            $('.bundling_products').append('<div class="bundling_heading"><p class="heading_addon">' + display_string + '</p><span></span></div>');
                            var sku_array;
                            if (collection_skus && collection_skus.toString().indexOf(",") != -1) {
                                sku_array = collection_skus.split(",");
                            } else {
                                sku_array = [];
                                sku_array.push(collection_skus);
                            }

                            $(sku_array).each(function(index, data) {
                                console.log("data1")
                                var sku = data.trim();
                                console.log("sku");
                                console.log(sku);
                                window.stencilUtils.api.search.search(sku, {
                                    limit: 1,
                                    template: 'products/options/search'
                                }, (err, responsetext) => {
                                    console.log("testing_start");
                                    console.log("response");
                                    console.log(responsetext);

                                    $(responsetext).find('li').each(function(index) {
                                        var current_sku = "";
                                        current_sku = $(this).attr('data-sku');
                                        console.log("current_sku");
                                        console.log(current_sku);
                                        console.log("common_sku");
                                        console.log(sku);
                                        if (sku == current_sku) {
                                            increments_sku = increments_sku + 1;
                                            console.log(increments_sku);
                                            //if($(this).attr('data-sku').indexOf(name)>-1){
                                            console.log("inside");
                                            var productId = '';
                                            console.log('product_id');
                                            console.log('product_id');
                                            productId = $(this).text();
                                            console.log(productId);
                                            window.stencilUtils.api.product.getById(productId, {
                                                template: 'products/bundling-products'
                                            }, (err, response) => {
                                                console.log(response);
                                                console.log(err);
                                                console.log("bundling_products");
                                                $('.bundling_products').append(response);
                                                increments = increments + 1;
                                                console.log("sku_arraylength");
                                                console.log(increments);
                                                console.log("sku_length");
                                                console.log(sku_array.length);
                                                /*if(sku_array.length == Number(increments)){
                                                	console.log("sku_array_length");
                                                	if(store_hash != "zb8do3a1o5"){
                                                	$(".bundling_products").append('<div class="form_action_kit"><input id="form-action-addToCart-kit" class="button button--primary" type="button" value="Add all to Cart"></div>');
                                                	}
                                                }*/
                                                if (view_settings == "list") {
                                                    $("article.bundling_products.list").css("display", "block");
                                                    if (type == "Kit") {

                                                        $(".form_action_kit").css("display", "block");
                                                    } else {
                                                        $(".form_action_kit").css("display", "block");
                                                        //  $("article.bundling_products.list").css("margin-bottom", "30px");
                                                        // $("article.bundling_products.list").css("border", "1px solid black");
                                                    }
                                                } else {
                                                    $(".form_action_kit").css("display", "none");
                                                }
                                                if (type == "Kit") {
													$(".common_section_" + productId).find(".bundle_select").remove();
                                                    $(".product-bundling-view").find("button").prop('disabled', true);
                                                    $(".common_section_" + productId).find(".piece-quantity-status-controls").html("<div class='kit_add_status'><img src='https://cdn11.bigcommerce.com/s-1c4plmkfrp/product_images/uploaded_images/tick.png'/><span>ADD TO BUNDLE</span></div>");


                                                }
                                                $.each(product_bundle_kit_details, function(index, data) {
                                                    if (type == "Kit") {
                                                        if (productId == data.product_id) {
                                                            if (data.qty != null) {
                                                                $(".common_section_" + productId).find(".form-input-bundle").val(data.qty);
                                                            } else {
                                                                $(".common_section_" + productId).find(".form-input-bundle").val("1");
                                                            }
                                                            $(".common_section_" + productId).find(".form-input-bundle").prop("disabled", true);


                                                        }
                                                    }else{
														console.log("variant_hide1");
														if (productId == data.product_id) {
															console.log("variant_hide2");
															console.log(data.variant_id);
															if(data.variant_id == "" || data.variant_id !=  undefined || data.variant_id !=  null){
																console.log("variant_hide3");
																$(".common_section_" + productId).find(".bundle_select").remove();
															}
														}
													}
                                                    if (productId == data.product_id) {
                                                        if (data.adjusted_price) {

                                                            $(".common_section_" + productId).find("span.price.price--withoutTax").text("$" + data.adjusted_price);

                                                        }
                                                        console.log($(".common_section_" + productId).find(".price.price--withoutTax"));
                                                        var current_price = $(".common_section_" + productId).find(".price.price--withoutTax").text();
                                                        var quantity = $(".common_section_" + productId).find(".form-input-bundle").val();
                                                        current_price = current_price.split('$')[1].trim();
                                                        console.log(current_price);
                                                        console.log("current_price");
                                                        bundle_kit_price = Number(bundle_kit_price) + (Number(current_price.replace(/[^0-9.-]+/g, "")) * Number(quantity));
                                                        console.log("bundle_kit_price");
                                                        console.log(bundle_kit_price);

                                                        //$('.productView-details span.price.price--withoutTax').text("$"+formatMoney(final_price,2));
                                                    }

                                                });

                                                equal_height_set();

                                            });


                                        } else {
                                            console.log('tst');
                                        }
                                    });

                                    console.log("testing_end");
                                });

                            });

                            if (type == "Kit") {
                                $(".gage_diamonds").find(".heading_addon").text("Shop this Collection");


                                if (main_product_qty != null) {
                                    console.log("inside_main_product_qty");
                                    console.log(main_product_qty);
                                    for (var i = 1; i <= main_product_qty - 1; i++) {
                                        console.log("inner");
                                        console.log(i);
                                        // $('[data-action="inc"]').trigger("click");

                                    }
                                    $('[name=qty\\[\\]]').val(main_product_qty);
                                    //$(".form-input--incrementTotal").prop("disabled",true);
                                    $(".form-increment").find("button").prop("disabled", true);
                                } else {
                                    //$("#form-input--incrementTotal").val("0");
                                    //$("#form-input--incrementTotal").prop("disabled",true);
                                }
                            }

                        }



                    }
                });


            } else {
                $(".bundling_section").css("display", "none");
            }
        }

    });

}


    //Bundle Kit embed product display


    //button.button.button--icon

$(document).on("click", "#form-action-addToCart-kit", function(event) {
        console.log("test_clicks$form");
        console.log(type);
        if (type == "Kit") {
            var text = "bundle";
            var validate = validation(null);
            if (validate == false) {
                var bundle_option_mandatory_alert = "<p >Please Select the options in products<br /></p>";
                return Swal.fire({
                    customClass: 'bundle_product_option_details',
                    html: '<strong>' + bundle_option_mandatory_alert + '</strong>',
                    showConfirmButton: false,
                    showCloseButton: true
                });

            }
        }
        if (type == "Bundle") {
            console.log("bundle_all_to_cart1");
            var validate = validation(null);
            console.log(validate);
            if (validate == false) {
                var bundle_option_mandatory_alert = "<p >Please Select the options in products<br /></p>";
                return Swal.fire({
                    customClass: 'bundle_product_option_details',
                    html: '<strong>' + bundle_option_mandatory_alert + '</strong>',
                    showConfirmButton: false,
                    showCloseButton: true
                });

            }
            //var productAddOn_bundle =  localStorage.getItem("CartProducts_bundle");
            //localStorage.setItem("CartProducts", productAddOn_bundle);
			 var productAddOn_bundle =  CartProducts_bundle;
				 CartProducts = productAddOn_bundle;
        }
        $form.submit();
        //$("#form-action-addToCart").trigger("click");
});

    $("td.cart-item-block.cart-item-info").find("a").removeClass("cart-remove");
    $("td.cart-item-block.cart-item-info").find("a").addClass("cart-remove-custom");
    $(document).on("click", ".cart-remove-custom", function(event) {
        console.log("test_clicks");
        console.log(store_hash);
        console.log(event);
        const itemId = $(event.currentTarget).data('cartItemid');
        console.log(itemId);
        const string = $(event.currentTarget).data('confirmDelete');
        console.log(string);
        var product_id = $(event.currentTarget).attr('product_id');
        var product_name = $(this).parents().closest('tr').find(".cart-item-name").text();
        console.log("product_name");
        console.log(product_name);
        console.log("current_product_id");
        console.log(product_id);
        var product_mandatory = "";
        var product_detail_cart = "";
        var bundle_alert = "";
        var param_message = "";
        var param_message_main = "";
        var cart_bundle_type = "";
        var main_product_name = "";
		var bliss_custom_code = false;
		if($(this).hasClass("theme_code")){
			bliss_custom_code = true;
		}
		
        $.ajax({
            url: cart_validation,
            type: "get",
            cache: false,
            async: false,
            data: {
                cart_line_item_id: itemId,
					store:store_hash,
		store_hash:store_hash
            },
            success: function(result) {
                console.log("cart_results");
                console.log(result);

                var result = JSON.parse(result);
                console.log(result);
                console.log(result.length);
                if (result.length != 0) {
                    product_mandatory = result[0]['param_is_product_mandatory'];
                    cart_bundle_type = result[0]['param_bundle_kit_type'];
                    product_detail_cart = result[0]['param_cart_lineitem_ids'];
					var product_detail_cart_db_delete = result[0]['param_cart_lineitem_ids'];
                    if (product_detail_cart && product_detail_cart.toString().indexOf(",") != -1) {
                        product_detail_cart = product_detail_cart.split(',');
                        console.log(product_detail_cart);
                        console.log("product_detail_cart");
                        product_detail_cart = JSON.stringify(product_detail_cart);


                    }
                    param_message = result[0]['param_is_main_product'];
                    param_message_main = result[0]['param_is_product_mandatory'];
                    main_product_name = result[0]['param_product_name'];
                    console.log("product_mandatory");
                    console.log(product_mandatory);

                }
               // if (param_message == "true" && param_message_main == 'true') {
				    if (param_message == "true") {
                    bundle_alert = "This " + product_name + " (" + cart_bundle_type + ")" + " has mandatory products, if you delete this product your all " + cart_bundle_type + " products will be lost.";
					if(bliss_custom_code == true){
														var xhr = new XMLHttpRequest();
								xhr.onreadystatechange = function() {
									if (xhr.readyState !== 4) return; {
										console.log(JSON.parse(xhr.responseText));
										var cart_data = JSON.parse(xhr.responseText);
										if (cart_data.length) {
											var cart_id = cart_data[0].id;
											
											$.ajax({
												url: cart_line_item_delete,
												type: "post",
												cache: false,
												async: false,
												data: {
													cart_id: cart_id,
													product_detail_cart: product_detail_cart,
														product_detail_cart_db_delete:product_detail_cart_db_delete,
														store:store_hash,
		store_hash:store_hash
												},
												success: function(result) {
													console.log("cart_delete_result");
													console.log(result);
													refreshContent(true, $overlay_cart)
												}
											});
										}
									}
								};
								xhr.open('GET', '/api/storefront/carts?include=lineItems.digitalItems.options,lineItems.physicalItems.options');
								xhr.send();
					}else{
						Swal.fire({
							text: bundle_alert,
							icon: 'warning',
							showCancelButton: true,
							customClass: {confirmButton: 'button buttonfx bouncein angleinleft',cancelButton: 'button buttonfx bouncein angleinleft'},
						}).then((result) => {
							if (result.value) {
								var xhr = new XMLHttpRequest();
								xhr.onreadystatechange = function() {
									if (xhr.readyState !== 4) return; {
										console.log(JSON.parse(xhr.responseText));
										var cart_data = JSON.parse(xhr.responseText);
										if (cart_data.length) {
											var cart_id = cart_data[0].id;
											$.ajax({
												url: cart_line_item_delete,
												type: "post",
												cache: false,
												async: false,
												data: {
													cart_id: cart_id,
													product_detail_cart: product_detail_cart,
													store:store_hash,
		store_hash:store_hash
												},
												success: function(result) {
													console.log("cart_delete_result");
													console.log(result);
													refreshContent(true, $overlay_cart)
												}
											});
										}
									}
								};
								xhr.open('GET', '/api/storefront/carts?include=lineItems.digitalItems.options,lineItems.physicalItems.options');
								xhr.send();

							}
						});
					}

                } else if (param_message == "false" && param_message_main == "true") {
                    bundle_alert = "This is " + product_name + " (" + cart_bundle_type + ")" + "  product is Bundle/kit product, if you want to delete the product, please delete the main " + cart_bundle_type + " [ " + main_product_name + " ]";
                    Swal.fire({
                        text: bundle_alert,
                        icon: 'warning',
						customClass: {confirmButton: 'button buttonfx bouncein angleinleft',cancelButton: 'button buttonfx bouncein angleinleft'},
                    }).then((result) => {
                        if (result.value) {
                            // remove item from cart
                            //cartRemoveItem(itemId);
                        }
                    });
                } else {
                    console.log("false_false");
                    Swal.fire({
                        text: string,
                        icon: 'warning',
                        showCancelButton: true,
						customClass: {confirmButton: 'button buttonfx bouncein angleinleft',cancelButton: 'button buttonfx bouncein angleinleft'},
                    }).then((result) => {
                        if (result.value) {
                            cartRemoveItem(itemId, $overlay_cart);
                           var cart_btn_disabled = setInterval(function() {cart_off(); }, 1000);
    setTimeout(function() {clearInterval(cart_btn_disabled); }, 10000);
							 
                        }
                    });
                }
            }
        });


        event.preventDefault();
    });
    $(document).on("click", ".bundle-primary-remove", function() {
        var newQty = 0;
        $(this).parents().closest(".product-bundling-view").find(".form-input--incrementTotal-bundle").val(newQty);
        $(this).css("display", "none");
        $(this).siblings().css("display", "inline-block");
        var base_price = $('.productView-details .base_price_without_tax').val();
        //$('.productView-details span.price.price--withoutTax').text(base_price);

        ProductDeleteCart($(this), product_details, type, product_details_bundle_kit);
    });
    $(document).on("click", ".bundle-primary-add", function(e) {
        var validate = validation($(this));
        console.log(validate);
        console.log("validate");
        if (validate == false) {
            console.log("insie");

            //$(this).parents().closest(".product-bundling-view").find("form").find("select.form-select.form-select--small").
            return false;
        } else {
            //$(this).parents().closest(".product-bundling-view").find("form").find("select.form-select.form-select--small").css("border-color","#ccc");
            console.log('click_add');
            var newQty = +($(this).parents().closest(".product-bundling-view").find(".form-input--incrementTotal-bundle").val()) + 1;
            $(this).parents().closest(".product-bundling-view").find(".form-input--incrementTotal-bundle").val(newQty);
            $(this).css("display", "none");
            $(this).siblings().css("display", "inline-block");
            var current_price = $('.productView-details span.price.price--withoutTax').text();
            console.log(current_price);

            var current_price = $('.productView-details span.price.price--withoutTax').text();

            current_price = current_price.split('$')[1].trim();
            console.log(current_price);

            var update_price = $(this).parents().find('.product-bundling-view').find('span.price.price--withoutTax').text();
            console.log(update_price);
            update_price = update_price.split('$')[1].trim();

            var final_price = Number(update_price.replace(/[^0-9.-]+/g, "")) + Number(current_price.replace(/[^0-9.-]+/g, ""));
            console.log('final_price');
            console.log(final_price);
            //$('.productView-details span.price.price--withoutTax').text('$ ' + formatMoney(final_price, 2));
            console.log("product_details_add1232");
            console.log(product_details);
            ProductCartCall($(this), product_details, type, product_details_bundle_kit);

        }

    });
    $(document).on("click", "#add-bundle", function() {
        var validate = validation($(this));
        if (validate == false) {
            console.log("insie");

            //$(this).parents().closest(".product-bundling-view").find("form").find("select.form-select.form-select--small").css("border-color","red");
            return false;
        } else {
            //$(this).parents().closest(".product-bundling-view").find("form").find("select.form-select.form-select--small").css("border-color","#ccc");
            console.log('click_Add_bundle');
            console.log($(this).parents().closest('.common_div').find(".form-input--incrementTotal-bundle").val());
            var newQty = +($(this).parents().closest('.common_div').find(".form-input--incrementTotal-bundle").val()) + 1;
            console.log("newQty");
            console.log(newQty);
            $(this).parents().closest('.common_div').find(".form-input--incrementTotal-bundle").val(newQty);
            if (newQty >= 1) {
                $(this).parents().closest('.common_div').find('.bundle-primary-add').css('display', 'none');
                $(this).parents().closest('.common_div').find('.bundle-primary-remove').css('display', 'inline-block');
            }
            var current_price = $('.productView-details span.price.price--withoutTax').text();

            current_price = current_price.split('$')[1].trim();
            console.log(current_price);

            var update_price = $(this).parents().closest(".common_section").find('span.price.price--withoutTax').text();
            console.log(update_price);
            console.log("update_price");
            update_price = update_price.split('$')[1].trim();
            console.log(update_price);
            var final_price = Number(update_price.replace(/[^0-9.-]+/g, "")) + Number(current_price.replace(/[^0-9.-]+/g, ""));
            console.log('final_price');
            console.log(final_price);
            $('.productView-details span.price.price--withoutTax').text('$ ' + formatMoney(final_price, 2));
            console.log("product_details_add123");
            console.log(product_details);
            console.log(product_details);
            ProductCartCall($(this), product_details, type, product_details_bundle_kit);

        }
    });

    $(document).on("click", "#minus-bundle", function() {
        console.log('click_remove_bundle');

        var newQty = +($(this).parents().closest('.common_div').find(".form-input--incrementTotal-bundle").val()) - 1;
        if (newQty < 0) newQty = 0;
        $(this).parents().closest('.common_div').find(".form-input--incrementTotal-bundle").val(newQty);
        if (newQty >= 1) {
            $(this).parents().closest('.common_div').find('.bundle-primary-add').css('display', 'none');
            $(this).parents().closest('.common_div').find('.bundle-primary-remove').css('display', 'inline-block');
        } else {
            $(this).parents().closest('.common_div').find('.bundle-primary-add').css('display', 'inline-block');
            $(this).parents().closest('.common_div').find('.bundle-primary-remove').css('display', 'none');
        }
        var current_price = $('.productView-details span.price.price--withoutTax').text();
        console.log(current_price);
        var current_price = $('.productView-details span.price.price--withoutTax').text();

        current_price = current_price.split('$')[1].trim();
        console.log(current_price);

        var update_price = $(this).parents().closest(".common_section").find('span.price.price--withoutTax').text();
        console.log(update_price);
        update_price = update_price.split('$')[1].trim();
		
		 var final_price = Number(current_price.replace(/[^0-9.-]+/g, "")) - Number(update_price.replace(/[^0-9.-]+/g, "")); 
      //  var final_price = Number(update_price.replace(/[^0-9.-]+/g, "")) + Number(current_price.replace(/[^0-9.-]+/g, ""));
        console.log('final_price');
        console.log(final_price);
        $('.productView-details span.price.price--withoutTax').text('$ ' + formatMoney(final_price, 2));
        ProductCartCall($(this), product_details, null, product_details_bundle_kit);
    });

    $(document).on("change", ".bundle_select select", function(event) {
        console.log('click');
        var that = $(this);
        console.log($(this).closest('.productView'));
        const $changedOption = $(event.target);
        const $form = $changedOption.parents('form');
        const productId = $('[name="product_id"]', $form).val();

        // Do not trigger an ajax request if it's a file or if the browser doesn't support FormData
        if ($changedOption.attr('type') === 'file' || window.FormData === undefined) {
            return;
        }

        window.stencilUtils.api.productAttributes.optionChange(productId, $form.serialize(), 'products/bulk-discount-rates', (err, response) => {
            const productAttributesData = response.data || {};
            const productAttributesContent = response.content || {};
            //this.updateProductAttributes(productAttributesData);
            updateViewsku(productAttributesData, productAttributesContent, that, productId);
        });
    });
    $(document).on("click", ".modal-close", function(event) {
        console.log('click');
        $(".modal-background").css("display", "none");
        $("#previewModal").css("display", "none");
        $(".modal--large").css("display", "none");
    })

    function updateViewsku(data, content = null, that, productId) {
        updatePriceView(that, data.price);
        if (data.sku) {
            console.log('sku_inside');
			 console.log(product_details);
                console.log("product_details");
            that.closest('.productView').find('.sku-value').text(data.sku);
            that.closest('.productView').find('.variant_id').text(data.v3_variant_id); //variant_id
            that.closest('.productView').find('.sku-label-text').show();
			var disabled = $("button.button.btn.mt-3.bundle-primary-add").is(":disabled");
            if (data.v3_variant_id != undefined && data.v3_variant_id != null && data.v3_variant_id != "") {
				console.log('sku_insidetest');
				var text ="";
				if(disabled ==  true){
					text = product_details[productId] + ',' + data.v3_variant_id;
				}else{
					text = that.closest('.productView').find('.form-input--incrementTotal-bundle').val() + ',' + data.v3_variant_id;
					
				}
                 
                product_details[productId] = text;
                console.log(product_details);
                console.log("product_details");
				CartProducts = JSON.stringify(product_details);
				console.log(CartProducts);
                console.log("CartProducts");
               // localStorage.setItem("CartProducts", JSON.stringify(product_details));
            }
        } else {
            that.closest('.productView').find('.sku-label-text').hide();
            that.closest('.productView').find('.sku-value').text("");
        }
    }


    function updatePriceView(that, price) {
        //this.clearPricingNotFound(viewModel);

        /* if (price.with_tax) {
			viewModel.priceLabel.$span.show();
			viewModel.$priceWithTax.html(price.with_tax.formatted);
		}*/

        if (price.without_tax) {
            that.closest('.productView').find('span.price-label').text();
            that.closest('.productView').find('span.price.price--withoutTax').text(price.without_tax.formatted);
        }

        /* if (price.rrp_with_tax) {
			viewModel.rrpWithTax.$div.show();
			viewModel.rrpWithTax.$span.html(price.rrp_with_tax.formatted);
		}

		if (price.rrp_without_tax) {
			viewModel.rrpWithoutTax.$div.show();
			viewModel.rrpWithoutTax.$span.html(price.rrp_without_tax.formatted);
		}

		if (price.saved) {
			viewModel.priceSaved.$div.show();
			viewModel.priceSaved.$span.html(price.saved.formatted);
		}

		if (price.non_sale_price_with_tax) {
			viewModel.priceLabel.$span.hide();
			viewModel.nonSaleWithTax.$div.show();
			viewModel.priceNowLabel.$span.show();
			viewModel.nonSaleWithTax.$span.html(price.non_sale_price_with_tax.formatted);
		}

		if (price.non_sale_price_without_tax) {
			viewModel.priceLabel.$span.hide();
			viewModel.nonSaleWithoutTax.$div.show();
			viewModel.priceNowLabel.$span.show();
			viewModel.nonSaleWithoutTax.$span.html(price.non_sale_price_without_tax.formatted);
		}*/
    }
    //});

    function ProductCartCall(that, product_details, value, product_details_bundle_kit) {
        console.log(product_details);
        console.log("product_details1234");
        if (that.parents().closest('.product-bundling-view').find('.form-input--incrementTotal-bundle').val() >= 1) {
            var sku = that.parents().closest('.product-bundling-view').find('.sku-value').text();
            var quantity = that.parents().closest('.product-bundling-view').find('.form-input--incrementTotal-bundle').val();
            var productId = that.parents().closest('.product-bundling-view').find('.product_id').val();
            console.log(productId);
            var variant_id = that.parents().closest('.product-bundling-view').find('.variant_id').text();
            var text = quantity + ',' + variant_id;
            product_details[productId] = text;
            product_details_bundle_kit[productId] = text;
            console.log(product_details);
            console.log("product_details123");
            console.log(value);
			CartProducts = JSON.stringify(product_details);
            //localStorage.setItem("CartProducts", JSON.stringify(product_details));
            if (value.toLowerCase() == "bundle") {
                console.log("bundle_Test");
				CartProducts_bundle = JSON.stringify(product_details);
              //  localStorage.setItem("CartProducts_bundle", JSON.stringify(product_details_bundle_kit));
            }
        } else {
            var productId = that.closest('.productView').find('.product_id').val();
            const prop = productId;
            product_details = Object.keys(product_details).reduce((object, key) => {
                console.log("test");
                console.log(key);
                console.log(prop);
                if (key !== prop) {
                    object[key] = product_details[key]
                }
                return object
            }, {});
            product_details_bundle_kit = Object.keys(product_details_bundle_kit).reduce((object, key) => {
                console.log("test");
                console.log(key);
                console.log(prop);
                if (key !== prop) {
                    object[key] = product_details_bundle_kit[key]
                }
                return object
            }, {});
            console.log("product_details");
            console.log(product_details);
			CartProducts = JSON.stringify(product_details);
            localStorage.setItem("CartProducts", JSON.stringify(product_details));
            if (value != null && value.toLowerCase() == "bundle") {
				CartProducts_bundle = JSON.stringify(product_details);
                //localStorage.setItem("CartProducts_bundle", JSON.stringify(product_details_bundle_kit));
            }
        }
    }

    function ProductDeleteCart(that, product_details, value, product_details_bundle_kit) {
        var productId = that.closest('.productView').find('.product_id').val();
        const prop = productId;
        product_details = Object.keys(product_details).reduce((object, key) => {
            console.log("test");
            console.log(key);
            console.log(prop);
            if (key !== prop) {
                object[key] = product_details[key]
            }
            return object
        }, {});
        if (value.toLowerCase() == "bundle") {
            product_details_bundle_kit = Object.keys(product_details_bundle_kit).reduce((object, key) => {
                console.log("test");
                console.log(key);
                console.log(prop);
                if (key !== prop) {
                    object[key] = product_details_bundle_kit[key]
                }
                return object
            }, {});
        }
        console.log("product_details");
        console.log(product_details);
		CartProducts = JSON.stringify(product_details);
        //localStorage.setItem("CartProducts", JSON.stringify(product_details));
        if (value != null && value.toLowerCase() == "bundle") {
				CartProducts_bundle = JSON.stringify(product_details);
            //localStorage.setItem("CartProducts_bundle", JSON.stringify(product_details_bundle_kit));
        }
    }

    function equal_height_set() {


        setTimeout(function() {

            $('.product-bundling-view .productView-details-bundle').equalHeightsDiv();
            $('.product-bundling-view .productView-options').equalHeightsDiv();
            $('.product-bundling-view .productView-images').equalHeightsDiv();
            $('.product-bundling-view .common_section').equalHeightsDiv();
        }, 2000);
    }
    $.fn.extend({
        equalHeightsDiv: function() {
            var top = 0;
            var row = [];
            var classname = ('equalHeights' + Math.random()).replace('.', '');
            $(this).each(function() {
                var thistop = $(this).offset().top;
                if (thistop > top) {
                    $('.' + classname).removeClass(classname);
                    top = thistop;
                }
                $(this).addClass(classname);
                $(this).height('auto');
                var h = (Math.max.apply(null, $('.' + classname).map(function() {
                    return $(this).outerHeight();
                }).get()));
                $('.' + classname).height(h);
            }).removeClass(classname);
        }
    });

    function formatMoney(number, decPlaces, decSep, thouSep) {
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSep = typeof decSep === "undefined" ? "." : decSep;
        thouSep = typeof thouSep === "undefined" ? "," : thouSep;
        var sign = number < 0 ? "-" : "";
        var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
        var j = (j = i.length) > 3 ? j % 3 : 0;

        return sign +
            (j ? i.substr(0, j) + thouSep : "") +
            i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
            (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
    }
    $(window).on("resize orientationchange", function() {
        equal_height_set();
        if ($(window).width() < 600) {
            $('.bundling_products').removeClass("list");
            $('.bundling_products').addClass("grid");
        }
    });



    function getCartContent(cartItemId, onComplete) {
        const options = {
            template: 'cart/preview',
            params: {
                suggest: cartItemId,
            },
            config: {
                cart: {
                    suggestions: {
                        limit: 4,
                    },
                },
            },
        };

        window.stencilUtils.api.cart.getContent(options, onComplete);
    }

    function updateCartContent(modal, cartItemId, onComplete) {

        getCartContent(cartItemId, (err, response) => {
            if (err) {
                return;
            }
            console.log("response_data");
            console.log(modal);
            console.log(modal.find(".modal-content"));
            modal.find(".modal-content").html("test");
            modal.find(".modal-content").html(response);

            //$( "#previewModal" ).insertBefore('<div class="modal-background" style=""></div>');
            if (store_hash != '9p889rxpkb') {
                $(".modal-background").css("display", "block");
                previewModal.css("display", "block");
                previewModal.css("visibility", "visible");
                previewModal.css("opacity", "1");

            }
            // Update cart counter
            const $body = $('body');
            $body.on('cart-quantity-update', (event, quantity) => {
                $('.cart-quantity')
                    .text(quantity)
                    .toggleClass('countPill--positive', quantity > 0);
                if (window.stencilUtils.tools.storage.localStorageAvailable()) {
                    localStorage.setItem('cart-quantity', quantity);
                }
            });
            const $cartQuantity = $('[data-cart-quantity]', modal.$content);
            const $cartCounter = $('.navUser-action .cart-count');
            const quantity = $cartQuantity.data('cartQuantity') || 0;

            console.log("cart-quantity");
            console.log(quantity);
            $(".cart-quantity").text(quantity);
            $cartCounter.addClass('cart-count--positive');
            $body.trigger('cart-quantity-update', quantity);

            if (onComplete) {
                onComplete(response);
            }
        });
    }


    function redirectTo(url) {
        if (isRunningInIframe() && !window.iframeSdk) {
            window.top.location = url;
        } else {
            window.location = url;
        }
    }

    function isRunningInIframe() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }

    function validation(_this) {
        var status = true;
        if (_this == null) {
            console.log("validation_null");

            $(".productView-options").find("form").find("select.form-select.form-select--small").each(function() {
                if ($(this).prop('required') && $(this).find("option:selected").val() == "") {
                    status = false;
                    return false;
                }

            });
            $(".bundle_select").find("select.form-select.form-select--small").each(function() {
                if ($(this).prop('required') && $(this).find("option:selected").val() == "") {
                    status = false;
                    $(this).css("border-color", "red");
                    return false;
                } else {
                    $(this).css("border-color", "#ccc");
                }

            });
        } else if (_this == "bundle") {

        } else {
            console.log("validation_inside");
            console.log(_this.parents().closest(".product-bundling-view").find("form").find("select.form-select.form-select--small"));
            if (_this.parents().closest(".product-bundling-view").find("form").find("select.form-select.form-select--small").length > 0) {
                _this.parents().closest(".product-bundling-view").find("form").find("select.form-select.form-select--small").each(function() {
                    if ($(this).prop('required') && $(this).find("option:selected").val() == "") {
                        status = false;
                        $(this).css("border-color", "red");
                        return false;
                    } else {
                        $(this).css("border-color", "#ccc");
                    }

                });
            }
        }
        return status;
    }

    function cartRemoveItem(itemId, $overlay_cart) {
        $overlay_cart.show();
        console.log("item_remove");
        window.stencilUtils.api.cart.itemRemove(itemId, (err, response) => {
            console.log(response);
            if (response.data.status === 'succeed') {
                refreshContent_test(true, $overlay_cart);
            } else {
                Swal.fire({
                    text: response.data.errors.join('\n'),
                    icon: 'error',
                });
            }
        });
    }

    function refreshContent_test(remove, $overlay_cart) {
        console.log(refreshContent);
        console.log("refreshContent");
        const $cartContents = $('[data-cart-content]');
        const $cartMessages_test = $('[data-cart-status]');
        const $cartTotals_test = $('[data-cart-totals]');
        const $cartItemsRows = $('[data-item-row]', $cartContents);
        const $cartPageTitle = $('[data-cart-page-title]');
        const options = {
            template: {
                content: 'cart/content',
                totals: 'cart/totals',
                pageTitle: 'cart/page-title',
                statusMessages: 'cart/status-messages',
            },
        };

        $overlay_cart.show();

        // Remove last item from cart? Reload
        if (remove && $cartItemsRows.length === 1) {
            return window.location.reload();
        }
        window.stencilUtils.api.cart.getContent(options, (err, response) => {
            console.log("get_content");
            console.log(response);
            if (response != undefined) {
                console.log("get_content_in");
                $cartContents.html(response.content);
                console.log(response.content);
                $cartTotals_test.html(response.totals);
                console.log(response.totals);
                $cartMessages_test.html(response.statusMessages);
                console.log(response.statusMessages);

                $cartPageTitle.replaceWith(response.pageTitle);
                console.log(response.pageTitle);
                bindEvents();
                $overlay_cart.hide();

                const quantity = $('[data-cart-quantity]', $cartContents).data('cartQuantity') || 0;
                console.log(quantity);
                $("span.countPill.cart-quantity").text(quantity);

                $('body').trigger('cart-quantity-update', quantity);
                //cart_off();
            } else {
                $overlay_cart.hide();
            }
        });
	var cart_btn_disabled = setInterval(function() {cart_off(); }, 1000);
    setTimeout(function() {clearInterval(cart_btn_disabled); }, 10000);
       
    }

    function refreshContent(remove, $overlay_cart) {
        console.log(refreshContent);
        console.log("refreshContent");
        const $cartContents = $('[data-cart-content]');
        const $cartMessages_test = $('[data-cart-status]');
        const $cartTotals_test = $('[data-cart-totals]');
        const $cartItemsRows = $('[data-item-row]', $cartContents);
        const $cartPageTitle = $('[data-cart-page-title]');
        const options = {
            template: {
                content: 'cart/content',
                totals: 'cart/totals',
                pageTitle: 'cart/page-title',
                statusMessages: 'cart/status-messages',
            },
        };

        $overlay_cart.show();

        // Remove last item from cart? Reload
        if (remove && $cartItemsRows.length === 1) {
            return window.location.reload();
        }
        return window.location.reload();
        /*window.stencilUtils.api.cart.getContent(options, (err, response) => {
        console.log("get_content");
        console.log(response);
		if(response != undefined){
			 console.log("get_content_in");
        $cartContents.html(response.content);
		console.log(response.content);
        $cartTotals_test.html(response.totals);
		console.log(response.totals);
        $cartMessages_test.html(response.statusMessages);
		console.log(response.statusMessages);

        $cartPageTitle.replaceWith(response.pageTitle);
		console.log(response.pageTitle);
        bindEvents();
        $overlay_cart.hide();

        const quantity = $('[data-cart-quantity]', $cartContents).data('cartQuantity') || 0;
		console.log(quantity);
		$("span.countPill.cart-quantity").text(quantity);

        $('body').trigger('cart-quantity-update', quantity);
		}else{
			console.log("else_false");
			 $overlay_cart.hide();
			return false;
			
		}
    });*/
    }

    function main_product_cart_addition(type, bundlekit_id) {

    }