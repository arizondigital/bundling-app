const path = require('path');

module.exports = [
{
  output: {
    filename: './main.js'
  },
  name: 'main',
  entry: './src/index.js',
	module: 
	{
		rules: [
		{
			test: /\.css$/i,
			use: ['style-loader', 'css-loader'],
		},
		{
			test: /\.m?js$/, exclude: /node_modules/,
			use: {
			 loader: 'babel-loader',
			 options: {
				 presets: ['@babel/preset-env']
			 }
			}
		}],
	},
	devtool : 'source-map'
}];