<?php 
include 'DBException.php';
class DBConnection {
	public function openDB() {
		try {
		   $pdo = new PDO("pgsql:dbname="._DATABASE.";host="._HOSTNAME,_USERNAME,_PASSWORD);
		}
		catch(PDOException $e)
		{
			echo "Connection failed: " . $e->getMessage();
		}
		return $pdo;
	}

	public function closeDB($pdo) {
		$pdo=null;
	}
	
	function sp_call($sp_name, $data=''){
		try {
			$conn=$this->openDB();
			$parameters ='';
			if($data){
				$keyvalues=array_keys($data);
				foreach($keyvalues as $keys=>$values)
				{
					$keyvalues[$keys]=":param_".$values;
				}
				$parameters=implode(",",$keyvalues);
				$checkingData=array_combine($keyvalues,$data);
			}
			/*echo "<pre>";
			print_r($checkingData);
			echo "</pre>";*/
			$stmt=$conn->prepare("SELECT * FROM $sp_name($parameters)");
			
			if($data){
			foreach($checkingData as $key=>&$values)
				{
					$stmt->bindParam($key,$values);
				}
			}

			$stmt->execute();
			$response=$stmt->fetchAll(PDO::FETCH_ASSOC);
			//		echo "\nPDO::errorInfo():\n";
	//		print_r($stmt->errorInfo());
			$this->closeDB($conn);
			return $response;
		}
		catch(PDOException $e){
			throw new DBException($e,$e->className,$e->functionName);
		}catch (Exception $e) {
			throw new DBException($e,get_class($this),__FUNCTION__);
		}
	}
	
	function sp_call_migration($sp_name, $data){
		try {
			$conn=$this->openDB();
			$keyvalues=array_keys($data);
			foreach($keyvalues as $keys=>$values)
			{
				$keyvalues[$keys]=":param_".$values;
			}
			$parameters=implode(",",$keyvalues);
			$checkingData=array_combine($keyvalues,$data);
			echo "check-data";
			echo "<pre>";
			print_r($checkingData);
			echo "<pre>";
			$stmt=$conn->prepare("SELECT * FROM $sp_name($parameters)");

			foreach($checkingData as $key=>&$values)
			{
				$stmt->bindParam($key,$values);
			}

			$stmt->execute();
			$response=$stmt->fetchAll(PDO::FETCH_ASSOC);
	//		echo "\nPDO::errorInfo():\n";
	//		print_r($stmt->errorInfo());

			$this->closeDB($conn);
			return $response;
		}
		catch(PDOException $e){
			throw new DBException($e,$e->className,$e->functionName);
		}catch (Exception $e) {
			throw new DBException($e,get_class($this),__FUNCTION__);
		}
	}


	function sp_count($sp_name, $data){
		try {
			$conn=$this->openDB();
			$keyvalues=array_keys($data);
			foreach($keyvalues as $keys=>$values)
			{
				$keyvalues[$keys]=":param_".$values;
			}
			$parameters=implode(",",$keyvalues);
			$checkingData=array_combine($keyvalues,$data);
			$stmt=$conn->prepare("SELECT count(*) FROM $sp_name($parameters)");

			foreach($checkingData as $key=>&$values)
			{
				$stmt->bindParam($key,$values);
			}

			$stmt->execute();
			$response=$stmt->fetchAll(PDO::FETCH_ASSOC);
			$this->closeDB($conn);
			return $response;
		}
		catch(PDOException $e){
			throw new DBException($e,$e->className,$e->functionName);
		}catch (Exception $e) {
			throw new DBException($e,get_class($this),__FUNCTION__);
		}
	}
}

?>