<?php
	include_once 'models/DBconnection.php';

	class CommonModel{
		
		function __construct()
		{
			$this->dbConnection = new DBconnection();
		}
		function getBundleKitList($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_bundle_kit_list",$insert_data);
			return $result;
		}
		function editBundleKitList($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_bundle_kit_details",$insert_data);
			return $result;
		}
		//uom update
		function getSalesUom($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_uom_product_data",$insert_data);
			return $result;
		}
		function ManageSalesUomProduct($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_manage_uom_product_data",$insert_data);
			return $result;
		}
		function getUomSettings($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_uom_setting",$insert_data);
			return $result;
		}
		function manageUomSettings($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_manage_uom_settings",$insert_data);
			return $result;
		}
		//uom update
		function getProductDetails($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_product_name_list",$insert_data);
			return $result;
		}
		function saveBundleKit($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_manage_bundle_kit_values",$insert_data);
			return $result;
		}
		function deleteBundleKitList($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_delete_bundle_kit",$insert_data);
			return $result;
		}
		function sp_get_delete_bundle_product_kit($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_delete_bundle_product_kit",$insert_data);
			return $result;
		}
		function getSettings($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_bundle_kit_setting_details",$insert_data);
			return $result;
		}
		function manageSettings($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_manage_bundle_kit_settings",$insert_data);
			return $result;
		}
		function productCartValidation($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_cart_bundle_kit_details",$insert_data);
			return $result;
		}
		function get_cart_validation($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_cart_product_details",$insert_data);
			return $result;
		}
		function manage_cart_details($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_manage_cart_details",$insert_data);
			return $result;
		}
		function get_cart_bundle_kit($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_cart_kit_qty_restriction",$insert_data);
			return $result;
		}
		function sp_get_cron_details(){
			$sp_name = 'public.sp_get_sync_log';
			$result = $this->dbConnection->sp_call($sp_name,'');
			return $result;
		}
		function getSkuDetails($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_get_product_sku_details",$insert_data);
			return $result;
		}
		function sp_delete_bundle_kit($insert_data){
			$result = $this->dbConnection->sp_call("bundling.sp_delete_bundle_kit",$insert_data);
			return $result;
		}
	}
?>