<?php
	include_once 'models/DBconnection.php';

	class ProductFilterModel{
		
		function __construct()
		{
			$this->dbConnection = new DBconnection();
		}

		function get_filter_data($insert_data){
			$result = $this->dbConnection->sp_call("filter.sp_get_filter_details_adminapp",$insert_data);
			return $result;
		}
		function manage_filter_data($insert_data){
			$result = $this->dbConnection->sp_call("filter.sp_update_filter_data_adminapp",$insert_data);
			return $result;
		}
		function get_filter_value_data($insert_data){
			$result = $this->dbConnection->sp_call("filter.sp_get_filter_value_details_adminapp",$insert_data);
			return $result;
		}
		function manage_filter_value_data($insert_data){
			$result = $this->dbConnection->sp_call("filter.sp_update_filter_value_data_adminapp",$insert_data);
			return $result;
		}
		function filter_visibility_update($insert_data){
			$result = $this->dbConnection->sp_call("filter.sp_update_filter_isvisible_adminapp",$insert_data);
			return $result;
		}
		function Get_filter_parent_name($insert_data){
			$result = $this->dbConnection->sp_call("filter.sp_get_parent_filter_details_adminapp",$insert_data);
			return $result;
		}
	}
?>