<?php 
class DBException extends Exception {
	public $className;
	public $functionName;
	public function __construct($obj,$class_name,$function_name) {
		$this->className=$class_name;
		$this->functionName=$function_name;
		parent::__construct($obj->getMessage(),$obj->getCode());
	}
	public function getClassName() {
		return $this->className;
	}
	public function getFunctionName() {
		return $this->functionName;
	}
	public function getDBExceptionInfo() {
		return "Class Name : ".$this->getClassName()." <br> FunctionName : ".$this->getFunctionName()." <br> Message : {$this->getMessage()}<br>";
	}
}
?>