<?php
	include_once '/var/www/html/Furniture/Bundling_App/models/DBconnection.php';

	class AdminModel{
		
		function __construct()
		{
			$this->dbConnection = new DBconnection();
		}

		function getStoredetails($data)
		{
			$result = $this->dbConnection->sp_call("public.sp_get_store_details",$data);
			return $result;
		}
		
		}
		
?>