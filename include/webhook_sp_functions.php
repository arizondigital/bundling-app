<?php
include_once '/var/www/html/Furniture/Bundling_App/models/DBconnection.php';

class WebhookModel{
	function __construct(){
		$this->dbConnection = new DBconnection();
	}
/* --- Webhook Log Manage and Get SP functions Start --- */

	function getCategoryLog(){
		$sp_name = 'product.sp_get_category_webhook_log';
		$result = $this->dbConnection->sp_call($sp_name,'');
		return $result;
	}

	function getProductLog(){
		$sp_name = 'product.sp_get_product_webhook_log';
		$result = $this->dbConnection->sp_call($sp_name,'');
		return $result;
	}

	function upsertCategoryWebhookLog($insert_data){
	    $sp_name = 'product.sp_manage_category_webhook_log';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}

	function upsertProductWebhookLog($insert_data){
		$sp_name = 'product.sp_manage_product_webhook_log';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function getProductLogbyLimit($data){
		$sp_name = 'product.sp_get_product_webhook_log_new';
		$result = $this->dbConnection->sp_call($sp_name,$data);
		return $result;
	}

	function upsertOrderWebhookLog($insert_data){
		$sp_name = 'orders.sp_manage_order_webhook_log';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function Brand_Existence_Check($insert_data){
		$sp_name = 'product.sp_check_brand';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
/* --- Webhook Log Manage and Get SP functions End --- */

/* --- Insert / Update the BC Category Details DB, SP function Start --- */
// Order insert / update SP Function	
	function upsertBCOrderDetailDB($insert_data){
		echo $sp_name = 'orders.sp_manage_order';
		echo "<pre>";
		print_r($insert_data);
		echo "</pre>";
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}

	function upsertBCCategoryDetailDB($insert_data){
		//$sp_name = 'product.sp_manage_category';
		$sp_name = 'product.sp_manage_category_migration';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
// Product insert / update SP Function
	function upsertBCProductDetailDB($insert_data){
		//$sp_name = 'product.sp_manage_product';
		$sp_name = 'product.sp_manage_product_migration';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function upsertBCProductCustomFieldDetailDB($insert_data){
		//$sp_name = 'product.sp_manage_product_custom_fields';
		$sp_name = 'product.sp_manage_product_custom_fields_migration';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function upsertBCProductImageDetailDB($insert_data){
		//$sp_name = 'product.sp_manage_product_image';
		$sp_name = 'product.sp_manage_product_image_migration';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function upsertBCProductOptionDetailDB($insert_data){
		//$sp_name = 'product.sp_manage_product_options';
		$sp_name = 'product.sp_manage_product_options_migration';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function upsertBCProductSKUDetailDB($insert_data){
		//$sp_name = 'product.sp_manage_product_option_sku';
		$sp_name = 'product.sp_manage_product_option_sku_migration';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function deleteBCProductDetailDB($insert_data){
		$sp_name = 'product.sp_delete_product_details';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	
	function upsertBrandWebhookLog($insert_data)
	{
	 	$sp_name = 'brand.sp_manage_brand';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function sp_manage_product_bundle_migration($insert_data){
		$sp_name = 'product.sp_manage_product_bundle_migration';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function sp_get_bundle_kit_list_bc_to_db($insert_data){
		$sp_name = 'bundling.sp_get_bundle_kit_list_bc_to_db';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function sp_manage_bundle_bc_log($insert_data){
		$sp_name = 'bundling.sp_manage_bundle_bc_log';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function sp_get_cron_details(){
		$sp_name = 'public.sp_get_sync_log';
		$result = $this->dbConnection->sp_call($sp_name,'');
		return $result;
	}
	function sp_manage_cron_details($insert_data){
		echo "test";
		$sp_name = 'public.sp_manage_sync_log';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
	function getSettings($insert_data){
		$sp_name = 'bundling.sp_get_bundle_kit_setting_details';
		$result = $this->dbConnection->sp_call($sp_name,$insert_data);
		return $result;
	}
/* --- Insert / Update the BC Category Details DB, SP function End --- */


/*--BundleKit sync details--*/



/*--BundleKit sync details--*/
/*get & update uom product details*/
function getUomProducts($insert_data){
	$sp_name = 'bundling.sp_get_uom_product_data';
	$result = $this->dbConnection->sp_call($sp_name,$insert_data);
	return $result;
}
function manageUomProducts($insert_data){
	$sp_name = 'bundling.sp_manage_uom_product_data';
	$result = $this->dbConnection->sp_call($sp_name,$insert_data);
	return $result;
}

/*get & update uom product details*/
	
}
	
?>