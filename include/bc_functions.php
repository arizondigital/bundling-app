<?php

function getCategoryDetailsBC($store_hash,$header,$bc_category_id){
	$api_url = 'v3/catalog/categories/'.$bc_category_id;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}

function getOrderDetailsBC($store_hash,$header,$bc_order_id){
	$api_url = 'v2/orders/'.$bc_order_id;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}
function getSingleBrand($store_hash,$header,$brand_id){
	$api_url = 'v3/catalog/brands/'.$brand_id;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}
function getOrderProductDetailsBC($store_hash,$header,$bc_order_id){
	$api_url = 'v2/orders/'.$bc_order_id.'/products';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}

function getOrderProductShippingDetailsBC($store_hash,$header,$bc_order_id){
	$api_url = 'v2/orders/'.$bc_order_id.'/shippingaddresses';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}

function getProductDetailsBC($store_hash,$header,$bc_product_id){
	$api_url = 'v3/catalog/products/'.$bc_product_id;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	
	return $bc_response;
}

function getBrandDetailsBC($store_hash,$header,$bc_brand_id){
	$api_url = 'v3/catalog/brands/'.$bc_brand_id;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}

function getProductOptionsetDetailsBC($store_hash,$header,$bc_optionset_id){
	$api_url = 'v2/option_sets/'.$bc_optionset_id;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}

function getProductCustomFieldDetailsBC($store_hash,$header,$bc_product_id){
	$api_url = 'v3/catalog/products/'.$bc_product_id.'/custom-fields';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}

function getProductOptionDetailsBC($store_hash,$header,$bc_product_id){
	$api_url = 'v3/catalog/products/'.$bc_product_id.'/options';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}
function getProductModifierDetailsBC($store_hash,$header,$bc_product_id){
	$api_url = 'v3/catalog/products/'.$bc_product_id.'/modifiers';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}

function getProductOptionValueDetailsBC($store_hash,$header,$bc_option_id){
	$api_url = 'v2/options/'.$bc_option_id.'/values';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}
function getProductSkuDetailsBC($store_hash,$header,$bc_product_id){
	$api_url = 'v3/catalog/products/'.$bc_product_id.'/variants';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}
function getProductImageDetailsBC($store_hash,$header,$bc_product_id){
	$api_url = 'v3/catalog/products/'.$bc_product_id.'/images';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}

function getProductCount($store_hash,$header,$date){
	$api_url = 'v2/products/count?min_date_modified='.$date;
	echo $api_url;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	//echo "bc_response<pre>";
	//print_r($bc_response);
	//echo "</pre>";
	return $bc_response->count;
}
function custom_getProductCount($store_hash,$header,$from_date,$to_date){
	if($to_date == null){
		$api_url = 'v2/products/count?min_date_modified='.$from_date;
	}else{
		$api_url = 'v2/products/count?min_date_modified='.$from_date.'&max_date_modified='.$to_date;
		//$api_url = 'v3/catalog/products/count?date_modified:min='.$from_date.'&date_modified:max='.$to_date;
	}
	echo $api_url;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	
	return $bc_response->count;
}
function custom_get_All_Product_Details_BC($store_hash,$header,$date,$to_date,$page_number){
	if($to_date == null){
		$api_url = 'v3/catalog/products?date_modified:min='.$date.'&limit=250&page='.$page_number;
	}else{
		$api_url = 'v3/catalog/products?date_modified:min='.$date.'&date_modified:max='.$to_date.'&limit=250&page='.$page_number;
	}
	//$api_url = 'v2/products?min_date_modified='.$date.'&limit=250&page='.$page_number.'&category=218';
	//$api_url = 'v2/products?min_date_modified='.$date.'&max_date_modified='.$max_date.'&limit=250&page='.$page_number;
	
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	
	return $bc_response;
}
function getProductCountAll($store_hash,$header,$category_id){
	$api_url = 'v2/products/count?category='.$category_id;
	//$api_url = 'v2/products/count';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response->count;
}

function get_All_Product_Details_BC($store_hash,$header,$date,$page_number){
	//$api_url = 'v2/products?min_date_modified='.$date.'&limit=250&page='.$page_number.'&category=218';
	//$api_url = 'v2/products?min_date_modified='.$date.'&max_date_modified='.$max_date.'&limit=250&page='.$page_number;
	$api_url = 'v3/catalog/products?date_modified:min='.$date.'&limit=250&page='.$page_number;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	
	return $bc_response;
}
function get_All_Product_Details_BC_total($store_hash,$header,$page_number,$category_id){
	$api_url = 'v2/products?limit=250&page='.$page_number.'&category='.$category_id;
	//$api_url = 'v2/products?limit=250&page='.$page_number;
	//$api_url = 'v3/catalog/products';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	/*echo "<pre>";
	print_r($bc_response);
	echo "</pre>";*/
	return $bc_response;
}

function getOrderCount($store_hash,$header,$date){
	$api_url = 'v2/orders/count?min_date_modified='.$date;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response->count;
}

function get_All_Order_Details_BC($store_hash,$header,$date,$page_number){
	$api_url = 'v2/orders?min_date_modified='.$date.'&limit=250&page='.$page_number;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}

function get_Shipping_Group_Details_BC($store_hash,$header,$product_id){
	$api_url = "v3/catalog/products/".$product_id."/metafields";
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	//echo "<pre>";
	//print_r($bc_response);
	//echo "</pre>";
	$shipping_group_data = $bc_response->data[0]->value;
	return $shipping_group_data;
}

function update_Product_Availability_BC($store_hash,$header,$product_id,$availablity_text,$update_product_condition){
	$api_url = 'v3/catalog/products/'.$product_id;
	$data=array();
	$data['availability_description']=$availablity_text;
	if($update_product_condition != ''){
		$data['condition']= $update_product_condition;
	}
	$method = 'PUT';
	$bc_response = BigCommerce_API($store_hash, $header, $api_url, $data, $method);
	return $bc_response;
}

function update_Order_status($store_hash,$header,$bc_order_id){
	$api_url = 'v2/orders/'.$bc_order_id;
	$data=array();
	$data['status_id'] = 8;
	$method = 'PUT';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $data, $method);
	return $bc_response;
}

function getCustomerCount($store_hash,$header,$date){
	$api_url = 'v3/customers?date_modified:min='.$date;
	//$api_url = 'v3/customers';
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response->meta->pagination->total;
}
function getCustomer($store_hash,$header,$date,$page_number){
	$api_url = 'v3/customers?date_modified:min='.$date.'&limit=250&page='.$page_number;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response->data;
}

function getLogCustomer($store_hash,$header,$id){
	$api_url = 'v3/customers?id:in='.$id;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response->data;
}

function getCustomerAddresslog($store_hash,$header,$id){
	$api_url = 'v3/customers/addresses?customer_id:in='.$id;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	//echo "test-arr";
	//print_r($bc_response);
	return $bc_response->data;
}
function getProductsData($store_hash,$header,$back_date){
	$api_url = "v3/catalog/products?date_modified:min=".$back_date;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	
	return $bc_response->data;
}
function getCategoryProducts($store_hash,$header,$category_id,$page_number){
	$api_url = "v3/catalog/products?categories:in=".$category_id."&limit=250&page=".$page_number;
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response->data;
}
function getCustomfields($store_hash,$header,$product_id){
	$api_url = "v3/catalog/products/".$product_id."/custom-fields";
	$fields = '';
	$method = 'GET';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response->data;
}
function updateCustomfields($store_hash,$header,$fields,$custom_field_id,$product_id){
	$api_url = "v3/catalog/products/".$product_id."/custom-fields/".$custom_field_id;
	$method = 'PUT';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}
function deletecustomfields($store_hash,$header,$custom_field_id,$product_id){
	$api_url = "v3/catalog/products/".$product_id."/custom-fields/".$custom_field_id;
	$method = 'DELETE';
	$fields = '';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response;
}
function createCustomfields($store_hash,$header,$fields,$product_id){
	$api_url = "v3/catalog/products/".$product_id."/custom-fields";
	$method = 'POST';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response->data;
}
function CreateProductBC($store_hash,$header,$fields){
	$api_url = "v3/catalog/products";
	$method = 'POST';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	echo "bc_responseCreateProductBC<pre>";print_r($bc_response);echo "</pre>";
	return $bc_response->data;
}
function UpdateProductBC($store_hash,$header,$fields,$product_id){
	$api_url = "v3/catalog/products/".$product_id;
	$method = 'PUT';
	$bc_response = BigCommerce_API ($store_hash, $header, $api_url, $fields, $method);
	return $bc_response->data;
}
function BigCommerce_API ($store_hash, $header, $api_url, $fields, $method) {
	$fields = json_encode($fields);
	$api_url = 'https://api.bigcommerce.com/stores/'.$store_hash.'/'.$api_url;
	echo $api_url;
	$ch = curl_init(); 
	curl_setopt( $ch, CURLOPT_URL, $api_url );
	curl_setopt($ch, CURLOPT_TIMEOUT, 1000); 
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );         
	if( isset($method) ) {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    } else {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    }
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 ); 
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields);
	curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );   
	$response = curl_exec($ch);   
	$result = json_decode($response); 
	return $result;	
}
?>