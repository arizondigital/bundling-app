<?php
ini_set('display_errors', "1");
error_reporting(E_ALL);
//header('Access-Control-Allow-Origin: http://localhost:4005');
///header("Access-Control-Allow-Origin: *");
//header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
//header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
	// Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
	// you want to allow, and if so:
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		// may also be using PUT, PATCH, HEAD etc
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

require_once __DIR__ . '/vendor/autoload.php';
require_once 'controllers/AdminController.php';
require_once 'controllers/DashboardController.php';
require_once 'controllers/CommonController.php';
require_once 'include/config.php';


use Bigcommerce\Api\Client as Bigcommerce;
use Firebase\JWT\JWT;
use Guzzle\Http\Client;
use Handlebars\Handlebars;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// Load from .env file
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$app = new Application();
$app['debug'] = true;

/*dashboard*/
$app->get('/index',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	
	$get_dashboard_details = new DashboardController();
	$result = $get_dashboard_details->getDashboardDetails($request,$storeId);
	$result['store'] = $storeHash;
	$app->render("/index",$result);
});
/*dashboard*/


$app->get('/list',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$type = $_REQUEST['type'];
	$storeId = getStoreID($storeHash);
	if($storeHash != ""){
	$get_bundle_kit_details = new CommonController();
	$result = $get_bundle_kit_details->getBundleKitList($request,$storeId,$type);
	$result['store'] = $storeHash;
	$result['type'] = $type;
	$app->render("/list",$result);
	}
});
$app->post('/list',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$type = $_REQUEST['type'];
	$storeId = getStoreID($storeHash);

	$get_bundle_kit_details = new CommonController();
	$result = $get_bundle_kit_details->getBundleKitList($request,$storeId,$type);
	$result['store'] = $storeHash;
	$result['type'] = $type;
	$app->render("/list",$result);
});
$app->get('/add',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$result['store'] = $storeHash;

	$app->render("/add",$result);
});
$app->get('/view',function(Request $request) use($app){
	/*$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$result['store'] = $storeHash;*/
	$result = [];
	$app->render("/view");
});
$app->get('/edit',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$bundle_kit_id = $_REQUEST['id'];
	$storeId = getStoreID($storeHash);
	$get_bundle_kit_details = new CommonController();
	$result = $get_bundle_kit_details->editBundleKitList($request,$storeId,$bundle_kit_id);
	$result['store'] = $storeHash;
	
	$app->render("/edit",$result);
});
$app->get('/import-products',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$import_product_details = new CommonController();
	$result = $import_product_details->importProducts($request, $storeId);
	return 	json_encode($result);
});
$app->post('/import-products',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$import_product_details = new CommonController();
	$result = $import_product_details->importProducts($request, $storeId);
	return 	json_encode($result);
});
$app->get('/export-products',function(Request $request) use($app)
{
		$storeHash = $_REQUEST['store'];
		$storeId = getStoreID($storeHash);
		$export_products = new CommonController();
		$result =$export_products->createExportProducts($request,$storeId);
		//return $result;
});


//UOM update
$app->get('/sales_uom',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$productId = null;
	$sku = null;
	$get_uom_products_details = new CommonController();
	$result = $get_uom_products_details->getSalesUom($request, $storeId, $productId, $sku);
	$result['store'] = $storeHash;
	$app->render("/sales_uom",$result);
});
$app->post('/sales_uom',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$productId = null;
	$sku = null;
	$get_uom_products_details = new CommonController();
	$result = $get_uom_products_details->getSalesUom($request, $storeId, $productId, $sku);
	$result['store'] = $storeHash;
	$app->render("/sales_uom",$result);
});
$app->post('/save-sales-uom',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$productId = $_REQUEST['product_id'];
	$sku = $_REQUEST['sku'];
	$base_uom = $_REQUEST['base_uom'];
	$sales_uom = $_REQUEST['sales_uom'];
	$is_uom_override = $_REQUEST['is_uom_override'];
	$bundle_override = $_REQUEST['bundle_override'];
	$bundle_available = $_REQUEST['bundle_available'];
	$bundle_qty = $_REQUEST['bundle_qty'];
	$broken_percentage = $_REQUEST['broken_percentage'];
	$measurement_value = $_REQUEST['measurement_value'];
	$get_uom_products_details = new CommonController();
	$result = $get_uom_products_details->ManageSalesUomProduct($request, $storeId, $productId, $sku, $base_uom, $sales_uom, $is_uom_override, $bundle_override, $bundle_available,$bundle_qty, $broken_percentage, 	$measurement_value );
	$result['store'] = $storeHash;
	return json_encode($result);
	//$app->render("/sales_uom",$result);
});

$app->get('/edit_sales_uom',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$product_id = $_REQUEST['id'];
	$sku = $_REQUEST['sku'];
	$storeId = getStoreID($storeHash);
	$get_uom_product_details = new CommonController();
	$result = $get_uom_product_details->getSalesUom($request,$storeId,$product_id, $sku);
	$result['store'] = $storeHash;
	
	$app->render("/edit_sales_uom",$result);
});

$app->get('/uom_settings',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$get_store_settings = new CommonController();
	$result = $get_store_settings->getUomSettings($request,$storeId);
	$result['store'] = $storeHash;
	
	$app->render("/uom_settings",$result);
});
$app->post('/uom_settings',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$get_store_settings = new CommonController();
	$result = $get_store_settings->getUomSettings($request,$storeId);
	$result['store'] = $storeHash;
	
	$app->render("/uom_settings",$result);
});
$app->post('/save_uom_settings',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$buy_in_base_sale_uom = $_REQUEST['buy_in_base_sale_uom'];
	$restrict_qty_change_cart = $_REQUEST['restrict_qty_change_cart'];
	$display_uom_conversion_details = $_REQUEST['display_uom_conversion_details'];
	$get_store_settings = new CommonController();
	$result = $get_store_settings->manageUomSettings($request,$storeId,$buy_in_base_sale_uom,$restrict_qty_change_cart,$display_uom_conversion_details);
	$result['store'] = $storeHash;
	return json_encode($result);
	//$app->render("/uom_settings",$result);
});
//UOM update
$app->get('/settings',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$get_store_settings = new CommonController();
	$result = $get_store_settings->getSettings($request,$storeId);
	$result['store'] = $storeHash;
	
	$app->render("/settings",$result);
});
$app->get('/get-settings',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$get_store_settings = new CommonController();
	$result = $get_store_settings->getSettings($request,$storeId);
	$result['store'] = $storeHash;
	
	return json_encode($result);
});
$app->post('/settings',function(Request $request) use($app){
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$get_store_settings = new CommonController();
	$result = $get_store_settings->manageSettings($request,$storeId);
	$result['store'] = $storeHash;
	
	return json_encode($result);
});
$app->post('/add-product',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$client_id = clientId();
	$storeId = getStoreID($storeHash);
	$get_product_details = new CommonController();
	$result = $get_product_details->getProductDetails($request,$storeId);
	return json_encode($result);
});

$app->post('/get-product-sku-details',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$client_id = clientId();
	$storeId = getStoreID($storeHash);
	$get_product_details = new CommonController();
	$result = $get_product_details->getSkuDetails($request,$storeId);
	return json_encode($result);
});

//delete-bundle
$app->post('/delete-bundle',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$client_id = clientId();
	$storeId = getStoreID($storeHash);
	$get_product_details = new CommonController();
	$result = $get_product_details->deleteBundle($request,$storeId);
	return json_encode($result);
});
$app->post('/save-bundle-kit',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$get_product_details = new CommonController();
	$result = $get_product_details->saveBundleKit($request,$storeId);
	return json_encode($result);
});
$app->post('/delete-bundle-kit',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$get_product_details = new CommonController();
	$result = $get_product_details->deleteBundleKit($request,$storeId);
	return json_encode($result);
});

$app->post('/delete-product-bundle-kit',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$get_product_details = new CommonController();
	$result = $get_product_details->deleteProductBundleKit($request,$storeId);
	return json_encode($result);
});
//product-cart-validation
$app->get('/product-cart-validation',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$product_cart_validation = new CommonController();
	$result = $product_cart_validation->productCartValidation($request,$storeId);
	return json_encode($result);
});
$app->get('/get-custom-fields-values',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$client_id = clientId();
	$access_token = getStoreAccessToken($storeHash);
	$get_custom_fields = new CommonController();
	$result = $get_custom_fields->get_custom_fields_values($request,$storeId,$storeHash,$client_id,$access_token);
	return json_encode($result);
});
$app->post('/add-products-cart',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$client_id = clientId();
	$access_token = getStoreAccessToken($storeHash);
	$add_products_cart = new CommonController();
	$result = $add_products_cart->addProductsCart($request,$storeId,$storeHash,$client_id,$access_token);
	return json_encode($result);
});
$app->get('/get-cart-validation',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$client_id = clientId();
	$access_token = getStoreAccessToken($storeHash);
	$add_products_cart = new CommonController();
	$result = $add_products_cart->get_cart_validation($request,$storeId);
	return json_encode($result);
});
$app->get('/get-cart-bundle-kit',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$add_products_cart = new CommonController();
	$result = $add_products_cart->get_cart_bundle_kit($request,$storeId);
	return json_encode($result);
});
$app->post('/cart-line-item-delete',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	//echo "test_inside";
	$client_id = clientId();
	$access_token = getStoreAccessToken($storeHash);
	$add_products_cart = new CommonController();
	$result = $add_products_cart->cart_line_item_delete($request,$storeId,$storeHash,$client_id,$access_token);
	return json_encode($result);
});
$app->post('/manage_cart_details',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$add_products_cart = new CommonController();
	$result = $add_products_cart->manage_cart_details($request,$storeId);
	return json_encode($result);
});
$app->get('/get-cron-details',function(Request $request) use($app)
{
	$storeHash = $_REQUEST['store'];
	$storeId = getStoreID($storeHash);
	$add_products_cart = new CommonController();
	$result = $add_products_cart->get_cron_details($request,$storeId);
	return json_encode($result);
});
$app->get('/load', function (Request $request) use ($app) {

	
	$data = verifySignedRequest($request->get('signed_payload'));
	/*echo "<pre>";
	print_r($data);
	echo "</pre>";die;*/
	$storeHash =$data['store_hash'];
	if (empty($data)) {
		echo "<script>location.href='templates/404.php'</script>";die;
	}else
	{
		if(count($_COOKIE)<=0||!isset($_COOKIE['Bundling_AppEmail'])||!isset($_COOKIE['Bundling_Apptoken'])){
			$timestamp=time() + (86400 * 30);
			$random_key=rand();
			$token=XOREncrypt($store_hash).'.'.XOREncrypt($random_key);
			$cookie_value = $data['user']['email'];
			/*setcookie("B2B_SuiteEmail", $cookie_value, $timestamp, "/", "", "", TRUE); // 86400 = 1 day
			setcookie("B2B_Suitetoken", $token, $timestamp, "/", "", "", TRUE); // 86400 = 1 day*/

			header('Set-Cookie: Bundling_AppEmail='.$cookie_value.'; Expires= '.$timestamp.'; SameSite=None; Secure', false);
			header('Set-Cookie: Bundling_Apptoken='.$token.'; Expires= '.$timestamp.'; SameSite=None; Secure', false);
		}
	}
	echo "<script>location.href='index?store=".$storeHash."'</script>";die;
});


$app->get('/auth/callback', function (Request $request) use ($app) {
	
	$payload = array(
		'client_id' => clientId(),
		'client_secret' => clientSecret(),
		'redirect_uri' => callbackUrl(),
		'grant_type' => 'authorization_code',
		'code' => $request->get('code'),
		'scope' => $request->get('scope'),
		'context' => $request->get('context'),
	);

	$client = new Client(bcAuthService());
	$req = $client->post('/oauth2/token', array(), $payload, array(
		'exceptions' => false,
	));
	$resp = $req->send();

	if ($resp->getStatusCode() == 200) {
		$data = $resp->json();

		//Getting Store Hash Data
		$storeHash = explode('/', $data['context'])[1];

		list($context, $storeHash) = explode('/', $data['context'], 2);
		$key = getUserKey($storeHash, $data['user']['email']);
		/*file writes*/
		$file=fopen(__DIR__.'/file.txt', "w+");
		fwrite($file,$data["access_token"]);
		fclose($file);
		$app_hash = md5($storeHash);
		$username =  $data['user']['username'];
		$useremail = $data['user']['email'];
		
		/*configureBCApi($storeHash);
		$store_info = Bigcommerce::getStore();
		$store_url = $store_info->secure_url;
		$store_name = $store_info->name;

		$admin_details = new AdminController();
		$result = $admin_details->insertStore($storeHash,$data["access_token"],$app_hash,$username,$useremail, $store_name, $store_url);
		//$storeHash = $data['store_hash'];
		$store_result = $admin_details->getStore($storeHash);*/
		
		//echo "<script>location.href='dashboard?store=".$storeHash."'</script>";
		header("Location: ../index?store=".$storeHash."");die;

	}

});


function getStoreID($storeHash){
	//echo $storeHash;
	$admin_details = new AdminController();
	$result = $admin_details->getStore($storeHash);
	//echo "result<pre>";print_R($result);echo "</pre>";
	$storeId = $result[0]['app_installed_store_id'];
	return $storeId;
}
function getStoreAccessToken($storeHash){
	
	$admin_details = new AdminController();
	$result = $admin_details->getStore($storeHash);
	$hash_token = $result[0]['hash_token'];
	return $hash_token;
}

/**
 * Configure the static BigCommerce API client with the authorized app's auth token, the client ID from the environment
 * and the store's hash as provided.
 * @param string $storeHash Store hash to point the BigCommece API to for outgoing requests.
 */
function configureBCApi($storeHash)
{
	Bigcommerce::configure(array(
		'client_id' => clientId(),
		'auth_token' => getAuthToken($storeHash),
		'store_hash' => $storeHash
	));
}

/**
 * @param string $storeHash store's hash that we want the access token for
 * @return string the oauth Access (aka Auth) Token to use in API requests.
 */
function getAuthToken($storeHash)
{
	$file=fopen(__DIR__.'/file.txt', "r");
	$content=fread($file,filesize(__DIR__.'/file.txt'));
	return $content;
}

/**
 * @param string $jwtToken	customer's JWT token sent from the storefront.
 * @return string customer's ID decoded and verified
 */
function getCustomerIdFromToken($jwtToken)
{
	$signedData = JWT::decode($jwtToken, clientSecret(), array('HS256', 'HS384', 'HS512', 'RS256'));
	return $signedData->customer->id;
}

/**
 * This is used by the `GET /load` endpoint to load the app in the BigCommerce control panel
 * @param string $signedRequest Pull signed data to verify it.
 * @return array|null null if bad request, array of data otherwise
 */
function verifySignedRequest($signedRequest)
{
	list($encodedData, $encodedSignature) = explode('.', $signedRequest, 2);
	// decode the data
	$signature = base64_decode($encodedSignature);
	$jsonStr = base64_decode($encodedData);
	$data = json_decode($jsonStr, true);
	// confirm the signature
	$expectedSignature = hash_hmac('sha256', $jsonStr, clientSecret(), $raw = false);
	if (!hash_equals($expectedSignature, $signature)) {
		error_log('Bad signed request from BigCommerce!');
		return null;
	}
	return $data;
}
/**
 * @return string Get the app's client ID from the environment vars
 */
function clientId()
{
	$clientId = getenv('BC_CLIENT_ID');
	return $clientId ?: '';
}

/**
 * @return string Get the app's client secret from the environment vars
 */
function clientSecret()
{
	$clientSecret = getenv('BC_CLIENT_SECRET');
	return $clientSecret ?: '';
}

/**
 * @return string Get the callback URL from the environment vars
 */
function callbackUrl()
{
	$callbackUrl = getenv('BC_CALLBACK_URL');
	return $callbackUrl ?: '';
}

/**
 * @return string Get auth service URL from the environment vars
 */
function bcAuthService()
{
	$bcAuthService = getenv('BC_AUTH_SERVICE');
	return $bcAuthService ?: '';
}

function getUserKey($storeHash, $email)
{
	return "kitty.php:$storeHash:$email";
}
$app->run();
//echo "test_test";die;
?>