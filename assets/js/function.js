$(document).ready(function(){
	var store_hash = $('.store_id').val();
  var view_settings =null;
	//Get store settings
	    $.ajax({
        url: "https://bundling.arizonreports.cloud/Furniture/Bundling_App/get-settings",
        type: 'get',
        crossDomain: true,
			data: {
					store:store_hash,
		store_hash:store_hash
            },
        success: function(data) {
            var result = JSON.parse(data);
            console.log(result);
			  view_settings = result[0]['param_product_display'];
			  console.log(view_settings);
			  console.log("view_settings");
					}
		});
	
	$.ajax({
		url:"https://bundling.arizonreports.cloud/Furniture/Bundling_App/get-cron-details?store="+store_hash,
						type:'get',
					success:function(data)
			{
				console.log("cron_status");
				
				var result=JSON.parse(data);
				console.log(result);
				
					console.log("result-checking");
					  $.each(result, function(index, value) {
						  console.log("cron_status_prod");
						  console.log(value.log_status);
						  if(value.log_status == "inprogress"){
							  $(".bundle_kit_page button.btn.sync-btn").prop("disabled",true);
							   $(".bundle_kit_page button.btn.sync-btn").text("Sync Processing");
							   $(".sync_container").append ("<div class='status_container'><span>Currently Sync processing...</span></div>");
						  }else{
							  $(".bundle_kit_page button.btn.sync-btn").prop("disabled",false);
							  $(".bundle_kit_page button.btn.sync-btn").text("Sync with BC Catalog");
							  $(".status_container").remove();
							 // $(".sync_container").append ("<div class='status_container'><span>Current sync process completed...</span></div>");
							 // setTimeout(function(){ $(".status_container").remove(); }, 3000);
						  }
					  });
			}
		});


	/*Equal height*/
	$.fn.extend({
	  equalHeightsDiv: function() {
		var top=0;
		var row=[]; 
		var classname=('equalHeights'+Math.random()).replace('.','');
		$(this).each(function() {
		  var thistop=$(this).offset().top;
		  if (thistop>top) {
			 $('.'+classname).removeClass(classname); 
			 top=thistop;
		  }
		  $(this).addClass(classname);
		  $(this).height('auto');
		  var h=(Math.max.apply(null, $('.'+classname).map(function(){ return $(this).outerHeight();}).get()));
		  $('.'+classname).height(h);
	   }).removeClass(classname); 
	 }
	}); 
	/*Equal height*/

	$('.table_section tr td a.sub_product_list').click(function(){
		$(this).siblings('.product_list').slideToggle();
	});
		/*page height*/
		var window_height=window.innerHeight;
		var header_height=$("header").outerHeight(true);
		var footer_height=$("footer").outerHeight(true);
		var height_section=header_height+footer_height;
		var main_height=window_height-height_section
		$("main").css("min-height",main_height);
		$(".main_container").css("min-height",main_height);
		/*page height*/

	$('.form_add_container a').click(function(){
			$('.form_add_container .btn + .expand_section').css('display','inline-block');
	});

// data sync from bc to database.//
	if($("#custom_field_label").val() != ""){
		console.log("test_test");
		$("#custom_field_label").prop("disabled",true);
	}
	//daterangepicker
	$(function() {
	  $('#custom_sync_date').daterangepicker({
		opens: 'left',
		autoUpdateInput: false,
		locale: {
		  format: 'DD/MM/YYYY'
		}
	  }, function(start, end, label) {
		console.log("A new date selection was made: " + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY'));
		$('#custom_sync_date').val(start.format('DD/MM/YYYY')+" - "+end.format('DD/MM/YYYY'));
		$('.order_from_date').val(start.format('MMMM D, YYYY'));
		$('.order_to_date').val(end.format('MMMM D, YYYY'));
	  });
	});
	$(document).on('change', '#sync_type' ,function(){
		console.log("sync_date");
		console.log($('#sync_type option').filter(':selected').val());
		if( $('#sync_type option').filter(':selected').val() =='custom'){
			$(".custom_sync_date_div").css("display","inline-block");
		}else{
			$(".custom_sync_date_div").css("display","none");
			$(".order_from_date").val("");
			$(".order_to_date").val("");
		}
	});
	//button.btn.sync-btn
	$(document).on('click', 'button.btn.sync-btn' ,function(){
		console.log("sync_button_click");
		var to_date = null;
		var from_date = null;
		var page_type = null;
		var product_id = null;
		var waitMessage ="Sync Processing";
		var OriginalMessage="Sync with BC Catalog";
		var store_hash = $('.store_id').val();
		var type_of_sync = $('#sync_type option').filter(':selected').val();
		if($("#page_value").val() == "bundle_kit_page"){
			page_type = "list page";
			product_id = null;
			 $(".bundle_kit_page button.btn.sync-btn").text(waitMessage);
			 $(".bundle_kit_page button.btn.sync-btn").prop('disabled', true);
			 $(".sync_container").append("<div class='status_container'><span>Currently Sync processing....</span></div>");
			// location.reload();
		}
		if($("#page_value").val() == "edit_bundle_kit_page"){
			page_type = "edit page";
			product_id = $("#main_bc_product_id").val();
			 $(".edit_bundle_kit button.btn.sync-btn").text(waitMessage);
			 $(".edit_bundle_kit button.btn.sync-btn").prop('disabled', true);
			 location.reload();

        
		}
		if($("#page_value").val() == "sales_uom_page"){
			page_type = "list page";
			product_id = null;
			 $(".sales_uom button.btn.sync-btn").text(waitMessage);
			 $(".sales_uom button.btn.sync-btn").prop('disabled', true);
			 $(".sales_uom .action-bar").append("<div class='status_container'><span>Currently Sync processing....</span></div>");
			// location.reload();
		}


		if($(".order_from_date").val() != ""){
		  from_date = $(".order_from_date").val();
		}
		if($(".order_to_date").val() != ""){
		  to_date = $(".order_to_date").val();
		}
		
				$.ajax({
					url:"https://bundling.arizonreports.cloud/Furniture/Bundling_App/webhook/custom_product_sync.php",
						type:'get',
						data:{store_hash:store_hash,type:type_of_sync,from_date:from_date,to_date:to_date,page_type:page_type,product_id:product_id},
					success:function(data)
			{
				console.log(data);
				$(".edit_bundle_kit button.btn.sync-btn").text(OriginalMessage);
			 $(".edit_bundle_kit button.btn.sync-btn").prop('disabled', false);
			 	$.ajax({
		url:"https://bundling.arizonreports.cloud/Furniture/Bundling_App/get-cron-details?store="+store_hash,
						type:'get',
					success:function(data)
			{
				console.log("cron_status");
				
				var result=JSON.parse(data);
				console.log(result);
				
					console.log("result-checking");
					if(result.length > 0){
					  $.each(result, function(index, value) {
						  console.log("cron_status_prod");
						  console.log(value.log_status);
						  if(value.log_status == "inprogress"){
							  $(".bundle_kit_page button.btn.sync-btn").prop("disabled",true);
							   $(".bundle_kit_page button.btn.sync-btn").text("Sync Processing");
							   $(".sales_uom button.btn.sync-btn").prop("disabled",true);
							   $(".sales_uom button.btn.sync-btn").text("Sync Processing");
						  }else{
							  $(".bundle_kit_page button.btn.sync-btn").prop("disabled",false);
							  $(".bundle_kit_page button.btn.sync-btn").text("Sync with BC Catalog");
							  $(".sales_uom button.btn.sync-btn").prop("disabled",false);
							  $(".sales_uom button.btn.sync-btn").text("Sync with BC");
						  }
					  });
					}else{
						if($("#page_value").val() == "sales_uom_page"){
							$(".sales_uom button.btn.sync-btn").text("Sync with BC");
							$(".sales_uom button.btn.sync-btn").prop('disabled', false);
							$(".status_container").remove(); 
							$(".action-bar").append("<div class='status_container'><span>Current sync process completed successfully...</span></div>");
							 
						}else{
						 $(".bundle_kit_page button.btn.sync-btn").text(OriginalMessage);
							$(".bundle_kit_page button.btn.sync-btn").prop('disabled', false);
							$(".status_container").remove();
							 $(".sync_container").append("<div class='status_container'><span>Current sync process completed successfully...</span></div>");
						}
						setTimeout(function(){ $(".status_container").remove(); }, 3000);
					}
			}
		});
			}
		});
	});
// data sync from bc to database.//
	$(document).on('change', '#product_type' ,function(){
		console.log("data-select");
		if( $('#product_type option').filter(':selected').val() !=''){
			$('.bundle_kit_form').submit();
			var current_type = $(".current_type").val();
		}

		var product_type = $('#product_type').val();
		if(product_type == "Kit"){
			console.log(product_type);
			$('.td-qty, .th-qty').css('display','table-cell');
			$(".kit_mandatory").prop("checked",true);
			$(".kit_mandatory").prop("disabled",true);
		}else{
			$(".kit_mandatory").prop("checked",false);
			$(".kit_mandatory").prop("disabled",false);
			console.log(product_type);
			$('.td-qty, .th-qty').css('display','none');
		}
	});

	$(document).on('click', '.btn_search_bundle_kit' ,function(event){
		event.preventDefault();
			console.log("data-select-click");
		//|| $('#product_type option').filter(':selected').val() !=''
		if($('#Bundle_Kit_Name').val() !=''){
		
			 if($('#product_type option').filter(':selected').val() ==''){
				 var current_type = urlParam('type');
				 console.log(current_type);
				 $("#product_type").val(current_type);
			 }
		$('.bundle_kit_form').submit();
		}else{
			alert('Please enter Bundle/Kit Name');
		}
	});
	//btn_reset
	$(document).on('click', '#btn_reset' ,function(){
		console.log("data-select-click");
		$('#Bundle_Kit_Name').val('');
		
		 $("#product_type").val("both");
		 $('.bundle_kit_form').submit();
		//location.reload();
	});
	//Edit bundle kit
	var added_products = [];
	
	
	$(document).on('click','.expand_section .btn,.form_field_container.sku_details .btn',function(){
		if($("#Bundle_Kit_Name").val() !="" && $('#product_type option').filter(':selected').val() !='' && $('#product_name').val() != '' ){
			//&& ($("#sku_type").length > 0 && $('#sku_type option').filter(':selected').val() !='')
			if($("#page_type").val() == "edit_bundle_kit"){
		}else{
			if(isNaN($('#product_name').attr("product_id")) && $('#product_name').val() !="" ){
				return false;
			}
		}
		console.log('product_search');
		$(".add_bundle_table").css("display","block");
		$(".add_bundle_kit_page .button_block").css("display","block");
		var store_hash = $('.store_id').val();

		// selected product details
		var Bundle_Kit_Name = $("#Bundle_Kit_Name").val();
		var Bundle_Kit_SKU = $("#Bundle_Kit_SKU").val();
		var product_name = $('#product_name').attr('product_name');
		var id = $('#product_name').attr('product_id');
		console.log(id);
		console.log("product_id_in");
		$('.product_name').each(function() {
			var ids = $(this).attr('product_id');
			console.log("inside_push");
			console.log(ids);
			added_products.push(ids);
			
	});
	console.log(added_products.includes(id));
	if(added_products.includes(id)){
		alert("This product is already added to the bundle");
		$("#product_name").val("");
		$('.sku_details').remove();
		$("a.btn.product_add_btn").css("display","inline-block");
		return false;
	}
		console.log(added_products);
		console.log("added_products");
		
		var variant_sku_main = "";
		variant_sku_main = $('.sku_details #sku_type option').filter(':selected').val();
		//$('.sku_details #sku_type').val();
		var product_type = $('#product_type').val();
		 console.log("variant_sku_main" + variant_sku_main);
		var variant_sku = "";
		var variant_sku_id = "";
		if( product_type == "Kit" || view_settings == "embedded" && $("#sku_type").length > 0 && ( variant_sku_main == undefined || variant_sku_main == 'select')){
			console.log("empty");
			$("#sku_type").css("border","1px solid red");
			return false;
		}else if($("#sku_type").length > 0 && variant_sku_main != "" && variant_sku_main != null && variant_sku_main != "select"){
			variant_sku = variant_sku_main.split('***')[0].trim();
			variant_sku_id = variant_sku_main.split('***')[1].trim();
		}else{
			variant_sku = $('#product_name').attr('product_sku');
			if(variant_sku == undefined || variant_sku == null){
				variant_sku = '--';
			}
		}
		console.log("variant_sku" + variant_sku);
		console.log("variant_sku_id" + variant_sku_id);
		
		var price = "";
		if($("#sku_type").length > 0 && $('.sku_details #sku_type option').filter(':selected').val() !='' && $('.sku_details #sku_type option').filter(':selected').val() !='select'){
			price = $('.sku_details #sku_type option').filter(':selected').attr('sku-price');
		}else{
			price = $('#product_name').attr('price_data');
		}
		var qty = $('#product_name').attr('product_qty');
		qty = (qty) ? qty : '--';
		var data_source = $('#product_name').attr('product_source');
		data_source = (data_source) ? data_source  : '--';

		var new_row_sno = $('.table_section table tbody tr').length + 1;
		var new_row_sno_data = new_row_sno + 1;
		
		var qty_cell = '';
		qty_cell = "<td class='td-qty'><form action><input type='number' name='product_qty' class='product_qty' value='' autocomplete='off'></form></td>";
		var empty_cell = '';
		
			empty_cell = "<td class='td-qty'></td>";
		
		if(new_row_sno == 1){
			$('.table_section tbody').append('<tr class="bundle_kit_main_product"><td>'+new_row_sno+'</td><td class="product_name" >'+Bundle_Kit_Name+'</td><td><span class="variant_sku">'+Bundle_Kit_SKU+'</span></td><input type="hidden" class="bundle_kit_main_price" value="$0.00"><td class="default_price">$0.00</td>'+empty_cell+'<td></td><td></td><td><form action=""><input type="number" style="display:none" name="adjusted_price" id="adjusted_price" value="" autocomplete="off"><span class="no_adjusted_price">--</span></form></td><td><form action=""><span class="bundle_kit_total_price">--</span></form></td><td>App Product</td><td></td></tr>');
		}
		var new_row_sno = $('.table_section table tbody tr').length + 1;
		$('.table_section tbody').append('<tr class="new_row"><td>'+new_row_sno+'</td><td class="product_name" product_id='+id+'>'+product_name+'</td><td><span class="variant_sku" id='+variant_sku_id+'>'+variant_sku+'</span></td><td class="product_price default_price">$'+price+'</td><td><form action=""><input type="checkbox" class="kit_mandatory" id="mandatory" name="mandatory" value="" selected=""></form></td>'+qty_cell+'<td><form action=""><input type="checkbox" id="price_adjustment" name="price_adjustment" value="" selected=""></form></td><td><form action=""><input type="number" style="display:none" name="adjusted_price" id="adjusted_price" value="" autocomplete="off"><span class="no_adjusted_price">--</span></form></td><td><span class="total_adjusted_price"></span></td><td>'+data_source+'</td><td><a href="javascipt:void(0);" class="delete_icon" id="remove_bundle_kit" product-id='+id+'><svg id="svg840" xml:space="preserve" width="302.362" height="302.362" viewBox="0 0 302.362 302.362"><metadata id="metadata846"><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /><dc:title></dc:title></cc:Work></rdf:RDF></metadata><defs id="defs844" /><g id="g848" transform="matrix(1.3333333,0,0,-1.3333333,0,453.54333)"><g id="g2044" transform="translate(-56.692997,56.693003)"><path d="m 129.5433,249.2014 h 7.9141 v 26.3494 h 65.2427 v -26.3494 h 7.9138 v 24.1058 c 0,5.5978 -4.5564,10.1574 -10.1548,10.1574 h -60.7607 c -5.5984,0 -10.1551,-4.5596 -10.1551,-10.1574 z" style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none" id="path1468" /><path d="m 64.7535,247.8163 v -7.9149 H 83.6841 L 99.4878,60.3023 c 0.1798,-2.043 1.8905,-3.6094 3.9413,-3.6094 H 236.414 c 2.0506,0 3.7635,1.5667 3.9418,3.6131 l 15.8035,179.5954 h 19.2447 v 7.9149 z M 232.7907,64.6067 H 107.0529 L 91.6291,239.9014 h 156.5881 z" style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none" id="path1470" /><path d="m 141.9055,102.7171 -3.6403,99.3707 -7.9089,-0.2911 3.64,-99.3696 z" style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none" id="path1472" /><path d="m 166.1219,201.9351 v -99.364 h 7.9137 v 99.364 z" style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none" id="path1474" /><path d="m 209.7967,201.7939 -7.9093,0.29 -3.6382,-99.3682 7.9095,-0.2892 z" style="fill:#231f20;fill-opacity:1;fill-rule:evenodd;stroke:none" id="path1476" /></g></g> </svg></a></td>');

		
		
		$("#product_name").val("");
		$('.sku_details').remove();
		$("a.btn.product_add_btn").css("display","inline-block");
		
		if(product_type == "Kit"){
			console.log(product_type);
			$(".kit_mandatory").prop("checked",true);
			$(".kit_mandatory").prop("disabled",true);
			$('.td-qty, .th-qty').css('display','table-cell');
		}else{
			console.log(product_type);
			$('.td-qty, .th-qty').css('display','none');
		}

		}else{
			alert("Please enter information in all the fields");
		}

		
	});
	//remove_bundle_kit
	$(document).on('click','#remove_bundle_kit',function(){
		console.log("remove_bundle_kit");
		console.log($(this).parents().closest('tr.new_row'));
		$(this).parents().closest('tr.new_row').remove();
		added_products 
			 var remove_Item = $(this).attr("product-id");

                    //console.log('Array before removing the element = ' + parent_visible_array);
                    added_products = $.grep(added_products, function(value) {
                        return value != remove_Item;
                    });
	});
	
	$(document).on('click','.edit_and_save_buttton',function(){
		if (confirm("Are you sure to save changes on Bundle or Kits!")){

			var bundle_kit_id = $("#Bundle_Kit_id").val();
			if(isNaN(bundle_kit_id) == false){
			}else{
				console.log("bundle_id_else");
				bundle_kit_id = null;
			}
			var bundle_kit_name = $("#Bundle_Kit_Name").val();
			var bundle_kit_sku = $("#Bundle_Kit_SKU").val();
			var type = $('#product_type option').filter(':selected').val();
			var store_hash = $('.store_id').val();
			
			var product_details =[];
			var current_product_array=[];

			$(".edit_bundle_kit .table_section tbody,.add_bundle_table tbody").find('tr').each(function() {
				var name= $(this).find('.product_name').text().trim();
				var id = $(this).find('.product_name').attr('product_id');
				id = parseInt(id);
				console.log("product_idsss");
				console.log(id);
				var variant_id = $(this).find('.variant_sku').attr('id');
				console.log("variant_id");
				console.log(variant_id);

				var variant_sku = $(this).find('.variant_sku').text();
				console.log("variant_sku");
				console.log(variant_sku);

				if(variant_id == undefined || variant_id == ""){
					variant_id = null;
					
				}
				if(variant_sku == '--' || variant_sku == undefined || variant_sku == ""){
					variant_sku = null;
				}
				console.log(variant_id);
				var bundle_kit_product_id = $(this).find('.product_name').attr('bundle-kit-product-id');
				if(isNaN(bundle_kit_product_id) == false){
					console.log("if");
					bundle_kit_product_id = parseInt(bundle_kit_product_id);
				}else{
					console.log("else");
					bundle_kit_product_id= null;
				}
				console.log(bundle_kit_product_id);
				console.log("product_id");
				console.log(id);

				var mandatory = "";
				 if($(this).find('#mandatory').prop("checked") == true){
					mandatory = "true";
				}else{ mandatory = "false";}
				console.log("mandatory");
				console.log(mandatory);

				var price_adjustment ="";
				if($(this).find('#price_adjustment').prop("checked") == true){
					var price_adjustment ="true";
				}else{ var price_adjustment ="false";}

				console.log("price_adjustment");
				console.log(price_adjustment);
				var adjusted_price;
				if($(this).find('#price_adjustment').prop("checked") == true){
					if($(this).find('td').find('#adjusted_price').val() != ""){
				    adjusted_price = $(this).find('td').find('#adjusted_price').val();
					adjusted_price = adjusted_price.replace(",","");
					console.log("adjusted_price_range");
					console.log(adjusted_price);
					}else{
						adjusted_price = null;
					}
				}else{
					 adjusted_price = null;
				}
				console.log("adjusted_price");
				console.log(adjusted_price);

				var qty = $(this).find('.td-qty .product_qty').val();
				console.log(qty)
					console.log("quantity_test");
				qty = parseInt(qty);
				if(type == "Kit"){
					qty = qty;
					mandatory = "true";
				}else{
					qty = null;
				}
				if(isNaN(id) == false){
				current_product_array.push({"bundle_kit_product_id":bundle_kit_product_id,"product_id": id,"destination_variant_id":variant_id,"sku":variant_sku,"is_mandatory":mandatory,"is_price_adjustment":price_adjustment,"adjusted_price":adjusted_price,"qty":qty});
				}
			});
			var arr = {"product_details":current_product_array};
			//var myJSON = JSON.stringify(product_details);
			console.log("product_details");
			console.log(product_details);
			var json = JSON.stringify(arr);
			console.log(json);
			//return false;
			console.log(arr);
			$.ajax({
				url : "save-bundle-kit?store="+store_hash,
				type : 'POST',
				data : {bundle_kit_id: bundle_kit_id,bundle_kit_name:bundle_kit_name,type:type,product_details:json,bundle_kit_sku:bundle_kit_sku},
				dataType: "json",
				crossDomain: true,
				success: function(data) {
					//var result=JSON.parse(data);
					console.log("result-checking");
					//console.log(result);
					//location.reload();
					window.location.href="list?store="+store_hash+"&type=both";
					//console.log(dataparam_error_code);
				}
			});
		}
	});
	$(document).on('change','#price_adjustment',function(){
		console.log("test1");
	if($(this).prop("checked") == true){
		console.log("test2");
		$(this).parents().closest("tr").find(".no_adjusted_price").css('display','none');
		$(this).parents().closest("tr").find("#adjusted_price").css('display','block');
	}else{
		console.log("test3");
		$(this).parents().closest("tr").find("#adjusted_price").css('display','none');
		$(this).parents().closest("tr").find(".no_adjusted_price").css('display','block');
	}
	//$('.product_qty').keyup();
	});
	$(document).on('click','.cancel_button',function(){
		var store_hash = $('.store_id').val();
		window.location.href="list?store="+store_hash+"&type=Bundle";
	});
	
	if($('#page_value').val() == 'edit_bundle_kit_page' || $('#page_value').val() == "add_bundle_kit_page"){
			var store_hash = $('.store_id').val();
		$.ajax({url:"add-product?store="+store_hash,type:'Post',async:false,success:function(data){
			product_details = jQuery.parseJSON(data);
			console.log(product_details);
			console.log("product_details");
				if(product_details !=null){
					populateProductDetails('product_name',product_details);
				}
			}
		});
	}
	//delete_icon
	$(document).on('click','.delete_bundle_kit',function(){
		var bundle_kit_value = $(this).attr('id');
		console.log("bundle_kit_value");
		console.log(bundle_kit_value);
		bundle_kit_value = bundle_kit_value.replace("delete_bundle_kit_","");
		console.log(bundle_kit_value);
		var store_hash = $('.store_id').val();
		if (confirm("Are you sure to remove Bundle or Kits!")) 
		{
			$.ajax({
				url : "delete-bundle-kit?store="+store_hash,
				type : 'POST',
				data : {bundle_kit_id: bundle_kit_value},
				dataType: "json",
				crossDomain: true,
				success: function(data) {
					//var result=JSON.parse(data);
					console.log("result-checking");
					//	console.log(result);
						window.location.href="list?store="+store_hash+"&type=both";
						//console.log(dataparam_error_code);
					
				}
			});
		}

	});
	$(document).on('click','.delete_product_bundle_kit',function(){
		var bundle_kit_value = $(this).attr('bundle-kit-product-id');
		console.log("bundle_kit_value");
		console.log(bundle_kit_value);
		var product_id =$(this).attr('id');
			product_id = product_id.replace("delete_product_bundle_kit_","");
		console.log(product_id);
		var store_hash = $('.store_id').val();
		if (confirm("Are you sure to remove product from Bundle or Kits!")) 
		{
			$.ajax({
				url : "delete-product-bundle-kit?store="+store_hash,
				type : 'POST',
				data : {bundle_kit_id: bundle_kit_value,product_id:product_id},
				dataType: "json",
				crossDomain: true,
				success: function(data) {
					//var result=JSON.parse(data);
					console.log("result-checking");
					//	console.log(result);
						//window.location.href="list?store="+store_hash+"&type=Bundle";
						location.reload();
						//console.log(dataparam_error_code);
					
				}
			});
		}

	});
	//btn_settings
	var view_display ="";
	$(document).on('change','.status-switch',function(){
		if($(this).hasClass("checked")){
			$(this).removeClass("checked");
			$(this).addClass("unchecked");
			//$(".status-switch").removeClass("unchecked");
			$(".status-switch").prop('checked', false);
		}else{
			$(".status-switch").prop('checked', false);
			$(".status-switch").removeClass("checked");
			$(this).removeClass("unchecked");
			$(this).addClass("checked");
			$(this).prop('checked', true);
			if($(this).hasClass("grid")){
				view_display = "grid";
			}else if($(this).hasClass("list")){
				view_display = "list";
			}else if($(this).hasClass("embedded")){
				view_display = "embedded";
			}
			
		}
	});
	var bundle_kit_display = '';
	$(document).on('change','.switch-display',function(){
		if($(this).hasClass("checked")){
			$(this).removeClass("checked");
			$(this).addClass("unchecked");
			$(this).prop('checked', false);
			bundle_kit_display = 'false';
		}else{
		
			//$(this).prop('checked', false);
			//$(this).removeClass("checked");
			$(this).removeClass("unchecked");
			$(this).addClass("checked");
			$(this).prop('checked', true);
			bundle_kit_display = 'true';
			
		}
	});
	//param_is_embedded_display
	var param_is_embedded_display ="";
	$(document).on('change','.bundle_embed_display',function(){
		if($(this).hasClass("checked")){
			$(this).removeClass("checked");
			$(this).addClass("unchecked");
			$(this).prop('checked', false);
			param_is_embedded_display = 'false';
		}else{
		
			//$(this).prop('checked', false);
			//$(this).removeClass("checked");
			$(this).removeClass("unchecked");
			$(this).addClass("checked");
			$(this).prop('checked', true);
			param_is_embedded_display = 'true';
			
		}
	});
	$(document).on('click','.btn_settings',function(){
		var view = '';
		var title = '';
		var store_hash = $('.store_id').val();
		var custom_field_value = '';
		var flag = true;

		if(view_display != ""){
		  view = view_display;
		}else{
			alert("Please select Any one of view");
			return false;
			//view = "list";
		}

		title = $("#display_view").val();
		custom_field_value = $('#custom_field_label').val();
		
		if(title == ""){
			$("#display_view").css('border-color','red');
			flag = false;
		}

		if(custom_field_value == ""){
			$("#custom_field_label").css('border-color','red');
			flag = false;
		}
		
		if(flag == true){
			if (confirm("Are you sure to save the changes!")){
				$.ajax({
					url : "settings?store="+store_hash,
					type : 'POST',
					data : {title: title, view: view, custom_field_value: custom_field_value,display_settings:bundle_kit_display,param_is_embedded_display:param_is_embedded_display},
					dataType: "json",
					crossDomain: true,
					success: function(data) {
						//var result=JSON.parse(data);
						console.log("result-checking");
						//	console.log(result);
						//window.location.href="list?store="+store_hash+"&type=Bundle";
						location.reload();
					}
				});
			}
		}
	});
	function populateProductDetails(id,data){
		console.log('data');
		console.log(data);
		$("#"+id).autocomplete({
			source: data,
			minLength:2,
			select : function(event,ui){
				console.log('ui');
				console.log(ui);
				console.log("ui.item.label");
				console.log(ui.item.label);
				console.log(ui.item.bundle_product_type);
				var bundle_product_type = '';
				if(ui.item.bundle_product_type == null){
					bundle_product_type = "--";
				}else{
					bundle_product_type = ui.item.bundle_product_type;
				}

				show_overlay();
				$("#"+id).val(ui.item.label);
				$("#product_name").attr('price_data',ui.item.price);
				$("#product_name").attr('product_id',ui.item.product_id);
				$("#product_name").attr('product_name',ui.item.label);				
				$("#product_name").attr('product_source',bundle_product_type);
				$("#product_name").attr('product_sku',ui.item.product_sku);
				
				$.ajax({
					url : "get-product-sku-details?store="+store_hash,
					type : 'POST',
					data : {product_id: ui.item.product_id},
					dataType: "json",
					crossDomain: true,
					success: function(data) {
						if(data.length == 0){
							hide_overlay();
							$(".product_add_btn").css("display","inline-block");
						}else{
						//var result=JSON.parse(data);
						console.log("result-sku-details-checking");
						//	console.log(data);
						var sku_details = '<div class="form_field_container sku_details"><label>Select Variant:</label><select name="sku_type" id="sku_type"><option value="select">---Select---</option>';
							  $.each(data, function(index, value) {
								  console.log(value);
								  var sku_price = 0;
								  if(value.price != null){
									  sku_price = value.price;
								  }
								  console.log(sku_price);
								  console.log("sku_price");
								  sku_details = sku_details + '<option value='+value.sku+'***'+value.destination_sku_id+' sku-id='+value.destination_sku_id+' sku-price='+sku_price+'>'+value.sku+' ('+ value.option_details +')'+'</option>'
							  });
						sku_details = sku_details + '</select><a href="javascript:void(0);" class="btn">Add</a></div>';
						console.log("sku_details");
						console.log(sku_details);

						$(".add_page_container").append(sku_details);
						$(".product_add_btn").css("display","none");
						hide_overlay();
							
						}
						
					}
				});
				$('.bundle_kit_form').submit();
				   
			}
		});
	}

	//product_qty$('.product_qty').keyup();
	$(document).on('keyup','.product_qty',function(e){
	//$(".product_qty").keyup(function(e){
		var final_price ="";
		var quantity =$(this).parents().closest("tr").find(".product_qty").val();
		console.log(quantity);
		var default_price = $(this).parents().closest("tr").find(".default_price").text();
		//console.log(default_price);
		//console.log("default_price");
		default_price = default_price.replace("$","");
		var adjusted_price = $(this).parents().closest("tr").find("#adjusted_price").val();
		//console.log(adjusted_price);
		//console.log("adjusted_price");
		//if($("#adjusted_price").is(":visible")){
		if(adjusted_price != "" && $(this).parents().closest("tr").find("#adjusted_price").is(":visible")){
			final_price = parseFloat(adjusted_price) * (quantity);
			console.log(final_price);
		}else{
			final_price = parseFloat(default_price) * (quantity);
			console.log(final_price);
		}
		
		/*if($(this).parents().closest("tr").hasClass("bundle_kit_main_product")){
		var old_price = $(this).parents().closest("tr").find(".total_adjusted_price").text().replace("$","");
		var new_price = parseFloat(final_price) + parseFloat(old_price); 
		console.log(new_price);
		console.log("new_price");
	}*/
		final_price = "$"+formatMoney(final_price,2);
		console.log(final_price);
	console.log("final_price_Test");
	
		$(this).parents().closest("tr").find(".total_adjusted_price").text(final_price);
		//bundle_kit_total_price 
		var total = 0;
		if($(".bundle_kit_main_price").val() != '--' && $(".bundle_kit_main_price").text() != '' ){
			total =$(".bundle_kit_main_price").val();
			total = total.replace("$","");
		}
		var current_total = 0;
		$(".total_adjusted_price").each(function() {
			//console.log("entered");
			//console.log($(this).text());
			
			if($(this).text() != "" && $(this).text() != "--"){
				var current_price = $(this).text().replace("$","");
				console.log("before_current_price");
			console.log(current_price);
			console.log("before_current_total");
			console.log(current_total);
				current_price = current_price.toString().replace(/\$/g,"");
				current_total = current_total.toString().replace(/\$/g,"");
				current_price= current_price.toString().replace(/\,/g,"");
				current_total = current_total.toString().replace(/\,/g,"");
				//current_price = current_price.toString().replace(/\./g,"");
			//current_total =	current_total.toString().replace(/\./g,"");

			console.log("after_current_price");
			console.log(current_price);
			console.log("after_current_total");
			console.log(current_total);
			 current_total = parseFloat(current_total) + parseFloat(current_price);
			 console.log("total_adjust_price");
			console.log(current_total);
			
			}
		});
		//current_total = parseFloat(current_total) + parseFloat(total);
		current_total = "$"+formatMoney(current_total,2);
			console.log("after_current_total");
			console.log(current_total);
			$(".bundle_kit_total_price").text(current_total);
	});

	$(document).on('click','.bundle-reset',function(e){
		var bundle_kit_id =  urlParam('id');

	if (confirm("Are you sure to reset Bundle or Kit!")){
		$.ajax({
			url : "delete-bundle?store="+store_hash,
			type : 'POST',
			data : {bundle_kit_id: bundle_kit_id},
			dataType: "json",
			crossDomain: true,
			success: function(data) {
				location.reload();
				
			}
		});
	}

	});
	$(document).on('keyup','#adjusted_price',function(e){
		console.log("textsss");
		var keyCode = e.which;
				var final_price ="";
		var quantity =$(this).parents().closest("tr").find(".product_qty").val();
		console.log(quantity);
		if(quantity == ""){
			quantity = 1 ;
		}
		var default_price = $(this).parents().closest("tr").find(".default_price").text();
		//console.log(default_price);
		//console.log("default_price");
		default_price = default_price.replace("$","");
		var adjusted_price = $(this).parents().closest("tr").find("#adjusted_price").val();
		//console.log(adjusted_price);
		//console.log("adjusted_price");
		//if($("#adjusted_price").is(":visible")){
		if(adjusted_price != "" && $(this).parents().closest("tr").find("#adjusted_price").is(":visible")){
			final_price = parseFloat(adjusted_price) * (quantity);
			console.log(final_price);
		}else{
			final_price = parseFloat(default_price) * (quantity);
			console.log(final_price);
		}
		
		/*if($(this).parents().closest("tr").hasClass("bundle_kit_main_product")){
		var old_price = $(this).parents().closest("tr").find(".total_adjusted_price").text().replace("$","");
		var new_price = parseFloat(final_price) + parseFloat(old_price); 
		console.log(new_price);
		console.log("new_price");
	}*/
		final_price = "$"+formatMoney(final_price,2);
		console.log(final_price);
	console.log("final_price_Test");
	
		$(this).parents().closest("tr").find(".total_adjusted_price").text(final_price);
		//bundle_kit_total_price 
		var total = 0;
		if($(".bundle_kit_main_price").val() != '--' && $(".bundle_kit_main_price").text() != '' ){
			total =$(".bundle_kit_main_price").val();
			total = total.replace("$","");
		}
		var current_total = 0;
		$(".total_adjusted_price").each(function() {
			console.log("entered");
			console.log($(this).text());
			
			if($(this).text() != "" && $(this).text() != "--"){
				var current_price = $(this).text().replace("$","");
				console.log("before_current_price");
			console.log(current_price);
			console.log("before_current_total");
			console.log(current_total);
				current_price = current_price.toString().replace(/\$/g,"");
				current_total = current_total.toString().replace(/\$/g,"");
				current_price= current_price.toString().replace(/\,/g,"");
				current_total = current_total.toString().replace(/\,/g,"");
				//current_price = current_price.toString().replace(/\./g,"");
			//current_total =	current_total.toString().replace(/\./g,"");

			console.log("after_current_price");
			console.log(current_price);
			console.log("after_current_total");
			console.log(current_total);
			 current_total = parseFloat(current_total) + parseFloat(current_price);
			 console.log("total_adjust_price");
			console.log(current_total);
			
			}
		});
		//current_total = parseFloat(current_total) + parseFloat(total);
		current_total = "$"+formatMoney(current_total,2);
			console.log("after_current_total");
			console.log(current_total);
			$(".bundle_kit_total_price").text(current_total);


		/*
		8 - (backspace)
		32 - (space)
		48-57 - (0-9)Numbers
		*/

		if ( (keyCode != 8 || keyCode != 190 || keyCode ==32 ) && (keyCode < 48 || keyCode > 57)) { 
		
		return false;
		}else{

					
		}
	});
});
//product_name

function ProcessExcel(data) {
	//Read the Excel File data.
	var workbook = XLSX.read(data, {
		type: 'binary'
	});

	//Fetch the name of First Sheet.
	var firstSheet = workbook.SheetNames[0];

	//Read all rows from First Sheet into an JSON array.
	var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
	return excelRows;
};

$(window).on("load resize orientationchange",function(){
	/*Equal height*/
	$.fn.extend({
	  equalHeightsDiv: function() {
		var top=0;
		var row=[];
		var classname=('equalHeights'+Math.random()).replace('.','');
		$(this).each(function() {
		  var thistop=$(this).offset().top;
		  if (thistop>top) {
			 $('.'+classname).removeClass(classname); 
			 top=thistop;
		  }
		  $(this).addClass(classname);
		  $(this).height('auto');
		  var h=(Math.max.apply(null, $('.'+classname).map(function(){ return $(this).outerHeight();}).get()));
		  $('.'+classname).height(h);
	   }).removeClass(classname); 
	 }
	}); 
		/*page height*/
		var window_height=window.innerHeight;
		var header_height=$("header").outerHeight(true);
		var footer_height=$("footer").outerHeight(true);
		var height_section=header_height+footer_height;
		var main_height=window_height-height_section
		$("main").css("min-height",main_height);
		$(".main_container").css("min-height",main_height);
		$(".container.settings_page").parents().find("body").css("overflow","hidden");
		/*page height*/
});





function urlParam(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	console.log(results);
	if(results != null){
	return results[1] || 0;
	}else{
		return null;
	}
}
$(document).on('click', '.pagination_li', function() {
    let current_active_page = $(this).find('a').text();
    var current_page = parseInt($('.pagination_list .active').text());
    console.log(current_page);
    console.log("current_page");
	console.log(current_active_page);
    var customers_per_page = 15;
    var total_count = $('.total_count').val();

    if (current_active_page == 'Previous') {
		console.log(previous_page);
        var first_child_value = $(".pagination_list li.page_no").first().find('a').text();
        var previous_page = current_page - 1;
        if (previous_page > 0) {

            $('.pagination_li').removeClass('active');
            $('.' + previous_page).addClass('active');
			
            pagination_list_append_previous_customer(current_page, total_count, first_child_value, customers_per_page);
			if($('#page_value').val() =="bundle_kit_page"){
			$('.current_page').val(previous_page);
			$('.bundle_kit_form').submit();
			}
        }
    } else if (current_active_page == 'Next') {
        var last_child_value = $(".pagination_list li.page_no").last().find('a').text();
		console.log("last_child_value_value");
		console.log(last_child_value);
        var next_page = current_page + 1;
        //console.log(next_page);
        //console.log("next_page");

        var no_of_pages = total_count;
        //console.log(no_of_pages);
        //if(next_page <= total_customer_count_value){
        if (current_page != no_of_pages) {
            console.log("checkinglog");
			  console.log(next_page);
        console.log("next_page");
            
            $('.pagination_li').removeClass('active');
            $('.' + next_page).addClass('active');
            pagination_list_append_next_customer(current_page, total_count, last_child_value, customers_per_page);
			if($('#page_value').val() =="bundle_kit_page"){
			$('.current_page').val(next_page);
			$('.bundle_kit_form').submit();
			}
        } else {
            //hide_overlay();
        }
    } else {
       
        //   salesPersonData(customer_group_name,customer_name,current_active_page,customers_per_page);
        $('.pagination_li').removeClass('active');
        $(this).addClass('active');
		console.log("current_active_page");
		console.log(current_active_page);
		if($('#page_value').val() =="bundle_kit_page"){
			$('.current_page').val(current_active_page);
			console.log($('.current_page').val());
			$('.bundle_kit_form').submit();
		}
    }
});

function pagination_list_append_previous_customer(page_number, total_customers_list, first_count, customers_per_page) {
    let no_of_pages = total_customers_list;
    if (total_customers_list) {
        if (parseInt(page_number) == first_count) {
            var start = parseInt(first_count) - 2;
            if (start == 0) {
                start = 1;
            }
            var end = start + 4;
            if (end > no_of_pages) {
                end = no_of_pages;
            }
            $('.pagination .pagination_list').html('');
            var i;
            $('.pagination .pagination_list').append("<li class='previous pagination_li'><a>Previous</a></li>");
            for (i = start; i <= end; i++) {
                $('.pagination .pagination_list').append("<li class='" + i + " pagination_li page_no' id='" + i + "'><a>" + i + "</a></li>");
            }
            $('.pagination .pagination_list').append("<li class='next pagination_li'><a>Next</a></li>");
            $('.pagination_li').removeClass('active');
            $("." + page_number).addClass('active');
        }
    }
}

function pagination_list_append_next_customer(page_number, total_customers_list, last_count, customers_per_page) {
    let no_of_pages = total_customers_list;
    if (total_customers_list) {
      //  alert(page_number);
        console.log(last_count);
        if (parseInt(page_number) == last_count) {
			console.log("if");
           // var start = parseInt(last_count) - 2;
			  var start = last_count;
            //console.log(start);
           // console.log("start");
            var end = parseInt(last_count) + 4;
			//console.log(end);
            if (end > no_of_pages) {
                end = no_of_pages;
            }
           // console.log(end);
           // console.log("end");
            $('.pagination .pagination_list').html('');
            var i;
            $('.pagination .pagination_list').append("<li class='previous pagination_li'><a>Previous</a></li>");
            for (i = start; i <= end; i++) {
                $('.pagination .pagination_list').append("<li class='" + i + " pagination_li page_no' id='" + i + "'><a>" + i + "</a></li>");
            }
            $('.pagination .pagination_li').append("<li class='next pagination_li'><a>Next</a></li>");
            $('.pagination_li').removeClass('active');
			console.log("page_number");
			console.log(page_number);
			$("." + page_number +"").addClass('active');
           // page_number = page_number + 1;
            
        }
    }
}
function formatMoney(number, decPlaces, decSep, thouSep) {
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSep = typeof decSep === "undefined" ? "." : decSep;
    thouSep = typeof thouSep === "undefined" ? "," : thouSep;
    var sign = number < 0 ? "-" : "";
    var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
    var j = (j = i.length) > 3 ? j % 3 : 0;

    return sign +
        (j ? i.substr(0, j) + thouSep : "") +
        i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
        (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

//Show Overlay Function
function show_overlay() {
    console.log("show_overlay called");
    $("#overlay").css("display", "table");
}
//Hide Overlay Function
function hide_overlay() {
    console.log("hide called");
    $("#overlay").css("display", "none");
}