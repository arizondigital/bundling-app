$(document).ready(function(){
	//uom update
	/*	$('#uom_product_table thead tr')
		.first().clone(true)
       .addClass('filters noExport')
        .appendTo('#uom_product_table thead');
	console.log('thead');
    var table = $('#uom_product_table').DataTable({
        orderCellsTop: true,
       // fixedHeader: true,
        initComplete: function () {
            var api = this.api();
 
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
					console.log(colIdx);
                    // Set the header cell to contain the input element
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    $(cell).html('<input type="text" placeholder="' + title + '" />');
 
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('keyup change', function (e) {
                            e.stopPropagation();
 
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
 
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });
	$('.filters th').addClass('noExport');*/
    $(document).on('click',".plus_img_orders",function(){
		console.log("clicked");
		$(this).hide();
		$(this).parent().find(".minus_img_orders").show();
		$(this).closest('tr').next('tr').show();
	});
	$(document).on('click','.minus_img_orders',function(){
		$(this).hide();
		$(this).parent().find(".plus_img_orders").show();
		$(this).closest('tr').next('tr').hide();
	});

    $('#import').change(function() {
        console.log('radio');
        if($(this).prop('checked')){
            $('#export').prop('checked',false);
            $('.export_container').css('display','none');
            $('.import_container').css('display','block');
        }else{
            $('.import_container').css('display','none');
        }
        /*if (this.value == 'import') {
            $('.export_container').css('display','none');
            $('.import_container').css('display','block');
        }
        else if (this.value == 'export') {
            $('.import_container').css('display','none');
            $('.export_container').css('display','block');
            
        }*/
    });
    $('#export').change(function() {
        console.log('radio');
        if($(this).prop('checked')){
            $('#import').prop('checked',false);
            $('.import_container').css('display','none');
            $('.export_container').css('display','block');
        }else{
            $('.export_container').css('display','none');
        }
      
    });
    
	$('.import-btn').on("click", function(e){
		console.log('export/import');
		var excelRows = [];
		//var error_message = [];
		//Reference the FileUpload element.
		var fileUpload = document.getElementById("fileUpload");
        console.log('fileUpload'+fileUpload.value);
		var regex = /^([a-zA-Z0-9\s_\\.\-:(x)])+(.xls|.xlsx)$/;
		if (regex.test(fileUpload.value.toLowerCase())) {
			console.log('export/import1');
			if (typeof (FileReader) != "undefined") {
				console.log('export/import2');
				var reader = new FileReader();
				if (reader.readAsBinaryString) {
					console.log('export/import3');
                    reader.onload = function (e) {
						console.log('export/import4');
                        excelRows = ProcessExcel(e.target.result);
						console.log(excelRows);
						console.log(excelRows.length);
						for (var i = 0; i < excelRows.length; i++) {
							var product_id = excelRows[i]['Product Id'];
                            var sku = excelRows[i]['SKU'];
                            if($.isNumeric(excelRows[i]['Base Uom'])){
                                var base_uom = excelRows[i]['Base Uom'];
                            }else{
                                var base_uom = null;
                            }
                            if($.isNumeric(excelRows[i]['Sales Uom'])){
                                var sales_uom =excelRows[i]['Sales Uom'];
                            }else{
                                var sales_uom = null;
                            }
                            if(excelRows[i]['UOM Override'] == 'Yes'){
                                var uom_override = 1;
                            }else{
                                var uom_override = 0;
                            }
                            var store_hash = $('.store_id').val();
                            if(base_uom != null && sales_uom != null && uom_override != null && sku != null && product_id != null){
                                $.ajax({
                                    url : "save-sales-uom?store="+store_hash,
                                    async: false,
                                    type : 'POST',
                                    data : {product_id: product_id,sku:sku,base_uom:base_uom,sales_uom:sales_uom,is_uom_override:uom_override},
                                    dataType: "json",
                                    crossDomain: true,
                                    success: function(data) {
                                        console.log("result-checking");
                                        console.log(data);
                                        if(i == excelRows.length-1){
                                            window.location.href="sales_uom?store="+store_hash;
                                        }
                                    }
                                });
                            }else{
                                alert('Please enter valid input...');
                                if(i == excelRows.length-1){
                                    window.location.href="sales_uom?store="+store_hash;
                                }
                            }
						}
                        
                        setTimeout(function(){
                            $('.modal').css('display', 'none');
                            $('.modal-backdrop').css('display', 'none');
                        }, 2000);
                       
                    };
                    reader.readAsBinaryString(fileUpload.files[0]);
				}
			}
		}
		
		
	})
    
    $('.save_uom_settings').on("click", function(e){
		console.log('save_uom_settings');
        if(confirm("Are you sure to save changes on the settings?")){
            var store_hash = $('.store_id').val();
            var buy_in_base_sale_uom = $('#uom_types').val();
            if($('#quantity_restriction').prop('checked')){
                var restrict_qty_change_cart = 1;
            }else{
                var restrict_qty_change_cart = 0;
            }
            if($('#uom_conversion').prop('checked')){
                var display_uom_conversion_details = 1;
            }else{
                var display_uom_conversion_details = 0;
            }

            if(buy_in_base_sale_uom != null){
                $.ajax({
                    url : "save_uom_settings?store="+store_hash,
                    async: false,
                    type : 'POST',
                    data : {buy_in_base_sale_uom: buy_in_base_sale_uom,restrict_qty_change_cart:restrict_qty_change_cart,display_uom_conversion_details:display_uom_conversion_details},
                    dataType: "json",
                    crossDomain: true,
                    success: function(data) {
                        console.log("result-checking");
                        console.log(data);
                        //if(i == excelRows.length-1){
                            window.location.href="uom_settings?store="+store_hash;
                        //}
                    }
                });
            }else{
                alert('Please select proper input...');
            }
        }                                  
                            
	})
    $('.export-btn').on("click", function(e){
        console.log('export');
        $("#uom_product_table").table2excel({
            exclude: ".noExport",
            name: "Products",
            filename: "UOM_Products",
			fileext:".xls",
        });
        setTimeout(function(){
            $('.modal').css('display', 'none');
            $('.modal-backdrop').css('display', 'none');
        }, 3000);
    });
	
	//uom update
    //uom update
	$(document).on('click','.cancel_product_button',function(){
		var store_hash = $('.store_id').val();
		window.location.href="sales_uom?store="+store_hash+"";
	});
	$(document).on('click','.edit_product_button',function(){
		if (confirm("Are you sure to save changes on the products?")){
            console.log('sale_uom', $('.sales_uom').val());
			var product_id = $(".product_name").attr('product_id');
            product_id = 150;
			var sku = $(".variant_sku").text();
            sku = "H05P_IRON";
			var base_uom = $('.base_uom').val();
            base_uom = 1;
			var sales_uom = $('.uom_product_table .sales_uom').val();
            sales_uom = 5;
			var uom_override = false;
            
			if($('.uom_override').prop('checked') == true){
				uom_override = true;
			}
			var store_hash = $('.store_id').val();
			$.ajax({
				url : "save-sales-uom?store="+store_hash,
				type : 'POST',
				data : {product_id: product_id,sku:sku,base_uom:base_uom,sales_uom:sales_uom,is_uom_override:uom_override},
				dataType: "json",
				crossDomain: true,
				success: function(data) {
					console.log("result-checking");
					console.log(data);
					//window.location.href="sales_uom?store="+store_hash;
				}
			});
		}
	});
    //Edit Icon in the Inventory Page
	$(document).on('click', '#edit .edit_img',function(e){
		$(this).closest('tr').find('.base_uom').css('border', '1px solid rgb(171, 170, 170)');
        $(this).closest('tr').find('.base_uom').css('background-color', 'white');
		$(this).closest('tr').find('.base_uom').prop('readonly', false);

		$(this).closest('tr').find('.sales_uom').css('border', '1px solid rgb(171, 170, 170)');
        $(this).closest('tr').find('.sales_uom').css('background-color', 'white');
		$(this).closest('tr').find('.sales_uom').prop('readonly', false);

        $(this).closest('tr').find('.measurement_value').css('border', '1px solid rgb(171, 170, 170)');
        $(this).closest('tr').find('.measurement_value').css('background-color', 'white');
		$(this).closest('tr').find('.measurement_value').css('appearance', 'auto');
        $(this).closest('tr').find('.measurement_value').removeAttr('disabled');

		$(this).closest('tr').find('.uom_override').removeAttr('disabled');

        $(this).closest('tr').find('.bundle_available').removeAttr('disabled');

        $(this).closest('tr').find('.bundle_qty').css('border', '1px solid rgb(171, 170, 170)');
        $(this).closest('tr').find('.bundle_qty').css('background-color', 'white');
		$(this).closest('tr').find('.bundle_qty').prop('readonly', false);

        $(this).closest('tr').find('.bundle_override').removeAttr('disabled');

        $(this).closest('tr').find('.broken_percentage').css('border', '1px solid rgb(171, 170, 170)');
        $(this).closest('tr').find('.broken_percentage').css('background-color', 'white');
		$(this).closest('tr').find('.broken_percentage').prop('readonly', false);

        $(this).css('display', 'none');
		$(this).closest('#edit').find('.save_img_section').css('display', 'block');
	});
	
	//Cancel Icon in the Inventory Page
	$(document).on('click', '.cancel_img',function(e){
		e.preventDefault();
		
		$(this).closest('tr').find('.base_uom').css('border', 'unset');
        $(this).closest('tr').find('.base_uom').css('background-color', 'unset');
		$(this).closest('tr').find('.base_uom').prop('readonly', true);

		$(this).closest('tr').find('.sales_uom').css('border', 'unset');
        $(this).closest('tr').find('.sales_uom').css('background-color', 'unset');
		$(this).closest('tr').find('.sales_uom').prop('readonly', true);

        $(this).closest('tr').find('.measurement_value').css('border', 'unset');
        $(this).closest('tr').find('.measurement_value').css('background-color', 'unset');
		$(this).closest('tr').find('.measurement_value').css('appearance', 'none');
        $(this).closest('tr').find('.measurement_value').removeAttr('disabled');

		$(this).closest('tr').find('.uom_override').attr('disabled', true);

        $(this).closest('tr').find('.bundle_available').attr('disabled', true);

        $(this).closest('tr').find('.bundle_qty').css('border', 'unset');
        $(this).closest('tr').find('.bundle_qty').css('background-color', 'unset');
		$(this).closest('tr').find('.bundle_qty').prop('readonly', true);

        $(this).closest('tr').find('.bundle_override').attr('disabled', true);

        $(this).closest('tr').find('.broken_percentage').css('border', 'unset');
        $(this).closest('tr').find('.broken_percentage').css('background-color', 'unset');
		$(this).closest('tr').find('.broken_percentage').prop('readonly', true);

        $(this).closest('#edit').find('.save_img_section').css('display', 'none');
		$(this).closest('#edit').find('.edit_img').css('display', 'inline-block');

	});
    /*$(document).on('change', '#uom_override', function(e){
        console.log('change');
        if($(this).prop('checked') == true){
            $(this).prop('checked', false);
        }else{
            $(this).prop('checked', true);
        }
    })*/


    $(document).on('click', '.sales_uom .save_img_section .save_img', function(e){
		e.preventDefault();
		console.log('Save icon Clicked');
        if (confirm("Are you sure to save changes on the products?")){
            console.log('sale_uom', $(this).closest('tr').find('.sales_uom').val());
            var product_id = $(this).closest('tr').find(".product_name").attr('product_id');
           // product_id = 150;
            var sku = $(this).closest('tr').find(".variant_sku").text();
           // sku = "H05P_IRON";
            var base_uom = $(this).closest('tr').find('.base_uom').val();
        // base_uom = 1;
            var sales_uom = $(this).closest('tr').find('.sales_uom').val();

            var measurement = $(this).closest('tr').find('.measurement_value').val();
        // sales_uom = 12;
            var uom_override = false;
            var bundle_available = false;
            var bundle_override = false;
            var bundle_qty = '';
            var broken_percentage = '';
            var check = false;
            console.log($(this).closest('tr').find('.uom_override').prop('checked'));
            if($(this).closest('tr').find('.uom_override').prop('checked') == true){
                uom_override = true;
                if(base_uom == '' || base_uom <= 0 ){
                    $(this).closest('tr').find('.base_uom').css('border-color', 'red');
                    check = true;
                }else{
                    $(this).closest('tr').find('.base_uom').css('border-color', 'rgb(171, 170, 170)');
                }
                if(sales_uom == '' || sales_uom <= 0){
                    $(this).closest('tr').find('.sales_uom').css('border-color', 'red');
                    check = true;
                }else{
                    $(this).closest('tr').find('.sales_uom').css('border-color', 'rgb(171, 170, 170)');
                }
                if(measurement == ''){
                    $(this).closest('tr').find('.measurement_value').css('border-color', 'red');
                    check = true;
                }else{
                    $(this).closest('tr').find('.measurement_value').css('border-color', 'rgb(171, 170, 170)');
                }

            }
            if($(this).closest('tr').find('.bundle_available').prop('checked') == true){
                bundle_available = true;
                if($(this).closest('tr').find('.bundle_qty').val() == '' || $(this).closest('tr').find('.bundle_qty').val() <= 1){
                    console.log('empty');
                    $(this).closest('tr').find('.bundle_qty').css('border-color', 'red');
                    check = true;
                }else{
                    bundle_qty = $(this).closest('tr').find('.bundle_qty').val();
                    $(this).closest('tr').find('.bundle_qty').css('border-color', 'rgb(171, 170, 170)');
                }
            }
            if($(this).closest('tr').find('.bundle_override').prop('checked') == true){
                bundle_override = true;
                if($(this).closest('tr').find('.broken_percentage').val() == '' || $(this).closest('tr').find('.broken_percentage').val() < 1){
                    console.log('empty');
                    $(this).closest('tr').find('.broken_percentage').css('border-color', 'red');
                    check = true;
                }else{
                   broken_percentage= $(this).closest('tr').find('.broken_percentage').val();
                    $(this).closest('tr').find('.broken_percentage').css('border-color', 'rgb(171, 170, 170)');
                }
            }
            var store_hash = $('.store_id').val();
            if(!check){
            $.ajax({
                url : "save-sales-uom?store="+store_hash,
                type : 'POST',
                data : {product_id: product_id,sku:sku,base_uom:base_uom,sales_uom:sales_uom,is_uom_override:uom_override,bundle_available:bundle_available, bundle_override:bundle_override, bundle_qty:bundle_qty, broken_percentage:broken_percentage, measurement_value:measurement},
                dataType: "json",
                crossDomain: true,
                success: function(data) {
                    console.log("result-checking");
                    console.log(data);
                   // window.location.href="sales_uom?store="+store_hash;
                    location.reload();
                }
            });
        }
        }

	});
    //Filer in the Inventory Page
	$(document).on('keyup', '.product_search_visible', function(event){
		$('.action-bar .product_search').val($(this).val());

		if (event.keyCode === 13) {
			$('.inventory_form').submit();
		}
	});
    $(document).on('keyup', '.sku_search_visible', function(event){
		$('.action-bar .sku_search').val($(this).val());
		
		if (event.keyCode === 13) {
			$('.inventory_form').submit();
		}
	});

    $(document).on('change', '#visibility', function(){
		$('.action-bar .is_uom_override').val($(this).val());
		$('.inventory_form').submit();
	});
    $(document).on('change', '#bundle_available', function(){
		$('.action-bar .is_bundle_available').val($(this).val());
		$('.inventory_form').submit();
	});
    $(document).on('change', '#bundle_override', function(){
		$('.action-bar .is_bundle_override').val($(this).val());
		$('.inventory_form').submit();
	});
	$(document).on('change', '#measurement', function(){
		$('.action-bar .measurement_value').val($(this).val());
		$('.inventory_form').submit();
	});
    $(document).on('click','.clear',function()
	{
        console.log('clear');
		if($(this).siblings(".txt_search").val()!="")
		{
			$(this).siblings(".txt_search").val('');
            if($(this).siblings(".txt_search").hasClass("product_search_visible")){
                $('.product_search').val('');
            }else if($(this).siblings(".txt_search").hasClass("sku_search_visible")){
                $('.sku_search').val('');
            }
		}
		$(this).siblings(".txt_search").focus();
        $('.inventory_form').submit();
	});
    $(document).on('focus','.txt_search',function()
	{
		//$(this).css("border-color","#D7D6D9");
		$(this).parent().find(".clear").show();
		$(this).parent().find(".clear-exception").show();
	});

    	/*Manage Inventory Page Import Functionality for Warehouse*/

	// Get the modal
	var modal = document.getElementById("importModal");
	if(modal){
		// Get the button that opens the modal
		var btn = document.getElementById("importBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

		// When the user clicks on the button, open the modal
		btn.onclick = function() {
		  modal.style.display = "block";
		}

		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		  modal.style.display = "none";
		}

		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
		  if (event.target == modal) {
			modal.style.display = "none";
		  }
		}
	}
	/*Manage Inventory Page Import Functionality*/

    
	/*Manage Inventory Page Import Functionality for Location*/
	$(document).on('click', '#upload', function(){
		var fileUpload = document.getElementById("fileUpload");
		//let locationId = $('#locationId').val();
		let storeId = $('.store_id').val();
		//let inventory_type = $('.inventory_location_type_container select').val();

		var regex = /^([a-zA-Z0-9\s_\\.\-:(x)])+(.csv|.txt)$/;
		if (regex.test(fileUpload.value.toLowerCase())) {
			if (typeof (FileReader) != "undefined") {
                $("#importModal").hide();
				$('.loading').css('display', 'block');
				var reader = new FileReader();
				reader.onload = function (e) {
					var rows = e.target.result.split("\n");
					console.log(rows);
					let combinedArray = [];
					rows.forEach((value, index)=>{
                        if (index === 0){ 
                            return;
                        }else{
                            let row = value.split(',');
                            console.log('row'+row);
                            if(row[1] && row[0]){
                                let temp = {
                                    productId: row[0],
                                    productName: row[1],
                                    sku: row[2],
                                    brand: row[3],
                                    baseUOM: row[4],
                                    salesUOM:  row[5],
                                    UOMOverride: row[7],
                                    bundleAvailable: row[8],
                                    bundleQty: row[9],
                                    bundleOverride: row[10],
                                    brokenBundlePercentage: row[11],
									measurement: row[6],
                                }
                                //console.log(temp);
                                combinedArray.push(temp);
						    }
                        }
					});
					console.log(combinedArray);
					//console.log(locationId);
					/*
					let flag = false;
					let parentMapping = '';
					let childArray = [];
					let productArray = [];
					combinedArray.forEach((value, index)=>{
						let tempArray = [];
						let stock;
						if(inventory_type.trim().toLowerCase() == 'no sync'){
							 stock = value['stock'];
						}else{
							stock = null;
						}
						
						if(value['type'].toLowerCase() === 'product'){
							parentMapping = value['sku'];
							tempArray['product_type'] = value['type'];
							tempArray['parent_sku'] = value['sku'];
							tempArray['product_stock'] = stock;
							tempArray['buffer_stock'] = value['buffer_stock'];
							tempArray['childs'] = '';
							tempArray['lead_time'] = value['lead_time'];
							tempArray['is_ins'] = value['is_ins'];
							tempArray['location_id'] = locationId;
							tempArray['app_installed_store_id'] = 1;
							productArray.push(tempArray);
						}
							
						let bufferStock = value['buffer_stock']? value['buffer_stock'] : null;
						if(value['type'].toLowerCase() === 'sku'){
							if(childArray[parentMapping]){
								childArray[parentMapping] = childArray[parentMapping]+','+'{"sku":"'+value['sku']+'","child_stock":'+stock+', "buffer_stock": '+bufferStock+'}';
							}else{
								childArray[parentMapping] = '{"sku":"'+value['sku']+'","child_stock":'+stock+', "buffer_stock": '+bufferStock+'}';
							}
						}
					});
					console.log("childArray", childArray);
					
					let finalArray = [];
					
					productArray.forEach((value, index)=>{
						let temp = {};
						if(childArray[value['parent_sku']]){
							productArray[index]['childs'] = '['+childArray[value['parent_sku']]+']';
							temp = {
								product_type : productArray[index]['product_type'],
								parent_sku :  productArray[index]['parent_sku'],
								product_stock :  productArray[index]['product_stock'],
								childs :  '['+childArray[value['parent_sku']]+']',
								app_installed_store_id :  storeId,
								store_location_id : locationId,
								inventory_status : 'No Sync',
								warehouse_id : null,
								is_ins :  productArray[index]['is_ins'],
								is_shippable :  null,
								lead_time :  productArray[index]['lead_time'],
								buffer_stock : productArray[index]['buffer_stock']
							}
						}else{
							temp = {
								product_type : productArray[index]['product_type'],
								parent_sku :  productArray[index]['parent_sku'],
								product_stock :  productArray[index]['product_stock'],
								childs :  '',
								app_installed_store_id :  storeId,
								store_location_id : locationId,
								inventory_status : 'No Sync',
								warehouse_id : null,
								is_ins :  productArray[index]['is_ins'],
								is_shippable :  null,
								lead_time :  productArray[index]['lead_time'],
								buffer_stock : productArray[index]['buffer_stock']
							}
						}
						finalArray.push(temp);
					});
                    */
					
					let resultStr = JSON.stringify(combinedArray);
					console.log("finalArray", JSON.stringify(resultStr));
					console.log("finalArray",JSON.parse(resultStr));

					let route = "https://apps.arizonreports.cloud/Furniture/Bundling_App/import-products?store="+storeId;
					//let logged_user = localStorage.getItem("logged_user_id");
					
					$.post( route, { importData: resultStr}, function( data, status ) {
                        if(status == 'success'){
                            console.log("Data: " + data + "\nStatus: " + status);
                            $('.loading').css('display', 'none');
                            $("#mi-modal-error-inventory").modal('show');
                        }
						//location.reload();
					});
				}
				reader.readAsText(fileUpload.files[0]);
			} else {
				alert("This browser does not support HTML5.");
			}
		} else {
			alert("Please upload a valid CSV file.");
		}
	});
	/*Manage Inventory Page Import Functionality for Location*/
    $(document).on('click', '#modal-btn-ok--pickup-details', function(){
		$("#mi-modal-pickup-details").modal('hide');
		location.reload();
	});
    $(document).on('click', '.sales_uom .pagination_uom_li', function() {
        let current_active_page = $(this).find('a').text();
        var current_page = parseInt($('.pagination_list .active').text());
        console.log(current_page);
        console.log("current_page");
        console.log(current_active_page);
        var customers_per_page = 15;
        var total_count = $('.total_count').val();
    
        if (current_active_page == 'Previous') {
            console.log(previous_page);
            var first_child_value = $(".pagination_list li.page_no").first().find('a').text();
            var previous_page = current_page - 1;
            if (previous_page > 0) {
    
                $('.pagination_uom_li').removeClass('active');
                $('.' + previous_page).addClass('active');
                
                pagination_uom_list_append_previous_product(current_page, total_count, first_child_value, customers_per_page);
                if($('#page_value').val() =="sales_uom_page"){
                $('.current_page').val(previous_page);
                $('.inventory_form').submit();
                }
            }
        } else if (current_active_page == 'Next') {
            var last_child_value = $(".pagination_list li.page_no").last().find('a').text();
            console.log("last_child_value_value");
            console.log(last_child_value);
            var next_page = current_page + 1;
            console.log(next_page);
            //console.log("next_page");
    
            var no_of_pages = total_count;
            console.log(current_page);
            console.log(no_of_pages);
            //if(next_page <= total_customer_count_value){
            if (current_page != no_of_pages) {
                console.log("checkinglog");
                  console.log(next_page);
            console.log("next_page");
                
                $('.pagination_uom_li').removeClass('active');
                $('.' + next_page).addClass('active');
                pagination_uom_list_append_next_product(current_page, total_count, last_child_value, customers_per_page);
                if($('#page_value').val() =="sales_uom_page"){
                $('.current_page').val(next_page);
                $('.inventory_form').submit();
                }
            }else {
                //hide_overlay();
            }
        } else {
           
            //   salesPersonData(customer_group_name,customer_name,current_active_page,customers_per_page);
            $('.pagination_uom_li').removeClass('active');
            $(this).addClass('active');
            console.log("current_active_page");
            console.log(current_active_page);
            if($('#page_value').val() =="sales_uom_page"){
                $('.current_page').val(current_active_page);
                console.log($('.current_page').val());
                $('.inventory_form').submit();
            }
        }
    });
    
    function pagination_uom_list_append_previous_product(page_number, total_customers_list, first_count, customers_per_page) {
        let no_of_pages = total_customers_list;
        if (total_customers_list) {
            if (parseInt(page_number) == first_count) {
                var start = parseInt(first_count) - 2;
                if (start == 0) {
                    start = 1;
                }
                var end = start + 4;
                if (end > no_of_pages) {
                    end = no_of_pages;
                }
                $('.pagination .pagination_list').html('');
                var i;
                $('.pagination .pagination_list').append("<li class='previous pagination_uom_li'><a>Previous</a></li>");
                for (i = start; i <= end; i++) {
                    $('.pagination .pagination_list').append("<li class='" + i + " pagination_uom_li page_no' id='" + i + "'><a>" + i + "</a></li>");
                }
                $('.pagination .pagination_list').append("<li class='next pagination_uom_li'><a>Next</a></li>");
                $('.pagination_uom_li').removeClass('active');
                $("." + page_number).addClass('active');
            }
        }
    }
    
    function pagination_uom_list_append_next_product(page_number, total_customers_list, last_count, customers_per_page) {
        let no_of_pages = total_customers_list;
        if (total_customers_list) {
          //  alert(page_number);
            console.log(last_count);
            if (parseInt(page_number) == last_count) {
                console.log("if");
               // var start = parseInt(last_count) - 2;
                  var start = last_count;
                //console.log(start);
               // console.log("start");
                var end = parseInt(last_count) + 4;
                //console.log(end);
                if (end > no_of_pages) {
                    end = no_of_pages;
                }
               // console.log(end);
               // console.log("end");
                $('.pagination .pagination_list').html('');
                var i;
                $('.pagination .pagination_list').append("<li class='previous pagination_uom_li'><a>Previous</a></li>");
                for (i = start; i <= end; i++) {
                    $('.pagination .pagination_list').append("<li class='" + i + " pagination_uom_li page_no' id='" + i + "'><a>" + i + "</a></li>");
                }
                $('.pagination .pagination_uom_li').append("<li class='next pagination_uom_li'><a>Next</a></li>");
                $('.pagination_uom_li').removeClass('active');
                console.log("page_number");
                console.log(page_number);
                $("." + page_number +"").addClass('active');
               // page_number = page_number + 1;
                
            }
        }
    }
    
    
    






	
	//uom update


});