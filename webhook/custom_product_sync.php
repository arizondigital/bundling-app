<?php
ini_set('max_execution_time', 0);
ini_set("memory_limit", "-1");
ini_set('display_errors', "1");
error_reporting(E_ALL);
require_once '/var/www/html/Furniture/Bundling_App/vendor/autoload.php';
require ('/var/www/html/Furniture/Bundling_App/include/config.php');
require_once '/var/www/html/Furniture/Bundling_App/include/webhook_sp_functions.php';
require_once '/var/www/html/Furniture/Bundling_App/include/bc_functions.php';
require_once '/var/www/html/Furniture/Bundling_App/controllers/AdminController.php';

use Bigcommerce\Api\Client as Bigcommerce;
use Firebase\JWT\JWT;
use Guzzle\Http\Client;
use Handlebars\Handlebars;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// Load from .env file
$dotenv = new Dotenv\Dotenv('/var/www/html/Furniture/Bundling_App');
$dotenv->load();
//Object Creation for SP call functions
$pdo = new WebhookModel();
$timestamp = new DateTime();

$date_type = $_GET["type"];
echo "date_type";
echo "<br>";
echo $date_type;
echo "<br>";
$store = $_GET["store_hash"];
//echo "store";
//echo "<br>";
//echo $store;
//echo "<br>";
$from_date = $_GET["from_date"];
//echo "from_date";
//echo "<br>";
//echo $from_date;
//echo "<br>";
$to_date = $_GET["to_date"];
//echo "to_date";
//echo "<br>";
///echo $to_date;
//echo "<br>";
$main_product_id = $_GET["product_id"];
//echo "main_product_id";
//echo "<br>";
//echo $main_product_id;
//echo "<br>";
$page_type = $_GET["page_type"];
//echo "page_type";
//echo "<br>";
//echo $page_type;
//echo "<br>";

$back_date;
$from_date;
if ($date_type == "3_hour")
{
    $back_date = date('Y-m-d H:i:s', strtotime('-4 hours'));
}
else if ($date_type == "one_hour")
{
    $back_date = date('Y-m-d H:i:s', strtotime('-1 hours'));
}
else if ($date_type == "one_day")
{
    $back_date = date('Y-m-d H:i:s', strtotime('-1 day'));
}
else if ($date_type == "custom")
{
    $from_date = date('Y-m-d H:i:s', strtotime($from_date));
    echo "to_date"; echo $to_date = date('Y-m-d H:i:s', strtotime($to_date .' + 1 days'));

    $from_date = new DateTime($from_date);
    $formatted_date_from_date = $from_date->format(DateTime::ATOM); // Updated ISO8601
    $formatted_date_from_date_1 = substr($formatted_date_from_date, 0, 19);
    $formatted_date_from_date_2 = substr($formatted_date_from_date, 20, 25);
    $from_date = $formatted_date_from_date_1 . "-" . $formatted_date_from_date_2;

    $to_date = new DateTime($to_date);
    $formatted_date_from_date = $to_date->format(DateTime::ATOM); // Updated ISO8601
    $formatted_date_from_date_1 = substr($formatted_date_from_date, 0, 19);
    $formatted_date_from_date_2 = substr($formatted_date_from_date, 20, 25);
    $to_date = $formatted_date_from_date_1 . "-" . $formatted_date_from_date_2;
}
//$product_sync_date= 2000;
//$back_date = date('Y-m-d H:i:s', strtotime('-'.$product_sync_date.' days'));
//echo $back_date;
//echo "back_date";
$datetime = new DateTime($back_date);
$formatted_date = $datetime->format(DateTime::ATOM); // Updated ISO8601
$temp_1 = substr($formatted_date, 0, 19);
$temp_2 = substr($formatted_date, 20, 25);
$date = $temp_1 . "-" . $temp_2;

$admin_details = new AdminController();
$storeHash = $store;
$stores = array();
$stores = $admin_details->getStore($storeHash);

//echo "stores<pre>";
//print_r($stores);
//echo "</pre>";
foreach ($stores as $store)
{
    echo $store_hash = $store['store_hash'];
    echo "<br>";
    echo $auth_token = $store['hash_token'];
    echo "<br>";
    echo $store_id = $store['app_installed_store_id'];
    echo "<br>";
    $client_id = clientId();
    //echo "store_details<pre>";echo "test";echo $client;die;
    //echo $client_id = $store['client_id'];echo "<br>";
    //$client_id = clientId();
    //	$client_id = 'k8uffoed58f2ww7q7imrn2v61rnnavx';
    $header = array(
        'X-Auth-Client: ' . $client_id,
        'X-Auth-Token: ' . $auth_token,
        'Accept: application/json',
        'Content-Type: application/json'
    );
    print_r($header);

    //configureBCApi($store_hash);

if($page_type == 'list page'){
    //$product_count = Bigcommerce::getProductsCount();
    if ($date_type != "custom")
    {
        $product_count = custom_getProductCount($store_hash, $header, $date, null);

		insertLog("-------Bundle Kit Sync product count".$product_count." started-------");
    }
    else if ($date_type == "custom")
    {
        $product_count = custom_getProductCount($store_hash, $header, $from_date, $to_date);
		insertLog("-------Bundle Kit Sync product count".$product_count." started-------");
    }

    echo "productcount";
    echo $product_count;
    echo "<br>";
    if ($product_count % 250 != 0)
    {
        $page_limit = (int)($product_count / 250) + 1;
    }
    else
    {
        $page_limit = (int)$product_count / 250;
    }
    echo $product_count;
	if($product_count != 0){
		$input_manage_cron = array();
		$input_manage_cron["sync_log_id"] = null;
		$input_manage_cron["sync_type"] = "manual";
		$input_manage_cron["log_status"] = "inprogress";
		$input_manage_cron["sync_time_limit"] = $date_type;
		$input_manage_cron["app_installed_store_id"] = $store_id;
		//app_installed_store_id
		echo "input_manage_cron<pre>";print_r($input_manage_cron);echo "</pre>";
		$insert_manage_cron_detail = $pdo->sp_manage_cron_details($input_manage_cron);
		echo "insert_manage_cron_detail<pre>";print_r($insert_manage_cron_detail);echo "</pre>";
		echo $cron_details_id = $insert_manage_cron_detail[0]["param_sync_log_id"];

		for($page_number=1; $page_number<=$page_limit;$page_number++){
			//if($page_number==2){
			//	echo "end_page";die;
			//}
			echo "Called once123";
			$bc_product_details = array();
			if($date_type != "custom"){
				echo "inside_if";
			$bc_product_details = custom_get_All_Product_Details_BC($store_hash,$header,$date,null,$page_number);
				foreach($bc_product_details->data as $individual_product){
					echo "inside";
				
					$bc_product_id = '';
					$bc_product_id = $individual_product->id;
					insertLog("-------Bundle Kit Sync product id".$bc_product_id." started-------");
					insertLog("-------Bundle Kit Sync type".$date_type." started-------");
					Product_sync($bc_product_id,$store_hash,$header,$pdo,$store_id);
					insertLog("-------Bundle Kit Sync product id".$bc_product_id." end-------");
				}
			}else if ($date_type == "custom"){
				echo "inside_else";
			$bc_product_details = custom_get_All_Product_Details_BC($store_hash,$header,$from_date,$to_date,$page_number);
			//echo "bc_product_details<pre>";print_r($bc_product_details);echo "</pre>";die;
				foreach($bc_product_details->data as $individual_product){
					echo "inside_else_inside";
				
					$bc_product_id = '';
					echo $bc_product_id = $individual_product->id;
					insertLog("-------Bundle Kit Sync product id".$bc_product_id." started-------");
					insertLog("-------Bundle Kit Sync type".$date_type." started-------");
					Product_sync($bc_product_id,$store_hash,$header,$pdo,$store_id);
					insertLog("-------Bundle Kit Sync product id".$bc_product_id." end-------");
				}

			}
		}

		$input_manage_cron_update = array();
		$input_manage_cron_update["sync_log_id"] = $cron_details_id;
		$input_manage_cron_update["sync_type"] = "manual";
		$input_manage_cron_update["log_status"] = "completed";
		$input_manage_cron_update["sync_time_limit"] = $date_type;
		$input_manage_cron_update["app_installed_store_id"] = $store_id;
		//app_installed_store_id
		echo "input_manage_cron_update<pre>";print_r($input_manage_cron_update);echo "</pre>";
		$insert_manage_cron_detail = $pdo->sp_manage_cron_details($input_manage_cron_update);
		echo "insert_manage_cron_detail<pre>";print_r($insert_manage_cron_detail);echo "</pre>";
		//echo $cron_details_id = $insert_manage_cron_detail[0]["param_sync_log_id"];die;
	}else{
		echo "no products";
	}
}else if($page_type == 'edit page'){
	Product_sync($main_product_id,$store_hash,$header,$pdo,$store_id);
}

}
function configureBCApi($storeHash)
{
    Bigcommerce::configure(array(
        'client_id' => clientId() ,
        'auth_token' => getAuthToken($storeHash) ,
        'store_hash' => $storeHash
    ));
}
function clientId()
{
    $clientId = getenv('BC_CLIENT_ID');
    return $clientId ? : '';
}
function getAuthToken($storeHash)
{
    $admin_details = new AdminController();
    $result = $admin_details->getStore($storeHash,$header);
    return $result[0]['hash_token'];
}
function Product_sync($bc_product_id,$store_hash,$header,$pdo,$store_id){
	//echo "Product_sync";die;
	    // Get Product Details from BC
    $bc_product_detail = getProductDetailsBC($store_hash,$header,$bc_product_id);
    //$bc_product_detail = Bigcommerce::getProduct($bc_product_id);
    
    //echo"bc_product_detail<pre>";
    //print_r($bc_product_detail);
    //echo "</pre>";die;
    $product_categories = '';
    foreach($bc_product_detail->data->categories as $category_id){
    if($product_categories == ''){
    	$product_categories = $category_id;
    }else{
    	$product_categories .= ','.$category_id;
    }
    }
    
    $bc_brand_detail = array();
    if($bc_product_detail->data->brand_id != 0){
    $bc_brand_detail = getBrandDetailsBC($store_hash,$header,$bc_product_detail->data->brand_id);
    //$bc_brand_detail = Bigcommerce::getBrand($bc_product_detail->data->brand_id);	
    }
    
    $bc_optionset_detail = array();
    if($bc_product_detail->data->option_set_id != 0 && $bc_product_detail->data->option_set_id != ''){
    $bc_optionset_detail = getProductOptionsetDetailsBC($store_hash,$header,$bc_product_detail->data->option_set_id);
    //$bc_optionset_detail = Bigcommerce::getOptionSet($bc_product_detail->data->option_set_id);
    }
    
    if($bc_product_detail->data->id == $bc_product_id){
    
    // Insert Product Details to DB
    $insert_data = array();
    $insert_data['param_bc_product_id'] = $bc_product_detail->data->id;
    $insert_data['param_name'] = $bc_product_detail->data->name;
    $insert_data['param_type'] = $bc_product_detail->data->type;
    $insert_data['param_sku'] = ($bc_product_detail->data->sku != '')?$bc_product_detail->data->sku:null;
    $insert_data['param_description'] = ($bc_product_detail->data->description != '')?$bc_product_detail->data->description:null;
    $insert_data['param_weight'] = ($bc_product_detail->data->weight != '')?$bc_product_detail->data->weight:0;
    $insert_data['param_width'] = ($bc_product_detail->data->width != '')?$bc_product_detail->data->width:null;
    $insert_data['param_depth'] = ($bc_product_detail->data->depth != '')?$bc_product_detail->data->depth:null;
    $insert_data['param_height'] = ($bc_product_detail->data->height != '')?$bc_product_detail->data->height:null;
    $insert_data['param_price'] = $bc_product_detail->data->price;
    $insert_data['param_cost_price'] = ($bc_product_detail->data->cost_price !='')?$bc_product_detail->data->cost_price:null;
    $insert_data['param_retail_price'] = ($bc_product_detail->data->retail_price != '')?$bc_product_detail->data->retail_price:null;
    $insert_data['param_sale_price'] = ($bc_product_detail->data->sale_price !='')?$bc_product_detail->data->sale_price:null;
    $insert_data['param_tax_class_id'] = ($bc_product_detail->data->tax_class_id != '')?$bc_product_detail->data->tax_class_id:null;
    $insert_data['param_product_tax_code'] = ($bc_product_detail->data->product_tax_code != '')?$bc_product_detail->data->product_tax_code:null;
    $insert_data['param_bc_brand_id'] = ($bc_product_detail->data->brand_id != '')?$bc_product_detail->data->brand_id:null;
    $insert_data['param_inventory_level'] = ($bc_product_detail->data->inventory_level != '')?$bc_product_detail->data->inventory_level:null;
    $insert_data['param_inventory_warning_level'] = ($bc_product_detail->data->inventory_warning_level != '')?$bc_product_detail->data->inventory_warning_level:null;
    $insert_data['param_inventory_tracking'] = ($bc_product_detail->data->inventory_tracking != '')?$bc_product_detail->data->inventory_tracking:null;
    $insert_data['param_fixed_cost_shipping_price'] = ($bc_product_detail->data->fixed_cost_shipping_price != '')?$bc_product_detail->data->fixed_cost_shipping_price:null;
    $insert_data['param_is_free_shipping'] = ($bc_product_detail->data->is_free_shipping != '')?'t':'f';
    $insert_data['param_is_visible'] = ($bc_product_detail->data->is_visible != '')?'t':'f';
    $insert_data['param_is_featured'] = ($bc_product_detail->data->is_featured != '')?'t':'f';
    $insert_data['param_warranty'] = ($bc_product_detail->data->warranty != '')?$bc_product_detail->data->warranty:null;
    $insert_data['param_bin_picking_number'] = ($bc_product_detail->data->bin_picking_number != '')?$bc_product_detail->data->bin_picking_number:null;
    $insert_data['param_layout_file'] = ($bc_product_detail->data->layout_file != '')?$bc_product_detail->data->layout_file:null;
    $insert_data['param_upc'] = ($bc_product_detail->data->upc != '')?$bc_product_detail->data->upc:null;
    $insert_data['param_search_keywords'] =($bc_product_detail->data->search_keywords != '')?$bc_product_detail->data->search_keywords:null;
    $insert_data['param_availability'] = ($bc_product_detail->data->availability != '')?$bc_product_detail->data->availability:null;
    if($update_availablity_text != ''){
    	$insert_data['param_availability_description'] = $update_availablity_text;
    }else{
    	$insert_data['param_availability_description'] = ($bc_product_detail->data->availability_description != '')?$bc_product_detail->data->availability_description:null;
    }
    $insert_data['param_gift_wrapping_options_type'] = null;
    $insert_data['param_gift_wrapping_options_list'] = null;
    $insert_data['param_sort_order'] = ($bc_product_detail->data->sort_order != '')?$bc_product_detail->data->sort_order:null;
    if($update_product_condition != ''){
    	$insert_data['param_condition'] = $update_product_condition;
    }else{
    	$insert_data['param_condition'] = ($bc_product_detail->data->condition != '')?$bc_product_detail->data->condition:null;
    }
    $insert_data['param_is_condition_shown'] = ($bc_product_detail->data->is_condition_shown != '')?'t':'f';
    $insert_data['param_order_quantity_minimum'] = ($bc_product_detail->data->order_quantity_minimum != '')?$bc_product_detail->data->order_quantity_minimum:null;
    $insert_data['param_order_quantity_maximum'] = ($bc_product_detail->data->order_quantity_maximum != '')?$bc_product_detail->data->order_quantity_maximum:null;
    $insert_data['param_page_title'] = null;
    $insert_data['param_meta_keywords'] = null;
    $insert_data['param_meta_description'] = ($bc_product_detail->data->meta_description != '')?$bc_product_detail->data->meta_description:null;
    $insert_data['param_view_count'] = ($bc_product_detail->data->view_count != '')?$bc_product_detail->data->view_count:null;
    $insert_data['param_preorder_release_date'] = ($bc_product_detail->data->preorder_release_date != '')?$bc_product_detail->data->preorder_release_date:null;
    $insert_data['param_preorder_message'] = ($bc_product_detail->data->preorder_message != '')?$bc_product_detail->data->preorder_message:null;
    $insert_data['param_is_preorder_only'] = ($bc_product_detail->data->is_preorder_only !='')?'t':'f';
    $insert_data['param_is_price_hidden'] = ($bc_product_detail->data->is_price_hidden != '')?'t':'f';
    $insert_data['param_price_hidden_label'] = ($bc_product_detail->data->price_hidden_label != '')?$bc_product_detail->data->price_hidden_label:null;
    $insert_data['param_custom_url'] = ($bc_product_detail->data->custom_url->url != "")?$bc_product_detail->data->custom_url->url:null;
    $insert_data['param_is_customized'] = ($bc_product_detail->data->custom_url->is_customized != '')?'t':'f';
    $insert_data['param_open_graph_type'] = null;
    $insert_data['param_open_graph_title'] = null;
    $insert_data['param_open_graph_description'] = null;
    $insert_data['param_open_graph_use_meta_description'] = ($bc_product_detail->data->open_graph_use_meta_description != '')?'t':'f';
    $insert_data['param_open_graph_use_product_name'] = ($bc_product_detail->data->open_graph_use_product_name != '')?'t':'f';
    $insert_data['param_open_graph_use_image'] = ($bc_product_detail->data->open_graph_use_image != "")?'t':'f';
    $insert_data['param_brand_name'] = ($bc_brand_detail->data->name != '')?$bc_brand_detail->data->name:null;
    $insert_data['param_gtin'] = ($bc_product_detail->data->gtin != '' )?$bc_product_detail->data->gtin:null;
    $insert_data['param_mpn'] = ($bc_product_detail->data->mpn != '')?$bc_product_detail->data->mpn:null;
    $insert_data['param_calculated_price'] = ($bc_product_detail->data->calculated_price != '')?$bc_product_detail->data->calculated_price:null;
    $insert_data['param_reviews_rating_sum'] = null;
    $insert_data['param_reviews_count'] = ($bc_product_detail->data->reviews_count != '')?$bc_product_detail->data->reviews_count:null;
    $insert_data['param_total_sold'] = ($bc_product_detail->data->total_sold != '')?$bc_product_detail->data->total_sold:null;
    $insert_data['param_bc_option_set_id'] = ($bc_product_detail->data->option_set_id != '')?$bc_product_detail->data->option_set_id:null;
    $insert_data['param_option_set_name'] = ($bc_optionset_detail->data->name != '')?$bc_optionset_detail->data->name:null;
    $insert_data['param_inventory_available'] = ($bc_product_detail->data->availability != '')?'t':'f';
    $insert_data['param_bc_date_created'] = ($bc_product_detail->data->date_created != '')?strtotime($bc_product_detail->data->date_created):null;
    $insert_data['param_bc_date_modified'] = ($bc_product_detail->data->date_modified != '')?strtotime($bc_product_detail->data->date_modified):null;
    $insert_data['param_is_deleted'] = 'f';
    $insert_data['param_bc_category_id'] = $product_categories;
    $insert_data['param_app_installed_store_id'] = $store_id;
    
    echo "upsertBCProductDetailDB<pre>";
    print_R($insert_data);
    echo "</pre>";
    $insert_bc_product_detail = $pdo->upsertBCProductDetailDB($insert_data);
    echo "<pre>";
    print_R($insert_bc_product_detail);
    echo "</pre>";
    }
    $uom_product_check_data=[];
    $insert_uom_product_detail=[];
    $uom_product_insert_data=[];
    $manage_uom_product_detail=[];
    $uom_product_check_data['page_offset'] = null;
    $uom_product_check_data['page_limit'] = null;
    $uom_product_check_data['product_name'] = null;
    $uom_product_check_data['is_uom_override'] = null;
    $uom_product_check_data['product_id']=$bc_product_id;
    $uom_product_check_data['sku']=($bc_product_detail->data->sku != '')?$bc_product_detail->data->sku:null;
    $uom_product_check_data['app_installed_store_id']= $store_id;
    $uom_product_check_data['is_bundle_override'] = null;
    $uom_product_check_data['is_bundle_available'] = null;
	$uom_product_check_data['measurement_value'] = null;
    echo "insert_uom_product_detail<pre>";
    print_R($uom_product_check_data);
    echo "</pre>";
    $insert_uom_product_detail = $pdo->getUomProducts($uom_product_check_data);
    echo "insert_uom_product_detail<pre>";
    print_R($insert_uom_product_detail);
    echo "</pre>";
    print_R(count($insert_uom_product_detail));
    if(count($insert_uom_product_detail)==0){
        $uom_product_insert_data['page_offset'] = null;
        $uom_product_insert_data['page_limit'] = null;
        $uom_product_insert_data['product_id']=$bc_product_id;
        $uom_product_insert_data['sku']=($bc_product_detail->data->sku != '')?$bc_product_detail->data->sku:null;
        $uom_product_insert_data['base_uom'] = null;
        $uom_product_insert_data['sales_uom'] = null;
        $uom_product_insert_data['is_uom_override'] = false;
        $uom_product_insert_data['app_installed_store_id']= $store_id;
        $uom_product_insert_data['is_bundle_override'] = false;
        $uom_product_insert_data['is_bundle_available'] = false;
        $uom_product_insert_data['bundle_qty'] = null;
        $uom_product_insert_data['bundle_broken_percentage'] = null;
		$uom_product_insert_data['measurement_value'] = null;
        echo "manage_uom_product_detail<pre>";
        print_R($uom_product_insert_data);
        echo "</pre>";
        $manage_uom_product_detail = $pdo->manageUomProducts($uom_product_insert_data);
        echo "manage_uom_product_detail<pre>";
        print_R($manage_uom_product_detail);
        echo "</pre>";
    }/*else{
        $uom_product_insert_data['page_offset'] = null;
        $uom_product_insert_data['page_limit'] = null;
        $uom_product_insert_data['product_id']=$insert_uom_product_detail[0]->destination_product_id;
        $uom_product_insert_data['sku']=$insert_uom_product_detail[0]->sku;
        $uom_product_insert_data['base_uom'] = isset($insert_uom_product_detail[0]->base_uom)?$insert_uom_product_detail[0]->base_uom:null;
        $uom_product_insert_data['sales_uom'] = isset($insert_uom_product_detail[0]->sales_uom)?$insert_uom_product_detail[0]->sales_uom:null;
        $uom_product_insert_data['is_uom_override'] = isset($insert_uom_product_detail[0]->is_uom_override)?$insert_uom_product_detail[0]->is_uom_override:false;
        $uom_product_insert_data['app_installed_store_id']= $store_id;
        $uom_product_insert_data['is_bundle_override'] =isset($insert_uom_product_detail[0]->is_bundle_override)?$insert_uom_product_detail[0]->is_bundle_override:false;
        $uom_product_insert_data['is_bundle_available'] = isset($insert_uom_product_detail[0]->is_bundle_available)?$insert_uom_product_detail[0]->is_bundle_available:false;
        $uom_product_insert_data['bundle_qty'] = isset($insert_uom_product_detail[0]->bundle_qty)?$insert_uom_product_detail[0]->bundle_qty:null;
        $uom_product_insert_data['bundle_broken_percentage'] = isset($insert_uom_product_detail[0]->bundle_broken_percentage)?$insert_uom_product_detail[0]->bundle_broken_percentage:null;
        echo "manage_uom_product_detail<pre>";
        print_R($uom_product_insert_data);
        echo "</pre>";
        $manage_uom_product_detail = $pdo->manageUomProducts($uom_product_insert_data);
        echo "manage_uom_product_detail<pre>";
        print_R($manage_uom_product_detail);
        echo "</pre>";
    }   */ 
    // Get Product Image Details
    $bc_product_image_detail = array();
    $bc_product_image_detail = getProductImageDetailsBC($store_hash,$header,$bc_product_id);
    //$bc_product_image_detail = $bc_product_detail->data->images;
    $image_json_array = array();
    if(count($bc_product_image_detail->data)>0){
		foreach($bc_product_image_detail->data as $individual_image_detail){
			$individual_image_data_array = array();
			$individual_image_data_array['bc_product_option_sku_id'] = null;
			$individual_image_data_array['bc_product_option_rule_id'] = null;
			$individual_image_data_array['image_url'] = ($individual_image_detail->image_file != '')?$individual_image_detail->image_file:null;
			$individual_image_data_array['is_main_image'] = ($individual_image_detail->is_thumbnail != '')?'t':'f';
			$individual_image_data_array['bc_product_image_id'] = $individual_image_detail->id;
			$individual_image_data_array['image_file'] = ($individual_image_detail->image_file != '')?$individual_image_detail->image_file:null;
			$individual_image_data_array['url_zoom'] = ($individual_image_detail->url_zoom !='')?$individual_image_detail->url_zoom:null;
			$individual_image_data_array['url_standard'] = ($individual_image_detail->url_standard != '')?$individual_image_detail->url_standard:null;
			$individual_image_data_array['url_thumbnail'] = ($individual_image_detail->url_thumbnail != '')?$individual_image_detail->url_thumbnail:null;
			$individual_image_data_array['url_tiny'] = ($individual_image_detail->url_tiny != '')?$individual_image_detail->url_tiny:null;
			$individual_image_data_array['is_thumbnail'] = ($individual_image_detail->is_thumbnail != '')?'t':'f';
			$individual_image_data_array['sort_order'] = ($individual_image_detail->sort_order != '')?$individual_image_detail->sort_order:null;
			$individual_image_data_array['description'] = ($individual_image_detail->description != '')?$individual_image_detail->description:null;
			$individual_image_data_array['date_modified'] = strtotime($individual_image_detail->date_modified);
			$image_json_array['image_details'][] = $individual_image_data_array;
		}
    }
    
    $product_image_insert_data = array();
    $product_image_insert_data['param_bc_product_id'] = $bc_product_id;
    $product_image_insert_data['param_product_image_details'] = (count($image_json_array) > 0)?json_encode($image_json_array):null;
    $product_image_insert_data['param_app_installed_store_id'] = $store_id;
    
    //echo "upsertBCProductImageDetailDB<pre>";
    //print_R($product_image_insert_data);
    //echo "</pre>";
    $insert_bc_product_image_detail = $pdo->upsertBCProductImageDetailDB($product_image_insert_data);
    ////	echo "<pre>";
    //print_R($insert_bc_product_image_detail);
    //echo "</pre>";
    
    // Save Product Custom Field Details
    $bc_product_custom_detail = array();
    $bc_product_custom_detail = getProductCustomFieldDetailsBC($store_hash,$header,$bc_product_id);
    echo "<pre>";
    print_R($bc_product_custom_detail);
    echo "</pre>";
    //$bc_product_custom_detail = $bc_product_detail->data->custom_fields;
    $custom_field_json_array = array();
    if(count($bc_product_custom_detail->data) > 0){
		foreach($bc_product_custom_detail->data as $individual_custom_field){
			$individual_customfeild_data_array = array();
			$individual_customfeild_data_array['name'] = $individual_custom_field->name;
				if($individual_custom_field->name == 'Collection'){
					echo "founded";
				$bundle_kit_input = array("destination_product_id"=>$bc_product_id,"skus"=>$individual_custom_field->value,"app_installed_store_id"=>$store_id);
				echo "<pre>";print_r($bundle_kit_input);echo "</pre>";
				$bundle_kit_output = $pdo->sp_manage_product_bundle_migration($bundle_kit_input);
				echo "bundle_kit_output<pre>";print_r($bundle_kit_output);echo "</pre>";
				}
			$individual_customfeild_data_array['value'] = $individual_custom_field->value;
			$individual_customfeild_data_array['bc_product_custom_fields_id'] = $individual_custom_field->id;
			$custom_field_json_array['custom_details'][] = $individual_customfeild_data_array;
		}
    }
    $custom_field_insert_data = array();
    $custom_field_insert_data['param_bc_product_id'] = $bc_product_id;
    $custom_field_insert_data['param_product_custom_field_details'] = (count($custom_field_json_array) > 0)?json_encode($custom_field_json_array):null;
    $custom_field_insert_data['param_app_installed_store_id'] = $store_id;
    echo "<pre>";
    print_R($custom_field_insert_data);
    echo "</pre>";
    $insert_bc_product_custom_field_detail = $pdo->upsertBCProductCustomFieldDetailDB($custom_field_insert_data);
    echo "<pre>";
    print_R($insert_bc_product_custom_field_detail);
    echo "</pre>";
    
    // Save Product Option Details
    $bc_option_detail = array();
    $option_id_details_array = array();
    if($bc_product_detail->data->option_set_id != 0 && $bc_product_detail->data->option_set_id != ''){
    $bc_option_detail = getProductOptionDetailsBC($store_hash,$header,$bc_product_id);
    //$bc_option_detail = $bc_product_detail->data->options;
    echo "bc_option_detail<pre>";
    print_r($bc_option_detail);
    echo "</pre>";
    $option_detail_json_array = array();
    foreach($bc_option_detail->data as $individual_option_details){
    
    $option_id_details_array[$individual_option_details->id] = $individual_option_details->option_id;
    
    $bc_option_value_detail=array();
    $bc_option_value_detail=$individual_option_details->option_values;
    
    //$bc_option_value_detail=getProductOptionValueDetailsBC($store_hash,$header,$individual_option_details->option_id);
    //$app_bc_option_value_detail = Bigcommerce::getOption($individual_option_details->option_id);
    //$bc_option_value_detail = $app_bc_option_value_detail->values;				
    $option_value_json_array = array();
    if(count($bc_option_value_detail)>0){
    	foreach($bc_option_value_detail as $individual_option_values){
    		$individual_option_value_array = array();
    		$individual_option_value_array['bc_option_value_id'] = $individual_option_values->id;
    		$individual_option_value_array['is_default'] = $individual_option_values->is_default;
    		$individual_option_value_array['label'] = $individual_option_values->label;
    	
    		$individual_option_value_array['value_data'] = $individual_option_values->value_data;
    		$individual_option_value_array['sort_order'] = ($individual_option_values->sort_order != '' )?$individual_option_values->sort_order:null;
    		$option_value_json_array['option_value_details'][] = $individual_option_value_array;
    	}
    }
    
    $option_value_insert_data = array();
    $option_value_insert_data['param_bc_product_id'] = $bc_product_id;
    $option_value_insert_data['param_bc_option_id'] = $individual_option_details->id;
    $option_value_insert_data['param_display_name'] = $individual_option_details->display_name;
    $option_value_insert_data['param_type'] = null;
    $option_value_insert_data['param_sort_order'] = ($individual_option_details->sort_order != '')?$individual_option_details->sort_order:null;
    $option_value_insert_data['param_is_active'] = 't';
    $option_value_insert_data['param_is_deleted'] = 'f';
    $option_value_insert_data['param_bc_option_assign_id'] = null;
    $option_value_insert_data['param_option_value_details'] = json_encode($option_value_json_array);
    $option_value_insert_data['param_app_installed_store_id'] = $store_id;
    
    echo "option_value_insert_data<pre>";
    print_R($option_value_insert_data);
    echo "</pre>";
    $insert_bc_product_option_value_detail = $pdo->upsertBCProductOptionDetailDB($option_value_insert_data);
    echo "insert_bc_product_option_value_detail<pre>";
    print_R($insert_bc_product_option_value_detail);
    echo "</pre>";
    }
    
    //Modifires Option
    
    $bc_modifier_option_detail = getProductModifierDetailsBC($store_hash,$header,$bc_product_id);
    ///echo '<pre>';
    //print_r($bc_modifier_option_detail);
    //echo '</pre>';die;
    
    $option_detail_json_array = array();
    foreach($bc_modifier_option_detail->data as $individual_option_details){
    
    $bc_option_value_detail=array();
    $bc_option_value_detail=$individual_option_details->option_values;
    			
    $option_value_json_array = array();
    if(count($bc_option_value_detail)>0){
    	foreach($bc_option_value_detail as $individual_option_values){
    		$individual_option_value_array = array();
    		$individual_option_value_array['bc_option_value_id'] = $individual_option_values->id;
    		$individual_option_value_array['is_default'] = $individual_option_values->is_default;
    		$individual_option_value_array['label'] = $individual_option_values->label;
    		$individual_option_value_array['value_data'] = $individual_option_values->value_data;
    		$individual_option_value_array['sort_order'] = ($individual_option_values->sort_order != '' )?$individual_option_values->sort_order:null;
    		$option_value_json_array['option_value_details'][] = $individual_option_value_array;
    	}
    }
    
    $option_value_insert_data = array();
    $option_value_insert_data['param_bc_product_id'] = $bc_product_id;
    $option_value_insert_data['param_bc_option_id'] = $individual_option_details->id;
    $option_value_insert_data['param_display_name'] = $individual_option_details->display_name;
    $option_value_insert_data['param_type'] = null;
    $option_value_insert_data['param_sort_order'] = ($individual_option_details->sort_order != '')?$individual_option_details->sort_order:null;
    $option_value_insert_data['param_is_active'] = 't';
    $option_value_insert_data['param_is_deleted'] = 'f';
    $option_value_insert_data['param_bc_option_assign_id'] = null;
    $option_value_insert_data['param_option_value_details'] = json_encode($option_value_json_array);
    $option_value_insert_data['param_app_installed_store_id'] = $store_id;
    
    echo "option_value_insert_data_check<pre>";
    print_R($option_value_insert_data);
    echo "</pre>";
    $insert_bc_product_option_value_detail = $pdo->upsertBCProductOptionDetailDB($option_value_insert_data);
    echo "<pre>";
    print_R($insert_bc_product_option_value_detail);
    echo "</pre>";
    }
    
    // Save Product SKU details
    $bc_sku_detail=array();
    $bc_sku_detail=getProductSkuDetailsBC($store_hash,$header,$bc_product_id);
    //echo '<pre>';
    //print_r($bc_sku_detail);
    //echo '</pre>';die;
    //$bc_sku_detail = $bc_product_detail->data->skus;
    
    if(count($bc_sku_detail->data)>0){
    	foreach($bc_sku_detail->data as $individual_sku_detail){
    
    		if(count($individual_sku_detail->option_values)>0){
    			$sku_option_detail_json_array = array();
    			foreach($individual_sku_detail->option_values as $individual_sku_option){
    				$product_sku_options=array();
    				$product_sku_options['option_name']=($individual_sku_option->option_display_name != '')?$individual_sku_option->option_display_name:null;
    				$product_sku_options['bc_product_option_id']=($individual_sku_option->option_id != '')?$individual_sku_option->option_id:null;
    				$product_sku_options['bc_product_option_value_id']=($individual_sku_option->id != '')?$individual_sku_option->id:null;
    				$product_sku_options['option_value']=($individual_sku_option->label != '')?$individual_sku_option->label:null;			
    				$sku_option_detail_json_array['product_option_sku_details'][] = $product_sku_options;
    			}
    		}
    		$product_sku_insert_data=array();
    		$product_sku_insert_data['param_bc_product_id']=$bc_product_id;
    		$product_sku_insert_data['param_bc_sku_id']=$individual_sku_detail->id;
    		$product_sku_insert_data['param_sku']=$individual_sku_detail->sku;
    		$product_sku_insert_data['param_cost_price']=($individual_sku_detail->cost_price != '')?$individual_sku_detail->cost_price:null;
    		$product_sku_insert_data['param_price']=($individual_sku_detail->price !='')?$bc_product_detail->data->price:null;
    		$product_sku_insert_data['param_sale_price']=($individual_sku_detail->sale_price != '')?$individual_sku_detail->sale_price:null;
    		$product_sku_insert_data['param_retail_price']=($individual_sku_detail->retail_price != '')?$individual_sku_detail->retail_price:null;
    		$product_sku_insert_data['param_weight']=($individual_sku_detail->weight != '')?$individual_sku_detail->weight:null;
    		$product_sku_insert_data['param_width']=($individual_sku_detail->width != '')?$bc_product_detail->data->weight:null;
    		$product_sku_insert_data['param_height']=($individual_sku_detail->height != '')?$individual_sku_detail->height:null;
    		$product_sku_insert_data['param_depth']=($individual_sku_detail->depth != '')?$individual_sku_detail->depth:null;
    		$product_sku_insert_data['param_is_free_shipping']=($individual_sku_detail->is_free_shipping != '')?'t':'f';
    		$product_sku_insert_data['param_fixed_cost_shipping_price']=($individual_sku_detail->fixed_cost_shipping_price != '')?$individual_sku_detail->fixed_cost_shipping_price:null;  
    		$product_sku_insert_data['param_purchasing_disabled']=($individual_sku_detail->purchasing_disabled == '')?'f':'t'; 
    		$product_sku_insert_data['param_purchasing_disabled_message']=($individual_sku_detail->purchasing_disabled_message != '')?$individual_sku_detail->purchasing_disabled_message:null; 
    		$product_sku_insert_data['param_upc']=($individual_sku_detail->upc != '')?$individual_sku_detail->upc:null;  
    		$product_sku_insert_data['param_inventory_warning_level']=($individual_sku_detail->inventory_warning_level != '')?$individual_sku_detail->inventory_warning_level:null; 
    		$product_sku_insert_data['param_bin_picking_number']=($individual_sku_detail->bin_picking_number != '')?$individual_sku_detail->bin_picking_number:null; 
    		$product_sku_insert_data['param_is_deleted']='f'; 
    		$product_sku_insert_data['param_product_option_sku_details']=json_encode($sku_option_detail_json_array); 
    		$product_sku_insert_data['param_inventory_level']=($individual_sku_detail->inventory_level != '')?$individual_sku_detail->inventory_level:null; 
    		$product_sku_insert_data['param_app_installed_store_id']=$store_id; 
    
    		echo "product_sku_insert_data<pre>";
    		print_R($product_sku_insert_data);
    		echo "</pre>";
    		$insert_bc_product_SKU_detail = $pdo->upsertBCProductSKUDetailDB($product_sku_insert_data);
    		echo "<pre>";
    		print_R($insert_bc_product_SKU_detail);
    		echo "</pre>";

            $uom_product_check_data=[];
            $insert_uom_product_detail=[];
            $uom_product_insert_data=[];
            $manage_uom_product_detail=[];
            $uom_product_check_data['page_offset'] = null;
            $uom_product_check_data['page_limit'] = null;
            $uom_product_check_data['product_name'] = null;
            $uom_product_check_data['is_uom_override'] = null;
            $uom_product_check_data['product_id']=$bc_product_id;
    		$uom_product_check_data['sku']=$individual_sku_detail->sku;
            $uom_product_check_data['app_installed_store_id']= $store_id;
            $uom_product_check_data['is_bundle_override'] = null;
            $uom_product_check_data['is_bundle_available'] = null;
			$uom_product_check_data['measurement_value'] = null;
            $insert_uom_product_detail = $pdo->getUomProducts($uom_product_check_data);
            echo "insert_uom_product_detail<pre>";
    		print_R($insert_uom_product_detail);
    		echo "</pre>";
            if(count($insert_uom_product_detail)==0){
                $uom_product_insert_data['page_offset'] = null;
                $uom_product_insert_data['page_limit'] = null;
                $uom_product_insert_data['product_id']=$bc_product_id;
                $uom_product_insert_data['sku']=$individual_sku_detail->sku;
                $uom_product_insert_data['base_uom'] = null;
                $uom_product_insert_data['sales_uom'] = null;
                $uom_product_insert_data['is_uom_override'] = false;
                $uom_product_insert_data['app_installed_store_id']= $store_id;
                $uom_product_insert_data['is_bundle_override'] = false;
                $uom_product_insert_data['is_bundle_available'] = false;
                $uom_product_insert_data['bundle_qty'] = null;
                $uom_product_insert_data['bundle_broken_percentage'] = null;
				$uom_product_insert_data['measurement_value'] = null;
                echo "manage_uom_product_detail<pre>";
                print_R($uom_product_insert_data);
                echo "</pre>";
                $manage_uom_product_detail = $pdo->manageUomProducts($uom_product_insert_data);
                echo "manage_uom_product_detail<pre>";
                print_R($manage_uom_product_detail);
                echo "</pre>";
            }
            
            
            //die;

    			
    	}
    }
    }
    //echo "endstate";die;
}
function insertLog($category_id){
	$log = '['.date("y:m:d h:i:s").']-- ' .$category_id .PHP_EOL;
	echo file_put_contents('/var/www/html/Furniture/Bundling_App/webhook/Log/custom_product_sync_log.txt', $log, FILE_APPEND);
}
?>
