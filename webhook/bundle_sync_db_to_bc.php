<?php
ini_set('max_execution_time', 0);
ini_set("memory_limit","-1");
ini_set('display_errors', "1");
error_reporting(E_ALL);

require_once '/var/www/html/Furniture/Bundling_App/vendor/autoload.php';
require('/var/www/html/Furniture/Bundling_App/include/config.php');
require_once '/var/www/html/Furniture/Bundling_App/include/webhook_sp_functions.php';
require_once '/var/www/html/Furniture/Bundling_App/include/bc_functions.php';
require_once '/var/www/html/Furniture/Bundling_App/controllers/AdminController.php';

use Bigcommerce\Api\Client as Bigcommerce;
use Firebase\JWT\JWT;
use Guzzle\Http\Client;
use Handlebars\Handlebars;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// Load from .env file
$dotenv = new Dotenv\Dotenv('/var/www/html/Furniture/Bundling_App');
$dotenv->load();
$pdo = new WebhookModel();

$admin_details = new AdminController();
$storeHash=null;
$stores = array();
$stores = $admin_details->getStore($storeHash);

echo "<pre>";
print_r($stores);
echo "</pre>";

$insert_data = array("page_offset"=>null,"page_limit"=>null);
$get_product_details = $pdo->sp_get_bundle_kit_list_bc_to_db($insert_data);
echo "<pre>get_product_details";print_r($get_product_details);echo "get_product_details</pre>";
foreach($get_product_details as $data){
	echo "<pre>";print_r($get_product_details);echo "</pre>";
	echo $store_hash = $data['store_hash'];echo "<br>";
	echo $auth_token = $data['hash_token'];echo "<br>";
	echo $store_id = $data['app_installed_store_id'];echo "<br>";
	$client_id = clientId();
	$header = array ('X-Auth-Client: '.$client_id,'X-Auth-Token: '.$auth_token,'Accept: application/json', 'Content-Type: application/json');	
	print_r($header);

	if($data["destination_product_id"] && $data['status'] != "delete"){

			if($data["price"] != 0.00){
					echo $bc_product_id = $data["destination_product_id"];
					$fields["price"]=$data["price"];
					$insert_products = UpdateProductBC($store_hash,$header,$fields,$bc_product_id);
					echo "<pre>";print_r($insert_products);echo "</pre>";
					$destination_product_id = $insert_products->id;
					if($destination_product_id){
						$response = json_encode($insert_products);
						$response_text = "success";
						}else{
							$response = json_encode($insert_products);
						$response_text = "failure";
						}
					$insert_log = array("bc_product_id"=>$destination_product_id,"bundle_kit_id"=>$data["bundlekit_id"],"status"=>$response_text,"response"=>$response,"type"=>"Insert","app_installed_store_id"=>$store_id);
					$insert_log_to_db = $pdo->sp_manage_bundle_bc_log($insert_log);
					echo "<pre>";print_r($insert_log_to_db);echo"</pre>";
			}
		echo $bc_product_id = $data["destination_product_id"];
		$bc_product_custom_detail = getCustomfields($store_hash,$header,$bc_product_id);
		echo "<pre>";print_r($bc_product_custom_detail);echo "</pre>";
			foreach($bc_product_custom_detail as $values){
				echo "<pre>";print_r($values);echo "</pre>";
				$input_data = array("app_installed_store_id"=>$store_id);
				$get_settings_details = $pdo->getSettings($input_data);
				//echo "get_settings_details<pre>";print_r($get_settings_details);echo "<pre>";
				$bundle_custom_filed_name = $get_settings_details[0]['param_bundling_custom_field_name'];
				echo "bundle_custom_filed_name";echo $bundle_custom_filed_name;
				if($values->name == $bundle_custom_filed_name && $data["product_details"] != null){
					echo $custom_field_id = $values->id;
					$fields = array("name"=>$bundle_custom_filed_name,"value"=> $data["product_details"]);
					$bc_product_custom_detail_update = updateCustomfields($store_hash,$header,$fields,$custom_field_id,$bc_product_id);
					echo "old product end";
				}else{
					echo $custom_field_id = $values->id;
					echo "delete product end";
					$bc_product_custom_detail_update = deletecustomfields($store_hash,$header,$custom_field_id,$bc_product_id);
					echo "<pre>";print_r($bc_product_custom_detail_delete);echo "</pre>";
				}
			}
			//if($bc_product_custom_detail_update->id){
			$response = json_encode($bc_product_custom_detail_update);
			$response_text = "success";
			/*}else{
				$response = json_encode($bc_product_custom_detail_update);
			$response_text = "failure";
			}*/
		$insert_log = array("bc_product_id"=>$bc_product_id,"bundle_kit_id"=>$data["bundlekit_id"],"status"=>$response_text,"response"=>$response,"type"=>"Update","app_installed_store_id"=>$store_id);
		echo "<pre>";print_r($insert_log);echo"</pre>";
		$insert_log_to_db = $pdo->sp_manage_bundle_bc_log($insert_log);
		echo "insert_log_to_db<pre>";print_r($insert_log_to_db);echo"</pre>";
	}else if( $data["destination_product_id"] && $data['status'] == "delete"){
		$bc_product_id = $data["destination_product_id"];
					$fields["is_visible"]=false;
					$insert_products = UpdateProductBC($store_hash,$header,$fields,$bc_product_id);
										echo "<pre>";print_r($insert_products);echo "</pre>";
					$destination_product_id = $insert_products->id;
					if($destination_product_id){
						$response = json_encode($insert_products);
						$response_text = "success";
						}else{
							$response = json_encode($insert_products);
						$response_text = "failure";
						}
					$insert_log = array("bc_product_id"=>$destination_product_id,"bundle_kit_id"=>$data["bundlekit_id"],"status"=>$response_text,"response"=>$response,"type"=>"Delete","app_installed_store_id"=>$store_id);
					echo "<pre>";print_r($insert_log);echo"</pre>";
					$insert_log_to_db = $pdo->sp_manage_bundle_bc_log($insert_log);
					echo "<pre>";print_r($insert_log_to_db);echo"</pre>";
		
	}else if($data["destination_product_id"] == null && $data['status'] == "insert"){
		$fields["name"]=$data["bundle_kit_name"];
		$fields["type"]="physical";
		$fields["price"]=$data["main_bundle_price"];
		$fields["categories"]=array(299);
		$fields["weight"]="0";
		$fields["sku"]=$data["bundle_sku"];
		$fields["is_visible"]=false;
		echo "fields_product<pre>";print_r($fields);echo "</pre>";
		$insert_products = CreateProductBC($store_hash,$header,$fields);
		

		if($insert_products->id){
		echo "insert_products<pre>";print_r($insert_products);echo "</pre>";
		$destination_product_id = $insert_products->id;
		if($destination_product_id){
			$response = json_encode($insert_products);
			$response_text = "success";
			}else{
				$response = json_encode($insert_products);
			$response_text = "failure";
			}
		$insert_log = array("bc_product_id"=>$destination_product_id,"bundle_kit_id"=>$data["bundlekit_id"],"status"=>$response_text,"response"=>$response,"type"=>"Insert","app_installed_store_id"=>$store_id);
		$insert_log_to_db = $pdo->sp_manage_bundle_bc_log($insert_log);
		echo "<pre>";print_r($insert_log_to_db);echo"</pre>";
		$input_data = array("app_installed_store_id"=>$store_id);
		$get_settings_details = $pdo->getSettings($input_data);
				//echo "get_settings_details<pre>";print_r($get_settings_details);echo "<pre>";
				$bundle_custom_filed_name = $get_settings_details[0]['param_bundling_custom_field_name'];
				echo "bundle_custom_filed_name";echo $bundle_custom_filed_name;
		$fields = array("name"=>$bundle_custom_filed_name,"value"=> $data["product_details"]);
		$bc_product_custom_detail_insert = createCustomfields($store_hash,$header,$fields,$destination_product_id);
		}

	}
}
function clientId()
{
$clientId = getenv('BC_CLIENT_ID');
return $clientId ?: '';
}
?>