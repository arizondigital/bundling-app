<?php
require('/var/www/html/Furniture/Bundling_App/include/config.php');
require_once '/var/www/html/Furniture/Bundling_App/include/webhook_sp_functions.php';
require_once '/var/www/html/Furniture/Bundling_App/include/bc_functions.php';

$random_number = time().'-'.rand(1,1000000);

$webhook_flag_status = 'pending';
$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' |  Webhook Log start'.PHP_EOL;
save_log ($log);
// Data for Insert SP call
$webhookContent = file_get_contents("php://input");
$webhook_result = json_decode($webhookContent, true); 

$scope="";
$scope = $webhook_result["scope"];
$store_id = $webhook_result["store_id"];
$producer = $webhook_result["producer"];
$hash_array = explode("/",$producer);
$hash = $hash_array[1];
$data_type = $webhook_result["data"]["type"];
$created_at = $webhook_result["created_at"];

$product_id=0;
$product_sku_id=0;
if($scope == "store/product/created"){
	$product_id = $webhook_result["data"]["id"];
	$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' |  Product_id :'.$product_id.'|  Scope :'.$scope.'  | Message : Product created'.PHP_EOL;
	save_log ($log);
}
elseif($scope == "store/product/updated"){
	$product_id = $webhook_result["data"]["id"];
	$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' |  Product_id :'.$product_id.'|  Scope :'.$scope.'  | Message : Product updated'.PHP_EOL;
	save_log ($log);
}
elseif($scope == "store/product/deleted"){
	$product_id = $webhook_result["data"]["id"];
	$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' |  Product_id :'.$product_id.'|  Scope :'.$scope.'  | Message : Product deleted'.PHP_EOL;
	save_log ($log);
}
elseif($scope == "store/sku/created"){
	$product_id = $webhook_result["data"]["sku"]["product_id"];
	$product_sku_id = $webhook_result["data"]["id"];
	$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' |  Product_id :'.$product_id.'|  Product_sku_id :'.$product_sku_id.'| Scope:'.$scope.'  | Message : Product sku created'.PHP_EOL;
	save_log ($log);
}
elseif($scope == "store/sku/updated"){
	$product_id = $webhook_result["data"]["sku"]["product_id"];
	$product_sku_id = $webhook_result["data"]["id"];
	$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' |  Product_id :'.$product_id.'|  Product_sku_id :'.$product_sku_id.'| Scope:'.$scope.'  | Message : Product sku updated'.PHP_EOL;
	save_log ($log);
}
elseif($scope == "store/sku/deleted"){
	$product_id = $webhook_result["data"]["sku"]["product_id"];
	$product_sku_id = $webhook_result["data"]["id"];
	$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' |  Product_id :'.$product_id.'|  Product_sku_id :'.$product_sku_id.'| Scope:'.$scope.'  | Message : Product sku deleted'.PHP_EOL;
	save_log ($log);
}


/*$scope = 'store/product/created';
$bc_product_id = 9752;
$bc_sku_id = 0;
$current_status = 10;
$webhook_flag_status = 'pending';
$created_at = time();
$hash ="ne2ojoufwv"; */

$insert_data = array();
$insert_data['product_hook_log_id'] = null;
$insert_data['bc_product_id'] = $product_id;
$insert_data['bc_sku_id'] = $product_sku_id;
$insert_data['scope'] = $scope;
$insert_data['webhook_flag_status'] = $webhook_flag_status;
$insert_data['inserted_date'] = null;
$insert_data['store_hash'] = $hash;

/*echo "<pre>";
print_r($insert_data);
echo "</pre>";*/
$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' |Input json :'.json_encode($insert_data).' |  Store ID :'.$store_id.' | Scope :'.$scope.' | Successfully Inserted to Log table'.PHP_EOL;
save_log ($log);
// Object Creation for database function call
$pdo = new WebhookModel();
$insert_product_webhook_log = $pdo->upsertProductWebhookLog($insert_data);

/*echo "insert_product_webhook_log<pre>";
print_r($insert_product_webhook_log);
echo "</pre>";*/


if($insert_product_webhook_log[0]['param_error_code'] != 0){
	$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' | Product ID :'.$product_id.' |  Store ID :'.$store_id.' | Scope :'.$scope.' | Error not Inserted to Log table'.PHP_EOL;
}else{
	$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' | Product ID :'.$product_id.' |  Store ID :'.$store_id.' | Scope :'.$scope.' | Successfully Inserted to Log table'.PHP_EOL;
}
save_log ($log);

$log  = date("Y-m-d h:i:s").' | Session : '.$random_number.' | Product ID :'.$product_id.' | Store ID :'.$store_id.' | Scope :'.$scope.' | Webhook Log End'.PHP_EOL;
save_log ($log);

function save_log ($log){
	echo file_put_contents("/var/www/html/Furniture/Bundling_App/webhook/Log/Product_log.txt", $log, FILE_APPEND);
}

?>