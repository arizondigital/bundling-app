<?php

ini_set('max_execution_time', 0);
ini_set("memory_limit","256M");

ini_set('display_errors', "1");
error_reporting(E_ALL);

require_once '/var/www/html/Furniture/Bundling_App/vendor/autoload.php';
require('/var/www/html/Furniture/Bundling_App/include/config.php');
require_once '/var/www/html/Furniture/Bundling_App/include/webhook_sp_functions.php';
require_once '/var/www/html/Furniture/Bundling_App/controllers/AdminController.php';

use Bigcommerce\Api\Client as Bigcommerce;
use Firebase\JWT\JWT;
use Guzzle\Http\Client;
use Handlebars\Handlebars;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// Load from .env file
$dotenv = new Dotenv\Dotenv('/var/www/html/Furniture/Bundling_App');
$dotenv->load();

//Object Creation for SP call functions
$pdo = new WebhookModel();

$admin_details = new AdminController();
$storeHash="1vmlni8o2p";
//$storeHash='80xk3nuh5c';
$stores = array();
$stores = $admin_details->getStore($storeHash);

echo "<pre>";
print_r($stores);
echo "</pre>";

foreach($stores as $store){
	$store_hash = $store['store_hash'];
	$store_id = $store['app_installed_store_id'];
	$client_id =  clientId();
	$access_token = $store['hash_token'];
	
	$page=1;
	$category_result = bc_get_category($store_hash, $client_id, $access_token, $page);
	echo $page_limit = $category_result->meta->pagination->total_pages;

	for($page=1;$page<=$page_limit;$page++)
	{
		
		$category_result=bc_get_category($store_hash, $client_id, $access_token, $page);
		echo "<pre>";print_r($category_result);echo "</pre>";

		// Get BC Category data and save to category table
		foreach($category_result->data as $individual_category_detail){

			$bc_category_id = '';
			$bc_category_id = $individual_category_detail->id;
			echo $bc_category_id;
			if($bc_category_id!=0)
			{
				$insert_data = array();
				$insert_data['param_bc_category_id'] = $individual_category_detail->id;
				$insert_data['param_bc_parent_category_id'] = $individual_category_detail->parent_id;
				$insert_data['param_name'] = $individual_category_detail->name;
				$insert_data['param_description'] = $individual_category_detail->description;
				$insert_data['param_is_active'] = 1;
				$insert_data['param_is_deleted'] = 0;
				$insert_data['param_app_installed_store_id'] = $store_id;

				echo "<pre>";
				print_r($insert_data);
				echo "</pre>";
				$log = '['.date("y:m:d h:i:s").']-- ' .$store_id.PHP_EOL;
		echo file_put_contents('/var/www/html/Furniture/Bundling_App/webhook/Log/category_sync_log.txt', $log, FILE_APPEND);
				$log = '['.date("y:m:d h:i:s").']-- ' .json_encode($insert_data) .PHP_EOL;
				echo file_put_contents('/var/www/html/Furniture/Bundling_App/webhook/Log/category_sync_log.txt', $log, FILE_APPEND);
				 //Insert Category Details to DB
				$insert_bc_category_detail = $pdo->upsertBCCategoryDetailDB($insert_data);
				echo "<pre>result";
				print_r($insert_bc_category_detail);
				echo "</pre>";
				$log = '['.date("y:m:d h:i:s").']-- ' .$store_id.PHP_EOL;
		echo file_put_contents('/var/www/html/Furniture/Bundling_App/webhook/Log/category_sync_log.txt', $log, FILE_APPEND);
				$log = '['.date("y:m:d h:i:s").']-- ' .json_encode($insert_bc_category_detail) .PHP_EOL;
				echo file_put_contents('/var/www/html/Furniture/Bundling_App/webhook/Log/category_sync_log.txt', $log, FILE_APPEND);
				//echo "test";die;
			}
		}
	}
}
die;

function bc_get_category($store_hash, $client_id, $access_token, $page)
{
	$URL = 'https://api.bigcommerce.com/stores/'.$store_hash.'/v3/catalog/categories?limit=250&page='.$page;
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $URL);
	curl_setopt($ch, CURLOPT_TIMEOUT, 100); 
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array ('X-Auth-Client: '.$client_id.'','X-Auth-Token: '.$access_token.'','Accept: application/json', 'Content-Type: application/json'));      
	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET'); 
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 ); 
	curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );   
	$category_response = curl_exec($ch);   
	$category_result = json_decode($category_response);
	return $category_result;
}

function configureBCApi($storeHash)
{
	Bigcommerce::configure(array(
		'client_id' => clientId(),
		'auth_token' => getAuthToken($storeHash),
		'store_hash' => $storeHash
	));
}
function clientId()
{
	$clientId = getenv('BC_CLIENT_ID');
	return $clientId ?: '';
}
function getAuthToken($storeHash)
{
	$admin_details = new AdminController();
	$result = $admin_details->getStore($storeHash);
	return $result[0]['hash_token'];
}

?>