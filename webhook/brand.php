<?php

ini_set('max_execution_time', 0);
ini_set("memory_limit","256M");

ini_set('display_errors', "1");
error_reporting(E_ALL);

require_once '/var/www/html/Furniture/Bundling_App/vendor/autoload.php';
require('/var/www/html/Furniture/Bundling_App/include/config.php');
require_once '/var/www/html/Furniture/Bundling_App/include/webhook_sp_functions.php';
require_once '/var/www/html/Furniture/Bundling_App/controllers/AdminController.php';

use Bigcommerce\Api\Client as Bigcommerce;
use Firebase\JWT\JWT;
use Guzzle\Http\Client;
use Handlebars\Handlebars;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// Load from .env file
$dotenv = new Dotenv\Dotenv('/var/www/html/Furniture/Bundling_App');
$dotenv->load();

//Object Creation for SP call functions
$pdo = new WebhookModel();

$admin_details = new AdminController();
//$storeHash = '1vmlni8o2p';
$storeHash='z0fvuka6uw';
$stores = array();
$stores = $admin_details->getStore($storeHash);

echo "<pre>";
print_r($stores);
echo "</pre>";

foreach($stores as $store){
	$store_hash = $store['store_hash'];
	$store_id = $store['app_installed_store_id'];
	$client_id = clientId();
	//$client_id = $store['client_id'];
	$access_token = $store['hash_token'];
	
	$page=1;
	$brand_result=bc_get_brands($store_hash, $client_id, $access_token, $page);
	$page_limit=$brand_result->meta->pagination->total_pages;
	/*echo "<pre>";
	print_r($brand_result);
	echo "</pre>";*/
	for($page=1;$page<=$page_limit;$page++)
	{
		$category_result=bc_get_brands($store_hash, $client_id, $access_token, $page);

		// Get BC Category data and save to category table
		foreach($category_result->data as $individual_category_detail){

			$bc_brand_id = '';
			$bc_brand_id = $individual_category_detail->id;
			echo $bc_brand_id;
			if($bc_brand_id!=0)
			{
				$insert_data = array();
				$insert_data['param_bc_brand_id'] = $individual_category_detail->id;
				$insert_data['param_name'] = $individual_category_detail->name;
				$insert_data['param_page_title'] = $individual_category_detail->page_title;
	//			$insert_data['param_meta_keywords'] = $individual_category_detail->meta_keywords;
				foreach($individual_category_detail->meta_keywords as $val){
					$insert_data['param_meta_keywords'] =null;
				}
				$insert_data['param_meta_description'] = $individual_category_detail->meta_description;
				$insert_data['param_search_keywords'] = $individual_category_detail->search_keywords;
				$insert_data['param_image_url'] = $individual_category_detail->image_url;
				$insert_data['param_is_customized'] = 1;
				$insert_data['param_is_active'] = 1;
				$insert_data['param_is_deleted'] = 0;
				$insert_data['param_custom_url'] = $individual_category_detail->custom_url->url;
				$insert_data['app_installed_store_id'] = $store_id;

				echo "<pre>";
				print_r($insert_data);
				echo "</pre>";
			// Insert Category Details to DB
				$insert_bc_category_detail = $pdo->upsertBrandWebhookLog($insert_data);
				echo "<pre>result";
				print_r($insert_bc_category_detail);
				echo "</pre>";
			
			}
			//die;
		}
	}

}

function bc_get_brands($store_hash, $client_id, $access_token, $page)
{
	$URL ="https://api.bigcommerce.com/stores/".$store_hash."/v3/catalog/brands?limit=250&page=".$page;
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $URL);
	curl_setopt($ch, CURLOPT_TIMEOUT, 100); 
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array ('X-Auth-Client: '.$client_id.'','X-Auth-Token: '.$access_token.'','Accept: application/json', 'Content-Type: application/json'));        
	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET'); 
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 ); 
	curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );   
	$category_response = curl_exec($ch);   
	$category_result = json_decode($category_response);
	return $category_result;
}

function configureBCApi($storeHash)
{
	Bigcommerce::configure(array(
		'client_id' => clientId(),
		'auth_token' => getAuthToken($storeHash),
		'store_hash' => $storeHash
	));
}
function clientId()
{
	$clientId = getenv('BC_CLIENT_ID');
	return $clientId ?: '';
}
function getAuthToken($storeHash)
{
	$admin_details = new AdminController();
	$result = $admin_details->getStore($storeHash);
	return $result[0]['hash_token'];
}
?>