<?php
ini_set('max_execution_time', 0);
ini_set("memory_limit","-1");
ini_set('display_errors', "1");
error_reporting(E_ALL);
require_once '/var/www/html/Furniture/Bundling_App/vendor/autoload.php';
require('/var/www/html/Furniture/Bundling_App/include/config.php');
require_once '/var/www/html/Furniture/Bundling_App/include/webhook_sp_functions.php';
require_once '/var/www/html/Furniture/Bundling_App/include/bc_functions.php';
require_once '/var/www/html/Furniture/Bundling_App/controllers/AdminController.php';



use Bigcommerce\Api\Client as Bigcommerce;
use Firebase\JWT\JWT;
use Guzzle\Http\Client;
use Handlebars\Handlebars;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// Load from .env file
$dotenv = new Dotenv\Dotenv('/var/www/html/Furniture/Bundling_App');
$dotenv->load();
//Object Creation for SP call functions
$pdo = new WebhookModel();
$timestamp = new DateTime();
$product_sync_date= 2000;

$back_date = date('Y-m-d H:i:s', strtotime('-'.$product_sync_date.' days'));


$datetime = new DateTime($back_date);
$formatted_date = $datetime->format(DateTime::ATOM); // Updated ISO8601

$temp_1 = substr($formatted_date,0,19);
$temp_2 = substr($formatted_date,20,25);
$date = $temp_1."-".$temp_2;

$admin_details = new AdminController();
$storeHash=null;
$stores = array();
$stores = $admin_details->getStore($storeHash);

echo "<pre>";
print_r($stores);
echo "</pre>";
foreach($stores as $store){	
	echo $store_hash = $store['store_hash'];echo "<br>";
	echo $auth_token = $store['hash_token'];echo "<br>";
	echo $store_id = $store['app_installed_store_id'];echo "<br>";
	$client_id = clientId();
	//echo "store_details<pre>";echo "test";echo $client;die;
	//echo $client_id = $store['client_id'];echo "<br>";
	//$client_id = clientId();
//	$client_id = 'k8uffoed58f2ww7q7imrn2v61rnnavx';
	$header = array ('X-Auth-Client: '.$client_id,'X-Auth-Token: '.$auth_token,'Accept: application/json', 'Content-Type: application/json');	
	print_r($header);

	configureBCApi($store_hash);
	
	//$product_count = Bigcommerce::getProductsCount();
	$product_count = getProductCount($store_hash,$header,$date);


	echo "productcount";
	echo $product_count;
	echo "<br>";
	if($product_count%250 != 0){
		 $page_limit = (int)($product_count/250)+1;
	}else{
		 $page_limit = (int)$product_count/250;
	}
	echo $product_count;
	for($page_number=1; $page_number<=$page_limit;$page_number++){
		//if($page_number==2){
		//	echo "end_page";die;
		//}
		echo "Called once";
		$bc_product_details = get_All_Product_Details_BC($store_hash,$header,$date,$page_number);
		
		$filter = array('limit'=>250,"page"=> $page_number);


		foreach($bc_product_details->data as $individual_product){
	
			$bc_product_id = '';
			$bc_product_id = $individual_product->id;
			//$bc_product_id = 14838;

			// Get Product Details from BC
			$bc_product_detail = getProductDetailsBC($store_hash,$header,$bc_product_id);			
			//$bc_product_detail = Bigcommerce::getProduct($bc_product_id);			
			
			// Save Product Custom Field Details
			$bc_product_custom_detail = array();
			$bc_product_custom_detail = getProductCustomFieldDetailsBC($store_hash,$header,$bc_product_id);
			echo "<pre>";
			print_R($bc_product_custom_detail);
			echo "</pre>";
			//$bc_product_custom_detail = $bc_product_detail->data->custom_fields;		
			$custom_field_json_array = array();
			if(count($bc_product_custom_detail->data) > 0){
				foreach($bc_product_custom_detail->data as $individual_custom_field){
					//$individual_customfeild_data_array = array();
					//$individual_customfeild_data_array['name'] = $individual_custom_field->name;
						if($individual_custom_field->name == 'Collection'){
							echo "founded";
						$bundle_kit_input = array("destination_product_id"=>$bc_product_id,"skus"=>$individual_custom_field->value,"app_installed_store_id"=>$store_id);
						echo "<pre>";print_r($bundle_kit_input);echo "</pre>";
						$bundle_kit_output = $pdo->sp_manage_product_bundle_migration($bundle_kit_input);
						echo "bundle_kit_output<pre>";print_r($bundle_kit_output);echo "</pre>";
						}
					//$individual_customfeild_data_array['value'] = $individual_custom_field->value;
					//$individual_customfeild_data_array['bc_product_custom_fields_id'] = $individual_custom_field->id;
					//$custom_field_json_array['custom_details'][] = $individual_customfeild_data_array;
				}
			}
			/*$custom_field_insert_data = array();
			$custom_field_insert_data['param_bc_product_id'] = $bc_product_id;
			$custom_field_insert_data['param_product_custom_field_details'] = (count($custom_field_json_array) > 0)?json_encode($custom_field_json_array):null;
			$custom_field_insert_data['param_app_installed_store_id'] = $store_id;
			echo "<pre>";
			print_R($custom_field_insert_data);
			echo "</pre>";
			$insert_bc_product_custom_field_detail = $pdo->upsertBCProductCustomFieldDetailDB($custom_field_insert_data);
			echo "<pre>";
			print_R($insert_bc_product_custom_field_detail);
			echo "</pre>";*/
			//die;
			

			//echo "endstate";die;
		}
	}
}
function configureBCApi($storeHash)
{
	Bigcommerce::configure(array(
		'client_id' => clientId(),
		'auth_token' => getAuthToken($storeHash),
		'store_hash' => $storeHash
	));
}
function clientId()
{
	$clientId = getenv('BC_CLIENT_ID');
	return $clientId ?: '';
}
function getAuthToken($storeHash)
{
	$admin_details = new AdminController();
	$result = $admin_details->getStore($storeHash);
	return $result[0]['hash_token'];
}
?>